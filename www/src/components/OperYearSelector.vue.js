// @ts-check

import Vue from 'vue';
import { BaseLogger as logger } from '@cern/base-vue';
import { first, get, invoke, map, noop, set } from 'lodash';
import { sources } from '../store';
import { makeDeferred } from '@cern/prom';

/**
 * @typedef {{
 *   paramList: V.Instance<typeof BaseVue.BaseParamList>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> } Instance
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'OperYearSelector',
  props: {
    title: { type: String, default: 'Operation Year' },
    inEdit: { type: Boolean, default: false },
    value: { type: Number, default: null },
    allowEmpty: { type: Boolean, default: false }
  },
  /**
  * @this {Instance}
  * @return {{
  *   operYears: Array<number>?,
  *   readyProm: prom.Deferred<any>
  * }}
  */
  data() {
    return {
      operYears: null,
      readyProm: makeDeferred()
    };
  },
  computed: {
    editValue: {
      /**
      * @this {Instance}
      * @return {string | number | null | undefined}
      */
      get() { return get(this.$refs, [ 'paramList', 'editValue' ]); },
      /**
      * @this {Instance}
      * @param {number} value
      */
      set(value) { set(this.$refs, [ 'paramList', 'editValue' ], value); }
    },
    /**
     * @return {number=}
     */
    firstValue() { return first(this.operYears); }
  },
  /** @this {Instance} */
  async mounted() {
    try {
      this.operYears = await this._retrieveOpenYears();
      invoke(this.$refs, [ 'paramList.$options.watch.inEdit' ], this.inEdit);
    }
    catch (err) { logger.error(err); }
    finally {
      this.readyProm.resolve(true);
    }
  },
  methods: {
    /**
     * @this {Instance}
     * @return {Promise<Array<number>>}
     */
    async _retrieveOpenYears() {
      return sources.operationPeriods.get({ sort: 'year:desc' })
      .then((res) => map(res, 'year'));
    },
    /**
     * @this {Instance}
     * @param {number | string} val
     */
    async setEditValue(val) {
      await this.readyProm.promise.catch(noop);
      this.editValue = val;
    }
  }
});
export default component;
