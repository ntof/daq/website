// @ts-check
import { get, isNil, noop, toNumber, toString } from 'lodash';
import Vue from 'vue';
import { mapGetters, mapState } from 'vuex';
import { BaseKeyboardEventMixin as KeyboardEventMixin,
  BaseLogger as logger } from '@cern/base-vue';
import { createPutParam, runDesc } from '../../interfaces/database';
import DocumentsList from '../Documents/DocumentsList.vue';
import RunStatusIcon from './RunStatusIcon.vue';
import DataStatusIcon from './DataStatusIcon.vue';
import CommentsList from '../Comments/CommentsList.vue';
import { sources } from '../../store';
import { AreaByPrefix } from '../../Consts';
import RouteUtilsMixin from '../../mixins/RouteUtilsMixin';
import DAQConfig from './DAQConfig.vue';
import HVConfig from './HVConfig.vue';
import TriggersList from './TriggersList.vue';
import EditDataStatusDialog from './EditDataStatusDialog.vue';
import { DateTime } from 'luxon';

/**
 * @typedef {{
 *  editDataStatusDialog: V.Instance<typeof EditDataStatusDialog>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> &
 *   V.Instance<ReturnType<KeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin>
 * } Instance
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'RunInfo',
  components: {
    DocumentsList, RunStatusIcon, DataStatusIcon, CommentsList,
    DAQConfig, HVConfig, EditDataStatusDialog, TriggersList
  },
  mixins: [ KeyboardEventMixin({ local: false }), RouteUtilsMixin ],
  /**
   * @return {{ inEdit: boolean, showConfig: boolean, updateTriggersOngoing: boolean }}
   */
  data() {
    return {
      inEdit: get(this.$route, [ 'meta', 'isAdmin' ]),
      showConfig: false,
      updateTriggersOngoing: false
    };
  },
  computed: {
    /** @return {string} */
    parentPath() {
      if (get(this.$route, [ 'meta', 'isAdmin' ])) {
        return '/admin/runs?runNumer=' + this.id;
      }
      else {
        return `/${get(this.area, [ 'name' ], 'runs')}?runNumber=${this.id}`;
      }
    },
    /** @return {any} */
    area() {
      return get(AreaByPrefix, Math.trunc((this.id || 0) / 100000));
    },
    /** @return {number} */
    id() { return toNumber(get(this.$route, [ 'params', 'id' ])); },
    .../** @type {{ showKeyHints(): boolean }} */(mapState('ui', [ 'showKeyHints' ])),
    .../** @type {{ runLoading(): boolean, run(): any }} */(mapState('runs', {
      runLoading: (state) => state.loading,
      run(state) { return get(state, [ 'db', this.id ]); }
    })),

    .../** @type {{ matSetupLoading(): boolean, matSetup(): any }} */(mapState('materialsSetups',
      {
        matSetupLoading: (state) => state.loading,
        matSetup(state) { return get(state, [ 'db', get(this.run, 'matSetupId') ]); }
      })),
    .../** @type {{ detSetupLoading(): boolean, detSetup(): any }} */(mapState('detectorsSetups',
      {
        detSetupLoading: (state) => state.loading,
        detSetup(state) { return get(state, [ 'db', get(this.run, 'detSetupId') ]); }
      })),
    .../** @type {{ docsLoading(): boolean, docs(): any[] }} */(mapState('documents',
      { docs: 'rows', docsLoading: 'loading' })),

    .../** @type {{ canEdit(): boolean }} */(mapGetters([ 'canEdit' ])),
    /** @return {boolean} */
    loading() {
      return this.runLoading || this.matSetupLoading || this.docsLoading;
    },
    duration() {
      const stop = get(this.run, [ 'stop' ]) || 0;
      const stopDate = (get(this.run, 'runStatus') === "ongoing") ?
        DateTime.now() : DateTime.fromMillis(stop);
      const startDate = DateTime.fromMillis(get(this.run, [ 'start' ], 0))
      .set({ millisecond: 0 });
      return stopDate.set({ millisecond: 0 }).diff(startDate)
      .toISOTime({ suppressMilliseconds: true });
    }
  },
  watch: {
    id() {
      if (isNil(this.run)) {
        this.fetch();
      }
    },
    run(value) {
      if (value) {
        this.fetchSetups();
      }
    }
  },
  /** @this {Instance} */
  beforeMount() {
    if (isNil(this.run)) {
      this.fetch();
    }
    else {
      this.fetchSetups();
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('ctrl-s-keydown', this.doSubmit);
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
    this.onKey('ctrl-left', () => this.goToRun(this.id - 1));
    this.onKey('ctrl-right', () => this.goToRun(this.id + 1));
    this.onKey('ctrl-up', () => this.$router.push(this.parentPath));
  },
  methods: {
    /**
     * @param  {boolean} [nocache=false]
     */
    async fetch(nocache = false) {
      const id = get(this.$route, [ 'params', 'id' ]);
      sources.runs.fetch({ max: 1, query: { runNumber: id } }, nocache);
    },
    /**
     * @param  {boolean} [nocache=false]
     */
    async fetchSetups(nocache = false) {
      sources.materialsSetups.fetch(
        { max: 1, query: { id: get(this.run, 'matSetupId') } }, nocache);
      sources.detectorsSetups.fetch(
        { max: 1, query: { id: get(this.run, 'detSetupId') } }, nocache);
    },
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value) {
      if (this.canEdit) {
        this.inEdit = value;
      }
    },
    async updateTriggers() {
      this.updateTriggersOngoing = true;
      try {
        await sources.runs.updateTriggers(this.id)
      }
      catch(err) {
        logger.error(err);
      }
      finally {
        this.updateTriggersOngoing = false;
        this.inEdit = false;
        await this.fetch(true);
      }
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doSubmit(event) {
      if (event) {
        event.preventDefault();
      }
      try {
        const params = createPutParam(this, this.run, runDesc);
        await sources.runs.put(this.id, params);
        this.inEdit = false;
        await this.fetch(true);
      }
      catch (e) { /* noop */ }
    },
    /**
     * @param {boolean} value
     */
    setShowConfig(value) {
      if (value) {
        this.showConfig = true;
      }
    },
    async doEditDataStatus() {
      await this.$refs.editDataStatusDialog.edit(this.run).catch(noop);
    },
    /**
     * @param  {number} number
     */
    goToRun(number) {
      this.$router.push({ params: { id: toString(number) } });
    }
  }
});
export default component;
