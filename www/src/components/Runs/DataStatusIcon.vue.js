
import Vue from 'vue';

const component = Vue.extend({
  name: 'DataStatusIcon',
  props: {
    run: { type: Object, default: null }
  },
  computed: {
    runClass() {
      if (!this.run) {
        return [ 'fa-question', 'text-warning' ];
      }
      switch (this.run.dataStatus) {
      case "ok":
        return [ 'fa-check' ];
      case "bad":
        return [ 'fa-trash', 'text-muted' ];
      default:
        return [ 'fa-question', 'text-warning' ];
      }
    },
    title() {
      return (this.run) ?
        `${this.run.dataStatus}` : 'invalid run';
    }
  }
});

export default component;
