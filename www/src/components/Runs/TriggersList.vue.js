// @ts-check

import Vue from 'vue';
import DBCollapsible from '../DBCollapsible.vue';

const component = Vue.extend({
  name: 'TriggersList',
  components: { DBCollapsible },
  props: {
    runNumber: { type: Number, default: null }
  }
});
export default component;
