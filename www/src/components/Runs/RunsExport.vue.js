/* eslint-disable max-lines */
// @ts-check

import Vue from 'vue';
import moment from 'moment';
import { get, isNil, toNumber } from 'lodash';
import OperYearSelector from '../OperYearSelector.vue';
import { Area } from '../../Consts';
import { CSVBuilder, HTMLTableBuilder, JSONBuilder } from '../ExportBuilders';
import { sources } from '../../store';
import { BaseLogger as logger } from '@cern/base-vue';
import { DateTime } from 'luxon';

/**
 * @typedef {import('../ExportBuilders').GenericBuilder} GenericBuilder
 * @typedef {import('../ExportBuilders').HTMLDataProcessorsMap} HTMLDataProcessorsMap
 * @typedef {import('../ExportBuilders').CSVDataProcessorsMap} CSVDataProcessorsMap
 * @typedef {V.Instance<typeof component> } Instance
 */
const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: 'RunsExport',
  components: { OperYearSelector },
  props: {
    title: { type: String, default: 'Export RunBook' },
    filePrefix: { type: String, default: 'export' },
    area: { type: String, default: null }
  },
  /**
   * @return {{
   *  startDate: number|null,
   *  stopDate: number|null,
   *  datesOk: boolean,
   *  exporting: boolean,
   * }}
   */
  data() {
    return { startDate: null, stopDate: null, datesOk: true, exporting: false };
  },
  computed: {
    /**
     * @this {Instance}
     * @returns number
     */
    prefix() {
      return get(Area, [ this.area, 'prefix' ], 0) * 100000;
    },
    /** @return {boolean} */
    disableExport() {
      return !this.datesOk || this.prefix === 0;
    }
  },
  methods: {
    /**
     * @this {Instance}
     * @param {number} year
     */
    async setOperYearDates(year) {
      if (isNil(year)) { return; }

      const operYear = await sources.operationPeriods.get({ query: { year } })
      .then((res) => get(res, [ 0 ]));
      this.startDate = operYear.start;
      this.stopDate = operYear.stop;
    },
    /**
     * @param {any} ref
     */
    checkDates() {
      const start = this.$refs.start.editTimestamp || 0;
      const stop = this.$refs.stop.editTimestamp || 0;
      this.datesOk = start < stop;
    },
    _saveDates() {
      this.startDate = this.$refs.start.editTimestamp * 1000;
      this.stopDate = this.$refs.stop.editTimestamp * 1000;
    },
    /** @this {Instance} */
    async downloadJson() {
      this._saveDates();
      if (!this._checkArea()) { return; }
      const builder = new JSONBuilder();
      await this.retrieveData(builder);
      var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(builder.getStringData());
      this.downloadFile(dataStr, 'json');
    },
    /** @this {Instance} */
    async downloadCsv() {
      this._saveDates();
      if (!this._checkArea()) { return; }
      const builder = new CSVBuilder();
      /** @type CSVDataProcessorsMap */
      const processorMap = {
        start: (value) => {
          const dateStr = DateTime.fromMillis(toNumber(value)).toISO();
          return dateStr;
        },
        stop: (value) => {
          const dateStr = DateTime.fromMillis(toNumber(value)).toISO();
          return dateStr;
        }
      };
      builder.setDataProcessors(processorMap);
      await this.retrieveData(builder);
      var dataStr = "data:text/csv;charset=utf-8," + encodeURIComponent(builder.getStringData());
      this.downloadFile(dataStr, 'csv');
    },
    /** @this {Instance} */
    async downloadHtml() {
      this._saveDates();
      if (!this._checkArea()) { return; }
      const builder = new HTMLTableBuilder();
      /** @type HTMLDataProcessorsMap */
      const processorMap = {
        description: (value) => {
          const pre = document.createElement('pre');
          pre.setAttribute("style", "text-align: left;");
          pre.appendChild(document.createTextNode(value));
          return pre;
        },
        start: (value) => {
          const dateStr = DateTime.fromMillis(toNumber(value)).toISO();
          return document.createTextNode(dateStr);
        },
        stop: (value) => {
          const dateStr = DateTime.fromMillis(toNumber(value)).toISO();
          return document.createTextNode(dateStr);
        },
        totalProtons: (value) => {
          const bigNumberFunc = get(this.$options.filters, 'b-bigNumber');
          const expValue = bigNumberFunc(value, '', { precision: 2 });
          return document.createTextNode(expValue);
        }
      };
      builder.setDataProcessors(processorMap);

      await this.retrieveData(builder);
      var dataStr = "data:text/html;charset=utf-8," + encodeURIComponent(builder.getStringData());
      this.downloadFile(dataStr, 'html');
    },
    _checkArea() {
      if (this.prefix === 0) {
        logger.error("Unknown AREA. Export not available.");
        return false;
      }
      return true;
    },
    /**
     * @param {GenericBuilder} builder
     */
    async retrieveData(builder) {
      builder.setIgnoredFields([ 'descOriginal', 'titleOriginal' ]);
      const query = {
        $and: [
          { runNumber: { '$gte': this.prefix, '$lte': this.prefix + 100000 - 1 } },
          { start: { '$gte': this.startDate, '$lte': this.stopDate } }
        ]
      };
      this.exporting = true;
      const max = 50;
      for (let offset = 0; ; offset += max) {
        const data = await sources.runs.get({ max, offset, query, sort: "runNumber:desc" });
        builder.append(data);
        if (data.length < max) { break; }
      }
    },
    /**
     * @param {string} dataStr
     * @param {'json' | 'csv' | 'html'} extension
     */
    downloadFile(dataStr, extension) {
      const fileName = `${this.filePrefix}-${new Date().toISOString()}.${extension}`;
      var downloadAnchorNode = document.createElement('a');
      downloadAnchorNode.setAttribute("href", dataStr);
      downloadAnchorNode.setAttribute("download", fileName);
      document.body.appendChild(downloadAnchorNode); // required for firefox
      downloadAnchorNode.click();
      downloadAnchorNode.remove();
      this.exporting = false;
    }
  }
});
export default component;
