// @ts-check

import { get, invoke } from 'lodash';
import { sources } from '../../store';
import Vue from 'vue';
import d from 'debug';
import $ from 'jquery';
import RouteUtilsMixin from '../../mixins/RouteUtilsMixin';

const debug = d('app:comp');

/**
 * @typedef {{
 *  dialog: V.Instance<typeof BaseVue.BaseDialog>
 *  md: V.Instance<typeof BaseVue.BaseMarkdownWidget>
 *  status: V.Instance<typeof BaseVue.BaseParamList>
 * }} Refs
 * @typedef {V.Instance<component, V.ExtVue<any, Refs>> &
 *  V.Instance<typeof RouteUtilsMixin>
 * } Instance
 */

const component = /** @type {V.Constructor<any, Refs> } */ (Vue).extend({
  name: 'EditDataStatusDialog',
  mixins: [ RouteUtilsMixin ],
  /**
   * @return {{ run: any, visible: boolean }}
   */
  data() {
    return { run: {}, visible: false };
  },
  mounted() {
    $(this.$el).on('shown.bs.modal', this.onShown);
  },
  beforeDestroy() {
    $(this.$el).off('shown.bs.modal', this.onShown);
  },
  methods: {
    /**
     * @this {Instance}
     * @param  {any} run
     * @return {Promise<void>}
     */
    async edit(run) {
      if (!run) { return; }

      this.run = run;
      this.visible = true;
      try {
        if (await this.$refs.dialog.request()) {
          const status = get(this, [ '$refs', 'status', 'editValue' ]);
          const comments =
            `:pushpin: _Run Status ${run.dataStatus} -> ${status}_ <!-- DO NOT REMOVE THIS LINE -->\n\n` +
            get(this, [ '$refs', 'md', 'editValue' ]);

          await sources.runs.put(this.run.runNumber, { dataStatus: status });
          await sources.comments.post(
            { comments, runNumber: get(run, 'runNumber') }, this.isAdmin);
          sources.runs.refresh();
          sources.comments.refresh();
        }
      }
      catch (err) {
        debug('%s error:', this.$options.name, err);
      }
      finally {
        this.visible = false;
      }
    },
    /** @this {Instance} */
    onShown() {
      invoke(this, [ '$refs', 'md', 'refreshEditor' ]);
      invoke(this, [ '$refs', 'md', 'focus' ]);
    }
  }
});
export default component;
