// @ts-check

import { keyBy, map, set } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';
import { sources } from '../../store';
import DAQChannel from './DAQConfigChannel.vue';

const component = Vue.extend({
  name: 'DAQConfig',
  components: { DAQChannel },
  props: {
    runNumber: { type: Number, default: null }
  },
  /**
   * @return {{ sortedConfs: ?any[] }}
   */
  data() {
    return { sortedConfs: null };
  },
  computed: {
    .../** @type {{ confsLoading(): boolean, confs(): any[] }} */(mapState('configurations',
      { confs: 'rows', confsLoading: 'loading' })),
    .../** @type {{ daqisLoading(): boolean, daqis(): any[] }} */(mapState('daqInstruments',
      { daqis: 'rows', daqisLoading: 'loading' })),
    /** @return {boolean} */
    loading() { return this.confsLoading || this.daqisLoading; }
  },
  watch: {
    runNumber() { this.fetch(); },
    confs() {
      const daqiIds = map(this.confs, 'daqiId');
      const query = set({}, [ 'id', '$in' ], daqiIds);
      sources.daqInstruments.fetch({ query,
        sort: 'crate:asc,module:asc,channel:asc' });
    },
    daqis() {
      this.update();
    }
  },
  mounted() {
    this.fetch();
    if (this.daqis && !this.loading) {
      this.update();
    }
  },
  methods: {
    fetch(nocache = false) {
      sources.configurations.fetchRelated('runs',
        { runNumber: this.runNumber }, { query: { status: 1 } }, nocache);
    },
    update() {
      const confsByDaq = keyBy(this.confs, 'daqiId');
      this.sortedConfs = map(this.daqis, (d) => confsByDaq[d.id]);
    }
  }
});
export default component;
