
import Vue from 'vue';

const component = Vue.extend({
  name: 'RunStatusIcon',
  props: {
    run: { type: Object, default: null }
  },
  computed: {
    runClass() {
      if (!this.run) {
        return [ 'fa-question', 'text-warning' ];
      }
      switch (this.run.runStatus) {
      case "ongoing": return [ 'fa-running', 'text-success' ];
      case "crashed": return [ 'fa-times', 'text-danger' ];
      case "finished": return [ 'fa-check' ];
      default:
        return [ 'fa-question', 'text-warning' ];
      }
    },
    title() {
      return (this.run) ?
        `${this.run.runStatus}` : 'invalid run';
    }
  }
});

export default component;
