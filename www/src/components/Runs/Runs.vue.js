// @ts-check

import Vue from 'vue';
import {
  assign, get, isEmpty, isNil, set, size,
  toNumber, toString } from 'lodash';
import { Area, AreaList } from '../../Consts';
import DBSearchMixin from '../../mixins/DBSearchMixin';
import RunStatusIcon from './RunStatusIcon.vue';
import RunsExport from './RunsExport.vue';

/**
 * @typedef {typeof import('../../mixins/RouteUtilsMixin').default} RouteUtilsMixin
 * @typedef {V.Instance<typeof component, V.ExtVue<typeof options, any>> &
 *   V.Instance<typeof DBSearchMixin> &
 *   V.Instance<ReturnType<BaseVue.BaseKeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin> } Instance
 */

/** @param {string} value */
function truncateFilter(value) {
  return (size(value) >= 40) ? (value.slice(0, 38) + '...') : value;
}

/** @param {string} value */
function untruncateFilter(value) {
  return (size(value) >= 40) ? value : '';
}

const options = { source: 'runs', AreaList };

const component = /** @type {V.Constructor<typeof options, any>} */ (Vue).extend({
  name: 'Runs',
  components: { RunStatusIcon, RunsExport },
  ...options,
  filters: {
    truncate: truncateFilter,
    untruncate: untruncateFilter
  },
  mixins: [ DBSearchMixin ],
  props: {
    area: { type: String, default: null }
  },
  /**
   * @return {{
   *  folder: string|null,
   *  date: number|null,
   *  runNumber: string|null,
   *  earNumber: number|null,
   *  title: string|null
   * }}
   */
  data() {
    return { folder: null, date: null, runNumber: null, earNumber: null, title: null };
  },
  computed: {
    prefix() {
      return get(Area, [ this.area, 'prefix' ], this.earNumber || 0) * 100000;
    }
  },
  mounted() {
    this.onKey('ctrl-left', () => this.goToPage(this.page - 1));
    this.onKey('ctrl-right', () => this.goToPage(this.page + 1));
  },
  methods: {
    loadRoute() {
      try {
        const query = this.$route.query;
        this.page = toNumber(get(query, 'page', 0));
        this.runNumber = get(query, 'runNumber', null);
        this.folder = /** @type {string} */ (get(query, 'folder', null));
        this.title = get(query, 'title', null);

        const date = get(query, 'date', null);
        if (isEmpty(date)) {
          this.date = null;
          this.$refs.date.editTimestamp = null;
        }
        else {
          this.date = toNumber(date);
          this.$refs.date.editTimestamp = this.date;
        }
        if (this.$refs.earNumber) {
          const earNumber =
            /** @type {string|number} */ (get(query, 'earNumber', null));
          this.earNumber = earNumber;
          this.$refs.earNumber.editValue = earNumber;
        }
        this.$refs.folder.editValue = this.folder;
        this.$refs.runNumber.editValue = this.runNumber;

        this.$refs.title.editValue = this.title;
      }
      catch (e) {
        console.log(e);
      }
    },
    /**
     * @param  {any} params
     * @return {any}
     */
    prepareQuery(params) {
      this.loadRoute();
      /** @type {any} */
      params = assign(params, {
        sort: 'runNumber:desc',
        query: { }
      });
      if (this.prefix !== 0) {
        params.query.runNumber = {
          '$gte': this.prefix, '$lte': this.prefix + 100000 - 1
        };
      }

      if (!isNil(this.date)) {
        params.query.start = { '$lte': this.date * 1000 };
      }
      if (!isNil(this.folder)) {
        params.query.castorFolder = { '$like': this.globLike(this.folder) };
      }
      if (!isNil(this.runNumber)) {
        set(params.query, [ 'runNumber', '$lte' ], this.runNumber);
      }
      if (!isNil(this.title)) {
        const titleLike = { '$like': this.globLike(this.title) };
        params.query['$or'] = [ { title: titleLike }, { description: titleLike } ];
      }
      return params;
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    doSearch(event) {
      if (event) {
        event.preventDefault();
      }

      this.addQuery({
        title: get(this.$refs, [ 'title', 'editValue' ]),
        earNumber: get(this.$refs, [ 'earNumber', 'editValue' ]),
        folder: get(this.$refs, [ 'folder', 'editValue' ]),
        runNumber: get(this.$refs, [ 'runNumber', 'editValue' ]),
        date: toString(get(this.$refs, 'date.editTimestamp')),
        page: "0"
      });
    }
  }
});
export default component;
