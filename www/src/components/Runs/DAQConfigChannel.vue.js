// @ts-check

import { get, isEmpty, isNil, map, set, transform } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';
import { sources } from '../../store';

const component = Vue.extend({
  name: 'DAQConfigChannel',
  props: {
    conf: { type: Object, default: null }
  },
  /**
   * @return {{ params: ?{ [key:string]: any }}}
   */
  data() {
    return { params: null };
  },
  computed: {
    .../** @type {{ daqiLoading(): boolean, daqi(): any }} */(mapState('daqInstruments', {
      daqi(state) { return get(state, [ 'db', get(this.conf, 'daqiId') ]); },
      daqiLoading: (state) => state.loading
    })),
    /** @return {boolean} */
    loading() { return this.daqiLoading || isNil(this.params); },
    /** @return {number} */
    timeWindow() {
      const sampleRate = get(this.params, 'sampleRate');
      return (sampleRate) ? (get(this.params, 'sampleSize') / sampleRate) : NaN;
    },
    lowerLimit() {
      return -(get(this.params, 'fullScale') / 2) - get(this.params, 'offset');
    },
    /** @return {boolean} */
    zspEnabled() {
      const start = get(this.params, 'Zero suppression start', 0);
      return (start < (this.timeWindow * 1E6));
    }
  },
  watch: {
    conf() {
      this.fetch();
    }
  },
  mounted() {
    this.fetch();
  },
  methods: {
    async fetch() {
      if (isNil(this.conf)) {
        this.params = null;
        return;
      }
      return sources.relConfParams.get({ query: { confId: this.conf.id } })
      .then((rels) => sources.daqParametersCache.fetchMore(map(rels, 'paramId')))
      .then((params) => transform(params, (r, param) => set(r, [ param.name ], param.value), {}))
      .then((params) => { this.params = params; });
    },
    getZspType() {
      if (!this.zspEnabled) {
        return "off";
      }
      const masterType = get(this.params, 'masterDetectorType');
      if (isNil(masterType)) {
        return "on";
      }
      else if (isEmpty(masterType)) {
        return "master";
      }
      else {
        return "slave";
      }
    },
    getMasterGroup() {
      if (!this.zspEnabled) {
        return "";
      }
      const masterType = get(this.params, 'masterDetectorType');
      const masterId = get(this.params, 'masterDetectorId');
      if (!isEmpty(masterType)) {
        return `${masterType} ${masterId}`;
      }
      return '';
    }
  }
});
export default component;
