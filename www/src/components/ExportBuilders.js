// @ts-check

import { forEach, forOwn,
  get, indexOf, isEmpty, isNil,
  omit, toNumber, toString } from 'lodash';

/**
 * @typedef {{[key: string]: string | number}} Fields
 */

export class GenericBuilder {
  constructor() {
    /** @type Array<string> */
    this.columns = [];
    /** @type Array<string> */
    this.ignoredFields = [];
  }

  /**
   * @param {Array<string>} keys
   */
  setIgnoredFields(keys) {
    this.ignoredFields = keys;
  }

  /**
   * @param {Array<Fields>} data
   */
  // eslint-disable-next-line no-unused-vars
  append(data) { throw new Error("Please override append(). Data length provided: " + data.length); } // To override

  /**
   * @returns {string} data
   */
  getStringData() { throw new Error("Please override getStringData()."); } // To override

  /**
   * @param {Array<Fields>} data
   */
  buildColumns(data) {
    forEach(data, (item) => {
      forOwn(item, (value, key) => {
        // return if already added
        if (indexOf(this.columns, key) !== -1) { return; }
        // return if is an ingored field
        if (indexOf(this.ignoredFields, key) !== -1) { return; }
        this.columns.push(key);
      });
    });
  }
}

export class JSONBuilder extends GenericBuilder {
  constructor() {
    super();
    /** @type Array<Fields> */
    this.data = [];
  }

  /**
   * @param {Array<Fields>} data
   */
  append(data) {
    // Push data omitting ignored fields
    this.data.push(...data.map((item) => omit(item, this.ignoredFields)));
  }

  /**
   * @returns {string} data
   */
  getStringData() { return JSON.stringify(this.data); }
}

/**
 * @typedef {{ [key: string]: function(string): string }} CSVDataProcessorsMap
 */

export class CSVBuilder extends GenericBuilder {

  constructor() {
    super();
    /** @type Array<string> */
    this.lines = []; // header + rows

    /** @type CSVDataProcessorsMap */
    this.dataProcessors = {};
  }

  /**
   * @param {CSVDataProcessorsMap} map
   */
  setDataProcessors(map) {
    this.dataProcessors = map;
  }

  /**
   * @param {Array<Fields>} data
   */
  append(data) {
    if (isEmpty(this.columns)) {
      this.buildColumns(data);
      this.lines.push(this.columns.join(','));
    }

    this.lines.push(
      ...data.map(
        (row) => this.columns.map(
          (field) => JSON.stringify(
            row[field], // Value
            (k, v) => this._replacer(field, v) // Replacer
          )).join(','))
    );
  }

  /**
   * @param {string} key
   * @param {any} value
   * @returns {any}
   */
  _replacer(key, value) {
    const processor = get(this.dataProcessors, key);
    if (!isNil(processor)) {
      return processor(value);
    }
    if (value === true || value === false) {
      return toNumber(value);
    }
    return isNil(value) ? '' : value;
  }

  getStringData() { return this.lines.join('\n'); }
}

/**
 * @typedef {{ [key: string]: function(string): Element | Text }} HTMLDataProcessorsMap
 */

export class HTMLTableBuilder extends GenericBuilder {

  constructor() {
    super();
    this.table = document.createElement('table');
    this.table.setAttribute("style", "border: 1px solid; border-collapse: collapse; font-size: small; text-align: center;");

    /** @type HTMLDataProcessorsMap */
    this.dataProcessors = {};

    // Utility objects
    this._tr_ = document.createElement('tr');
    this._tr_.setAttribute("style", "border: 1px solid;");
    this._th_ = document.createElement('th');
    this._th_.setAttribute("style", "border: 1px solid;");
    this._td_ = document.createElement('td');
    this._td_.setAttribute("style", "border: 1px solid;");
  }

  /**
   * @param {HTMLDataProcessorsMap} map
   */
  setDataProcessors(map) {
    this.dataProcessors = map;
  }

  createHeaders() {
    if (isEmpty(this.columns)) { return; }

    const tr = this._tr_.cloneNode(false);
    forEach(this.columns, (col) => {
      const th = this._th_.cloneNode(false);
      th.appendChild(document.createTextNode(col));
      tr.appendChild(th);
    });
    this.table.appendChild(tr);
  }

  /**
   * @param {Array<Fields>} data
   */
  append(data) {
    if (isEmpty(this.columns)) {
      this.buildColumns(data);
      this.createHeaders();
    }

    forEach(data, (item) => {
      const tr = this._tr_.cloneNode(false);
      forEach(this.columns, (col) => {
        const td = this._td_.cloneNode(false);
        const cellValue = toString(get(item, col, ''));
        const processor = get(this.dataProcessors, col);
        if (!isNil(processor)) {
          td.appendChild(processor(cellValue));
        }
        else {
          td.appendChild(document.createTextNode(cellValue));
        }
        tr.appendChild(td);
      });
      this.table.appendChild(tr);
    });
  }

  /**
   * @returns {string} the html table
   */
  getStringData() {
    return this.table.outerHTML;
  }
}
