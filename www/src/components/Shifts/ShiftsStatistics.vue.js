// @ts-check

import Vue from 'vue';
import { mapState } from 'vuex';
import { BaseKeyboardEventMixin as KeyboardEventMixin } from '@cern/base-vue';
import RouteUtilsMixin from '../../mixins/RouteUtilsMixin';
import ShiftsStatsByUser from './ShiftsStatsByUser.vue';
import ShiftsStatsByInstitute from './ShiftsStatsByInstitute.vue';
import OperYearSelector from '../OperYearSelector.vue';
import { assign, get, isEqual, toNumber, toString } from 'lodash';
import { ShiftStatsKind } from '../../Consts';

/**
 * @typedef {typeof import('@cern/base-vue').BaseParamToggle} ParamToggle
 * @typedef {{
 *   operYearSelect: V.Instance<typeof OperYearSelector>
 *   performed: V.Instance<ParamToggle>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> &
 *   V.Instance<RouteUtilsMixin> &
 *   V.Instance<ReturnType<KeyboardEventMixin>> } Instance
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'ShiftsStatistics',
  components: { OperYearSelector, ShiftsStatsByUser, ShiftsStatsByInstitute },
  mixins: [ KeyboardEventMixin({ local: false }), RouteUtilsMixin ],
  data() {
    return {
      year: toNumber(get(this.$route, [ 'query', 'year' ]) || new Date().getFullYear()),
      isAll: get(this.$route, [ 'query', 'all' ], 'true') === 'true',
      isStatsByUser: get(this.$route, [ 'query', 'type' ], ShiftStatsKind.BYUSER) === ShiftStatsKind.BYUSER
    };
  },
  computed: {
    .../** @type {{ showKeyHints(): boolean }} */mapState('ui', {
      showKeyHints: (state) => state.showKeyHints
    })
  },
  watch: {
    '$route.query'(query, old) {
      if (!isEqual(query, old)) {
        this.loadRoute();
      }
    }
  },
  /** @this {Instance} */
  async mounted() {
    this.onKey('ctrl-s-keydown', (/** @type {KeyboardEvent} */ event) => {
      if (event) { event.preventDefault(); }
      if (this.isStatsByUser) {
        this.openStatsByUser();
      }
      else {
        this.openStatsByInstitute();
      }
    });
    this.loadRoute();
  },
  methods: {
    loadRoute() {
      assign(this, this.$options.data.call(this));
      this.$refs.performed.editValue = !this.isAll;
      this.$refs.operYearSelect.editValue = this.year;
    },
    /** @this {Instance} */
    openStatsByUser() {
      const val = this._getFormValues();
      this.pushQuery(val.year, true, val.isAll);
    },
    /** @this {Instance} */
    openStatsByInstitute() {
      const val = this._getFormValues();
      this.pushQuery(val.year, false, val.isAll);
    },
    /**
     * @this {Instance}
     * @return {{year: number, isAll: boolean}}
     */
    _getFormValues() {
      const year = this.$refs.operYearSelect.editValue;
      const isAll = !this.$refs.performed.editValue;
      return { year, isAll };
    },
    /**
     * @this {Instance}
     * @param {number} year
     * @param {boolean} isByUser
     * @param {boolean} all
     */
    pushQuery(year, isByUser, all) {
      this.addQuery({
        year: toString(year),
        type: isByUser ? ShiftStatsKind.BYUSER : ShiftStatsKind.BYINSTITUTE,
        all: toString(all) });
    }
  }
});
export default component;
