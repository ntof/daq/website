// @ts-check

import Vue from 'vue';
import DBCollapsible from '../DBCollapsible.vue';
import ShiftLink from './ShiftLink.vue';

const component = Vue.extend({
  name: 'ShiftsList',
  components: { DBCollapsible, ShiftLink },
  props: {
    association: { type: Object, default: null },
    title: { type: String, default: 'Shifts' }
  },
  computed: {
    assoID() {
      return this.association ? this.association.id : -1;
    }
  }
});
export default component;
