/* eslint-disable max-lines */
// @ts-check

import Vue from 'vue';
import { mapGetters, mapState } from 'vuex';
import { sources } from '../../store';
import {
  BaseKeyboardEventMixin as KeyboardEventMixin,
  BaseLogger as logger } from '@cern/base-vue';
import ShiftPointsDialog from './ShiftPointsDialog.vue';
import RouteUtilsMixin from '../../mixins/RouteUtilsMixin';
import {
  capitalize, first, get, includes, isArray, 
  isEqual, isNil, isNumber, isObject, keys,
  map, noop, set, toNumber,  uniq, values } from 'lodash';
import { ShiftKind, SpecialUsers } from '../../Consts';
// @ts-ignore Missing typescript types
import { CalendarView, CalendarViewHeader } from "vue-simple-calendar";
import { DateTime } from 'luxon';
// @ts-ignore The next line is processed by webpack.
require("vue-simple-calendar/static/css/default.css");

export const ShiftKindCSS = {
  'shift-leader': { classes: 'cv-shift-leader' },
  'night': { classes: '' },
  'morning': { classes: '' },
  'afternoon': { classes: '' }
};

/**
 * @typedef {typeof import('../../mixins/RouteUtilsMixin').default} RouteUtilsMixin
 *
 * @typedef {{
 *   id: string
 *   startDate: Date | string
 *   endDate?: Date | string
 *   title?: string
 *   url?: string
 *   classes?: string
 *   style?: string
 * }} VueCalendarItem
 *
 * @typedef {{
 *  confirmDialog: V.Instance<typeof BaseVue.BaseDialog>
 *  shiftPointsDialog: V.Instance<typeof ShiftPointsDialog>
 * }} Refs
 *
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> &
 *   V.Instance<ReturnType<KeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin> } Instance
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'Shifts',
  components: { CalendarView, CalendarViewHeader, ShiftPointsDialog },
  filters: {
    /**
     * @param {{ firstName: string; lastName: string; } | undefined } userObj
     */
    showUserInfo: (userObj) => {
      if (!userObj) { return 'Empty'; }
      return `${first(userObj.firstName)}. ${userObj.lastName}`;
    },
    /**
     * @param {{ alias: string; } | undefined } instObj
     */
    showInstInfo: (instObj) => {
      if (!instObj) { return ''; }
      return `(${instObj.alias})`;
    }
  },
  mixins: [ KeyboardEventMixin({ local: false }), RouteUtilsMixin ],
  /**
   * @this {Instance}
   * @return {{
   *  inEdit: boolean,
   *  actionProcessing: boolean
   *  showDate: Date
   *  dialogErrorMsg: string | null
   *  dialogInfo: {
   *    action: string,
   *    shift: any,
   *    username: string | null
   *  } | null
   * }}
   */
  data() {
    return {
      inEdit: get(this.$route, [ 'meta', 'isAdmin' ]),
      actionProcessing: false,
      // @ts-ignore
      showDate: this.thisMonth(1),
      dialogErrorMsg: null,
      dialogInfo: null
    };
  },
  computed: {
    .../** @type {{ darkMode(): boolean }} */(mapState('ui', [ 'darkMode' ])),
    /** @return {number} */
    year() {
      const paramYear = get(this.$route, [ 'query', 'year' ], new Date().getFullYear());
      return toNumber(paramYear);
    },
    /** @return {number} */
    month() {
      /** @type { any } */
      let paramMonth = get(this.$route, [ 'query', 'month' ]);
      paramMonth = paramMonth ? toNumber(paramMonth) - 1 : new Date().getMonth();
      return toNumber(paramMonth);
    },
    .../** @type {{ username(): string }}  */(mapGetters([ 'username' ])),
    .../** @type {{ canBookShift(): boolean }} */(mapGetters([ 'canBookShift' ])),
    .../** @type {{ showKeyHints(): boolean }} */mapState('ui', {
      showKeyHints: (state) => state.showKeyHints
    }),
    .../** @type {{ shiftLoading(): boolean, shifts(): any }} */(mapState('shifts', {
      shiftLoading: (state) => state.loading,
      shifts(state) { return get(state, [ 'rows' ]); }
    })),
    .../** @type {{ associations(): any }} */(mapState('associationsCache', { associations: 'db' })),
    .../** @type {{ users(): any }} */(mapState('usersCache', { users: 'db' })),
    .../** @type {{ institutes(): any }} */(mapState('institutesCache', { institutes: 'db' })),
    /** @return {boolean} */
    loading() {
      return this.shiftLoading || this.actionProcessing;
    },
    /** @return { Array<VueCalendarItem> } */
    shiftsCal() {
      return map(this.shifts, (/** @type any **/row) => {
        let user, institute;
        const association = this.getAssociation(row.assoId);
        if (association) { user = this.getUser(association.userId); }
        if (association) { institute = this.getInstitute(association.institute); }
        const startHour = get(ShiftKind, [ row.shiftKind, 'startHour' ]);
        const period = get(ShiftKind, [ row.shiftKind, 'period' ]);
        const startDate = this.getGenevaDateTime(row.date).set({ hour: startHour });
        // calculate css classess
        let classes = get(ShiftKindCSS, [ row.shiftKind, 'classes' ], '');
        if (user && this.isCurrentUser(user.id)) {
          classes += ' cv-user-reserved';
        }
        return {
          id: row.id,
          startDate: startDate.toISO(),
          title: `${capitalize(row.shiftKind)} (${period}): ` +
                 `${this.$options.filters.showUserInfo(user)} ` +
                 `${this.$options.filters.showInstInfo(institute)}`,
          classes,
          assoId: row.assoId,
          shiftKind: get(ShiftKind, [ row.shiftKind, 'alias' ]),
          association,
          user,
          institute
        };
      });
    }
  },
  watch: {
    '$route.query': function(query, old) {
      if (!isEqual(query, old)) {
        this.fetch();
      }
    },
    /** @this {Instance} */
    showDate() {
      this.pushQuery(this.showDate);
    },
    shifts(value) {
      if (isArray(value) && value.length > 0) {
        const assoIds = uniq(map(this.shifts, (shift) => shift.assoId));
        sources.associationsCache.fetchMore(assoIds);
      }
    },
    associations(value) {
      if (isObject(value) && keys(value).length > 0) {
        sources.usersCache.fetchMore(map(this.associations, 'userId'));
        sources.institutesCache.fetchMore(map(this.associations, 'institute'));
      }
    }
  },
  /** @this {Instance} */
  async mounted() {
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
    this.fetch();
  },
  methods: {
    // -------------------- //
    //       FETCHING
    // -------------------- //

    /** @this {Instance} */
    loadRoute() {
      this.showDate = new Date(this.year, this.month, 1);
    },
    /**
     * @this {Instance}
     * @param  {boolean} [nocache=false]
     */
    fetch(nocache = false) {
      this.loadRoute();
      const query = {
        date: {
          '$gte': this.getGenevaDateTime(this.showDate).minus({ month: 1}).endOf('month').toMillis(),
          '$lte': this.getGenevaDateTime(this.showDate).endOf('month').toMillis()
        }
      };
      sources.shifts.fetch({ max: 200, query }, nocache);
    },

    // -------------------- //
    //       GETTERS
    // -------------------- //

    /**
     * @this {Instance}
     * @param {number} id
     */
    isCurrentUser(id) {
      return get(this.users, [ id, 'login' ]) === this.username;
    },
    /**
     * @this {Instance}
     * @param {number} assoId
     */
    getAssociation(assoId) {
      return get(this.associations, [ assoId ]);
    },
    /**
     * @this {Instance}
     * @param {number} userId
     */
    getUser(userId) {
      return get(this.users, [ userId ]);
    },
    /**
     * @this {Instance}
     * @param {number} instId
     */
    getInstitute(instId) {
      return get(this.institutes, [ instId ]);
    },

    // -------------------- //
    //       ACTIONS
    // -------------------- //

    /**
     * @this {Instance}
     * @param {number} shiftId
     * @param {string | null} username
     */
    // eslint-disable-next-line complexity
    async doAssign(shiftId, username) {
      if (this.actionProcessing) { return; }
      this.actionProcessing = true;
      let refetch = false;

      try {
        // Check shift exist
        const shift = await this._retrieveShift(shiftId);
        if (!shift) { // Should never happen
          refetch = true;
          throw "Shift slot not found.";
        }

        // Admin Mode
        if (this.isAdmin) {
          refetch = true;
          await this.doAdminAction(shift, username);
        }
        // Normal user mode
        else {
          // Get related user association
          const operYear = new Date(shift.date).getFullYear();
          const association = await this._retrieveAssociation(operYear, this.username);
          if (!association) {
            throw `Missing associations for user ${this.username} for operational year ${operYear}.`;
          }
          refetch = true;
          if (username) { // ASSIGN action
            if (!isNil(shift.assoId)) { throw 'This slot is not available anymore.'; }
            await sources.shifts.put(shift.id, { assoId: association.id }, /* isAdmin */ false);
          }
          else { // FREE action
            if (shift.assoId !== association.id) { throw 'you can\'t free this slot.'; }
            await sources.shifts.put(shift.id, { assoId: null }, /* isAdmin */ false);
          }
        }
      }
      catch (err) {
        logger.error(err);
      }
      finally {
        if (refetch) { this.fetch(true); }
        this.actionProcessing = false;
      }
    },

    /**
     * @this {Instance}
     * @param {any} shift
     * @param {string | null} username
     */
    async doAdminAction(shift, username) {
      // FREE Action
      if (isNil(username)) {
        await this.executeAdminAction(null, shift, null);
        return;
      }

      if (!includes(values(SpecialUsers), username)) {
        throw 'this username is not allowed';
      }

      // Take the special association for Reserved or Cancelled
      const operYear = new Date(shift.date).getFullYear();
      const association = await this._retrieveAssociation(operYear, username);
      if (!association) {
        throw `Missing special association for user ${username}. Please create it.`;
      }
      return this.executeAdminAction(association.id, shift, username);
    },

    /**
     * @this {Instance}
     * @param {number | null} assoId to set
     * @param {any} shift
     * @param {string | null} username
     */
    async executeAdminAction(assoId, shift = null, username = null) {
      // Show Confirmation only if shift is assigned (so Free, force-reserve and force-cancel)
      let doAction = true;
      if (shift.assoId) {
        const action = isNil(username) ? "Free" : username;
        doAction = await this._showConfirmDialog(action, shift, username).catch(() => false);
      }
      if (doAction) {
        await sources.shifts.put(shift.id, { assoId: assoId });
        return true;
      }
    },

    /**
     * @this {Instance}
     * @param {number} shiftId
     */
    async _retrieveShift(shiftId) {
      // Retrieve again the shift from db in order to avoid concurrency
      return sources.shifts.get({ max: 1, query: { id: shiftId } })
      .then((res) => get(res, [ 0 ]));
    },

    /**
     * @this {Instance}
     * @param {number} operYear
     * @param {string} username
     */
    async _retrieveAssociation(operYear, username) {
      const query = {};
      let queryField = 'login';
      if (username === SpecialUsers.CANCELLED || username === SpecialUsers.RESERVED) {
        // This because special users have empty login field.
        queryField = 'firstName';
      }
      set(query, queryField, username);
      const userId = await sources.users.get({ max: 1, query })
      .then((res) => get(res, [ 0, 'id' ]));

      if (!userId) { return undefined; }

      return sources.associations.getRelations('users',
        { id: userId }, { max: 1, query: { operYear } })
      .then((res) => get(res, [ 0 ]));
    },

    // -------------------- //
    //       DIALOGS
    // -------------------- //

    /**
     * @this {Instance}
     * @param {string} action
     * @param {any} shift
     * @param {string | null} username
     * @return {Promise<boolean>}
     */
    async _showConfirmDialog(action, shift = null, username = null) {
      this.dialogInfo = { action, shift, username };
      const res = /** @type boolean */ (await this.$refs.confirmDialog.request());
      this.dialogInfo = null;
      return res;
    },
    /** @this {Instance} */
    async showDuePointsModal() {
      return this.$refs.shiftPointsDialog.request().catch(noop);
    },

    // -------------------- //
    //       MISC
    // -------------------- //

    /**
     * @this {Instance}
     * @param {number | Date} date
     * @returns DateTime
     */
    getGenevaDateTime(date) {
      if (isNumber(date)) {
        return DateTime.fromMillis(date, { zone: 'Europe/Paris' });
      }
      else {
        return DateTime.fromISO(date.toISOString(), { zone: 'Europe/Paris' });
      }
    },

    /**
     * @this {Instance}
     * @param {number} d
     * @param {number=} h
     * @param {number=} m
     * @returns Date
     */
    thisMonth(d, h = 0, m  = 0) {
      const t = new Date();
      return new Date(t.getFullYear(), t.getMonth(), d, h, m);
    },
    /**
     * @this {Instance}
     * @param {Date} d
     */
    setShowDate(d) {
      this.pushQuery(d);
    },
    /**
     * @this {Instance}
     * @param {Date} d
     */
    pushQuery(d) {
      this.addQuery({
        year: d.getFullYear().toString(),
        month: (d.getMonth() + 1).toString().padStart(2, '0')
      });
    },
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value) {
      if (this.canBookShift) {
        this.inEdit = value;
      }
    }
  }
});
export default component;
