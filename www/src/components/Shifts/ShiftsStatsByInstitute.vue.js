/* eslint-disable max-lines */
// @ts-check

import Vue from 'vue';
import { mapState } from 'vuex';
import { sources } from '../../store';
import InstituteLink from '../Institutes/InstituteLink.vue';
import UserLink from '../Users/UserLink.vue';
import {
  concat,
  forEach, get, isEmpty, isNil, map, omitBy, orderBy,
  pickBy,
  sumBy, transform, uniq, values } from 'lodash';
import { ShiftKindType, SpecialUsers } from '../../Consts';
import d from 'debug';

const debug = d('app:shifts:stats');

class StatsRow {
  /**
   * @param {number} instId
   * @param {string} instAlias
   */
  constructor(instId, instAlias) {
    this.instId = instId;
    this.instAlias = instAlias;
    this.isSpecialInst = false; // used for cancelled/reserved
    this.persons = 0;
    this.totalShifts = 0;
    this.totalOnCall = 0;
    this.pointsShifts = 0;
    this.pointsOnCall = 0;
    this.duePoints = 0;
    this.coverage = '0';
  }

  /**
   * @param {number} points
   * @param {Boolean} isOnCall
   */
  addPoints(points, isOnCall) {
    if (isOnCall) {
      this.totalOnCall++;
      this.pointsOnCall += points;
    }
    else {
      this.totalShifts++;
      this.pointsShifts += points;
    }
    // Update coverage
    if (!this.isSpecialInst) {
      this.coverage = ((this.pointsShifts / this.duePoints) * 100).toFixed(2);
    }
  }
}

/**
 * @typedef {V.Instance<typeof component, V.ExtVue<any, any>>} Instance
 */

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'ShiftsStatsByInstitute',
  components: { InstituteLink, UserLink },
  filters: {
    /**
     * @param {number} value
     */
    counter: (value) => {
      if (value === 0) { return '-'; }
      else { return value; }
    }
  },
  props: {
    year: { type: Number, default: new Date().getFullYear() },
    all: { type: Boolean, default: false }
  },
  /**
   * @return {{
   *   reservedUserId: number,
   *   cancelledUserId: number,
   *   ntofInstituteId: number,
   * }}
   */
  data() {
    return {
      reservedUserId: 0,
      cancelledUserId: 0,
      ntofInstituteId: 0
    };
  },
  computed: {
    .../** @type {{ shiftLoading(): boolean, shifts(): any }} */(mapState('shifts', {
      shiftLoading: (state) => state.loading,
      shifts(state) { return get(state, [ 'rows' ]); }
    })),
    .../** @type {{ pointsLoading(): boolean, pointsShifts(): Array<AppStore.PointsShifts> }} */(mapState('pointsShifts', {
      pointsLoading: (state) => state.loading,
      pointsShifts(state) { return get(state, [ 'rows' ]); }
    })),
    .../** @type {{ duePointsLoading(): boolean, duePoints(): Array<AppStore.DuePoints> }} */(mapState('duePoints', {
      duePointsLoading: (state) => state.loading,
      duePoints(state) { return get(state, [ 'rows' ]); }
    })),
    .../** @type {{ institutes(): any }} */(mapState('institutesCache', { institutes: 'db' })),
    .../** @type {{ associations(): any }} */(mapState('associationsCache', { associations: 'db' })),
    .../** @type {{ users(): any }} */(mapState('usersCache', { users: 'db' })),
    /** @return {boolean} */
    loading() {
      return this.shiftLoading || this.pointsLoading || this.duePointsLoading;
    },
    /** @return {{ [shkday: string]: AppStore.PointsShifts }} */
    pointsByShiftKndAndDay() {
      if (isEmpty(this.pointsShifts)) { return {}; }
      return transform(this.pointsShifts, (/** @type {{ [shkday: string]: AppStore.PointsShifts }} */res, ps) => {
        res[`${ps.shiftKind}-${ps.day}`] = ps;
      }, {});
    },
    /** @return {{ [idInst: string]: AppStore.DuePoints }} */
    duePointsByInst() {
      if (isEmpty(this.duePoints)) { return {}; }
      return transform(this.duePoints, (/** @type {{ [idInst: string]: AppStore.DuePoints }} */res, dp) => {
        res[dp.institute] = dp;
      }, {});
    },
    /** @return {boolean} */
    isCurrentYear() {
      return this.year === new Date().getFullYear();
    },
    /**
     * @return {{ instStats: Array<StatsRow>, onCallStats: Array<{ userId: number, signed: number }> }}
     */
    stats() {
      const resStats = {
        instStats: /** @type Array<StatsRow> */([]),
        onCallStats: /** @type Array<{ userId: number, signed: number }> */([])
      };

      if (isEmpty(this.associations) ||
          isEmpty(this.users) ||
          isEmpty(this.institutes) ||
          isEmpty(this.duePointsByInst) ||
          isEmpty(this.pointsByShiftKndAndDay)) {
        return resStats; // Still loading from db
      }

      /** @type {{ [id: string]: StatsRow }} */
      const instStats = transform(this.duePoints, (ret, dp) => {
        const inst = this.getInstitute(dp.institute);
        if (!inst) {
          debug('unknown institute in duePoints: %s', dp.institute);
          return;
        }
        else if (dp.shiftPoints === 0 && inst.id !== this.ntofInstituteId) {
          debug('institute has zero shiftPoints: %s', inst.alias);
          return;
        }

        const row = ret[inst.id] = new StatsRow(inst.id, inst.alias);
        row.isSpecialInst = inst.id === this.ntofInstituteId;
        row.persons = dp.nUsers;
        row.duePoints = dp.shiftPoints;
      }, /** @type {{ [id: string]: StatsRow }} */ ({}));
      /** @type {{ [userId: number]: { userId: number, instAlias: string, signed: number } }} */
      const onCallStats = {};

      // Process stats
      // eslint-disable-next-line complexity
      forEach(this.shifts, (shift) => {
        if (isNil(shift.assoId)) { return; } // Empty shift
        const association = this.getAssociation(shift.assoId);
        if (isNil(association)) { return false; } // Stop calcuilating, wait for new data coming from db
        const row = instStats[association.institute];
        if (isNil(row)) {
          debug('unknown institute: %s', association.institute);
          return;
        }
        const day = (new Date(shift.date).getDay() + 6) % 7; // Starts Monday with 0
        const points = get(this.pointsByShiftKndAndDay, [ `${shift.shiftKind}-${day}`, 'value' ]);
        const isOnCall = shift.shiftKind === ShiftKindType.LEADER;
        row.addPoints(points, isOnCall);
        if (isOnCall) {
          if (!onCallStats[association.userId]) {
            onCallStats[association.userId] = {
              userId: association.userId, instAlias: row.instAlias, signed: 1
            };
          }
          else {
            onCallStats[association.userId].signed++;
          }
        }
      });

      const specialInstStats = pickBy(values(instStats), (r) => r.isSpecialInst);
      const normalInstStats = orderBy(omitBy(values(instStats), (r) => r.isSpecialInst), [ 'instAlias' ], [ 'asc' ]);

      resStats.instStats = concat(values(specialInstStats), normalInstStats);
      resStats.onCallStats = orderBy(values(onCallStats), [ 'signed' ], [ 'desc' ]);
      return resStats;
    },
    /**
     * @return {{
     *   sumPersons: number,
     *   sumTotalShifts: number,
     *   sumPointsShifts: number,
     *   sumPointsOnCall: number,
     *   sumDuePoints: number,
     *   coverage: string
     * } | null }
     */
    totalRow() {
      if (isEmpty(this.stats.instStats)) { return null; }
      const instStats = this.stats.instStats;
      const total = {
        sumPersons: sumBy(instStats, 'persons'),
        sumTotalShifts: sumBy(instStats, 'totalShifts'),
        sumPointsShifts: sumBy(instStats, 'pointsShifts'),
        sumPointsOnCall: sumBy(instStats, 'pointsOnCall'),
        sumDuePoints: sumBy(instStats, 'duePoints'),
        coverage: '0'
      };
      total.coverage = (((total.sumPointsShifts) / total.sumDuePoints) * 100).toFixed(2);
      return total;
    }
  },
  watch: {
    year() {
      this.fetch(true);
    },
    all() {
      this.fetch(true);
    },
    shifts(value) {
      if (!isEmpty(value)) {
        const assoIds = uniq(map(this.shifts, (shift) => shift.assoId));
        sources.associationsCache.fetchMore(assoIds);
      }
    },
    associations(value) {
      if (!isEmpty(value)) {
        sources.usersCache.fetchMore(map(this.associations, 'userId'));
      }
    }
  },
  async beforeMount() {
    await this.fetchSpecialUsers();
  },
  /** @this {Instance} */
  mounted() {
    this.fetch(true);
  },
  methods: {
    // -------------------- //
    //       FETCHING
    // -------------------- //
    async fetchSpecialUsers() {
      // Retrieve cancelled and reserved users
      this.reservedUserId = await sources.users.get({ max: 1, query: { firstName: SpecialUsers.RESERVED } })
      .then((res) => get(res, [ 0, 'id' ]));
      this.cancelledUserId = await sources.users.get({ max: 1, query: { firstName: SpecialUsers.CANCELLED } })
      .then((res) => get(res, [ 0, 'id' ]));
      this.ntofInstituteId = await sources.institutes.get({ max: 1, query: { alias: SpecialUsers.NTOFALIAS } })
      .then((res) => get(res, [ 0, 'id' ]));
    },
    /**
     * @this {Instance}
     * @param  {boolean} [nocache=false]
     */
    fetch(nocache = false) {
      // Fetch shift points for this year
      const paramsSP = { query: { operYear: this.year } };
      sources.pointsShifts.fetch(paramsSP, nocache);
      // Fetch Due Points for relative year
      const paramsDP = { query: { operYear: this.year } };
      sources.duePoints.fetch(paramsDP, nocache);
      // Fetch all institutes
      sources.institutesCache.fetchAll();
      // Fetch Shifts
      const fromTime = new Date(this.year, 0).getTime();
      let toTime = new Date(this.year + 1, 0).getTime();
      if (this.year === new Date().getFullYear() && !this.all) {
        toTime = new Date().getTime();
      }
      const query = {
        date: { '$gte': fromTime, '$lte': toTime }
      };
      sources.shifts.fetch({ max: 1500, query }, nocache);
    },
    /**
     * @this {Instance}
     * @param {number} timestamp
     * @param { 'L' | 'N' | 'M' | 'A' } shiftAlias
     */
    isWeekend(timestamp, shiftAlias) {
      var day = new Date(timestamp).getDay();
      // 1 -> Monday "night", 6 -> Saturday, 0 -> Sunday
      return (day === 1 && shiftAlias === 'N') || (day === 6) || (day === 0);
    },
    /**
     * @this {Instance}
     * @param {number} assoId
     */
    getAssociation(assoId) {
      return get(this.associations, [ assoId ]);
    },
    /**
     * @this {Instance}
     * @param {number} userId
     */
    getUser(userId) {
      return get(this.users, [ userId ]);
    },
    /**
     * @this {Instance}
     * @param {number} instId
     */
    getInstitute(instId) {
      return get(this.institutes, [ instId ]);
    }
  }
});
export default component;
