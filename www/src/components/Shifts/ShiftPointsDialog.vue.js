// @ts-check
import Vue from 'vue';
import ShiftPoints from '../OperationPeriods/ShiftPoints.vue';
import ShiftDuePoints from '../OperationPeriods/ShiftDuePoints.vue';

/**
 * @typedef {V.Instance<typeof component, V.ExtVue<any, any>>} Instance
 */
const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'ShiftPointsDialog',
  components: { ShiftPoints, ShiftDuePoints },
  props: {
    year: { type: Number, required: true }
  },
  methods: {
    /**
     * @this {Instance}
     */
    async request() {
      return this.$refs.dialog.request();
    }
  }
});
export default component;
