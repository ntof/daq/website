// @ts-check

import Vue from 'vue';
import { mapGetters, mapState } from 'vuex';
import { sources } from '../../store';
import { BaseLogger as logger } from '@cern/base-vue';
import UserLink from '../Users/UserLink.vue';
import InstituteLink from '../Institutes/InstituteLink.vue';
import {
  concat,
  first, forEach, get, isEmpty, isNil,
  map, omitBy, pickBy, sortBy, transform, values } from 'lodash';
import { ShiftKind, SpecialUsers } from '../../Consts';
import d from 'debug';

const debug = d('app:shifts:stats');

/**
* @this {Instance}
* @param {number} year
* @param {number} day weekday 0-6
* @param { 'L' | 'N' | 'M' | 'A' } shiftAlias
*/
const isWeekend = (year, day, shiftAlias) => {
  // 0 -> Monday "night" starting from 2016, 5 -> Saturday, 6 -> Sunday
  return (year >= 2016 && (day === 0 && shiftAlias === 'N')) || (day === 5) || (day === 6);
};

class ShiftCounters {
  constructor() {
    this.L = 0;
    this.N = 0;
    this.M = 0;
    this.A = 0;
  }

  /**
   * @param { 'L' | 'N' | 'M' | 'A' } shiftAlias
   * @param {number} val
   */
  add(shiftAlias, val = 1) {
    this[shiftAlias] += val;
  }
}

class TotalCounters {
  constructor() {
    this.shifts = 0;
    this.oncall = 0;
  }

  /**
   * @param { 'shifts' | 'oncall' } shiftAlias
   * @param {number} val
   */
  add(shiftAlias, val = 1) {
    this[shiftAlias] += val;
  }
}

class StatsRow {
  /**
   * @param {number} userId
   * @param {string} firstName
   * @param {string} lastName
   */
  constructor(userId, firstName, lastName) {
    this.userId = userId;
    this.firstName = firstName; // Used for ordering
    this.lastName = lastName; // Used for ordering
    this.isSpecialUser = false; // Used for cancelled and reserved
    /** @type {string|null} */
    this.instituteId = null;
    this.week = new ShiftCounters();
    this.weekend = new ShiftCounters();
    this.total = new TotalCounters();
    this.points = new TotalCounters();
  }

  /**
   * @param {number} date
   * @param {string} shiftKind
   * @param {{[id: string]: number}} points
   */
  addStats(date, shiftKind, points) {
    const dateObj = new Date(date);
    var weekday = (dateObj.getDay() + 6) % 7; // Starts Monday with 0
    const shiftAlias = get(ShiftKind, [ shiftKind, 'alias' ]);
    const relatedPoins = get(points, `${shiftAlias}-${weekday}`, 0);
    const weekOrEnds = isWeekend(dateObj.getFullYear(), weekday, shiftAlias) ? this.weekend : this.week;
    weekOrEnds.add(shiftAlias);
    // Add to Total
    if (shiftAlias === 'L') { this.total.add('oncall'); }
    else { this.total.add('shifts'); }
    // Add Points
    if (shiftAlias === 'L') { this.points.add('oncall', relatedPoins); }
    else { this.points.add('shifts', relatedPoins); }
  }
}

/**
 * @typedef {V.Instance<typeof component, V.ExtVue<any, any>>} Instance
 */

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'ShiftsStatsByUser',
  components: { UserLink, InstituteLink },
  filters: {
    /**
     * @param {{ firstName: string; lastName: string; } | undefined } userObj
     */
    showUserInfo: (userObj) => {
      if (!userObj) { return ''; }
      return `${first(userObj.firstName)}. ${userObj.lastName}`;
    },
    /**
     * @param {number} value
     */
    counter: (value) => {
      if (value === 0) { return '-'; }
      else { return value; }
    }
  },
  props: {
    year: { type: Number, default: new Date().getFullYear() },
    all: { type: Boolean, default: false }
  },
  /**
  * @this {Instance}
  * @return {{
  *  points: { [id: string]: number }
  * }}
  */
  data() {
    return {
      points: {}
    };
  },
  computed: {
    .../** @type {{ username(): string }}  */(mapGetters([ 'username' ])),
    .../** @type {{ shiftLoading(): boolean, shifts(): any }} */(mapState('shifts', {
      shiftLoading: (state) => state.loading,
      shifts(state) { return get(state, [ 'rows' ]); }
    })),
    .../** @type {{ associations(): any }} */(mapState('associations', { associations: 'db' })),
    .../** @type {{ users(): any }} */(mapState('usersCache', { users: 'db' })),
    .../** @type {{ institutes(): any }} */(mapState('institutesCache', { institutes: 'db' })),
    /** @return {boolean} */
    loading() {
      return this.shiftLoading;
    },
    /** @return {boolean} */
    isCurrentYear() {
      return this.year === new Date().getFullYear();
    },
    /** @return Array<StatsRow> */
    stats() {
      if (isEmpty(this.points) ||
          isEmpty(this.shifts) ||
          isEmpty(this.associations) ||
          isEmpty(this.users) ||
          isEmpty(this.institutes)) {
        return []; // Still loading from db
      }

      /** @type {{ [id: string]: StatsRow }} */
      const assoStats = transform(this.associations, (ret, asso) => {
        const user = this.getUser(asso.userId);
        if (!user) {
          debug('user not found for association: %i', asso.id);
          return;
        }

        const row = ret[asso.id] =
          new StatsRow(user.id, user.firstName, user.lastName);
        row.instituteId = asso.institute;
        if (user.firstName === SpecialUsers.CANCELLED ||
          user.firstName === SpecialUsers.RESERVED) {
          row.isSpecialUser = true;
        }
      }, /** @type {{ [id: string]: StatsRow }} */ ({}));

      forEach(this.shifts, (shift) => {
        if (isNil(shift.assoId)) { return; }

        const row = get(assoStats, shift.assoId, null);
        if (!row) {
          if (!this.getAssociation(shift.assoId)) {
            // don't log ignored assoc (cancelled, reserved ...)
            debug('unknown association: %i', shift.assoId);
          }
          return;
        }
        row.addStats(shift.date, shift.shiftKind, this.points);
      });

      const specialUsersStats = pickBy(values(assoStats), (r) => r.isSpecialUser);
      const normalUserStats = sortBy(omitBy(values(assoStats), (r) => r.isSpecialUser), [ 'lastName', 'firstName' ]);
      return concat(values(specialUsersStats), normalUserStats);
    }
  },
  watch: {
    year() {
      this.fetch(true);
    },
    all() {
      this.fetch(true);
    },
    associations(value) {
      if (!isEmpty(value)) {
        sources.usersCache.fetchMore(map(this.associations, 'userId'));
      }
    }
  },
  /** @this {Instance} */
  mounted() {
    this.fetch(true);
  },
  methods: {
    // -------------------- //
    //       FETCHING
    // -------------------- //

    /**
     * @this {Instance}
     * @param  {boolean} [nocache=false]
     */
    async fetch(nocache = false) {
      this.points = {};
      await this.retrievePoints();
      sources.institutesCache.fetchAll();
      // Fetch Shifts
      const fromTime = new Date(this.year, 0).getTime();
      let toTime = new Date(this.year + 1, 0).getTime();
      if (this.isCurrentYear && !this.all) {
        toTime = new Date().getTime();
      }
      const query = {
        date: { '$gte': fromTime, '$lte': toTime }
      };
      sources.shifts.fetch({ max: 2000, query }, nocache);
      sources.associations.fetch({ max: 2000, query: { operYear: this.year } },
        nocache);
    },
    /** @this {Instance} */
    async retrievePoints() {
      const pointsShifts = await sources.pointsShifts.get({ max: 50, query: { operYear: this.year } });
      // Check points exist. If not, raise error
      if (isEmpty(pointsShifts)) {
        logger.error(`Missing shift points for operational year ${this.year}.`);
        return;
      }
      // It will be an "hashmap" like {shift-alias}-{weekday} => points
      this.points = transform(pointsShifts, (/** @type {{ [id: string]: number }} */ res, pointObj) => {
        const alias = get(ShiftKind, [ pointObj.shiftKind, 'alias' ]);
        res[`${alias}-${pointObj.day}`] = pointObj.value;
      }, {});
    },
    /**
     * @this {Instance}
     * @param {number} assoId
     */
    getAssociation(assoId) {
      return get(this.associations, [ assoId ]);
    },
    /**
     * @this {Instance}
     * @param {number} userId
     */
    getUser(userId) {
      return get(this.users, [ userId ]);
    },
    /**
     * @this {Instance}
     * @param {number} instId
     */
    getInstitute(instId) {
      return get(this.institutes, [ instId ]);
    }
  }
});
export default component;
