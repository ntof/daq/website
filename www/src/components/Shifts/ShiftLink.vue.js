// @ts-check
import Vue from 'vue';

export default Vue.extend({
  name: 'ShiftLink',
  props: {
    shift: { type: Object, default: null, required: true }
  },
  methods: {
    getShiftLink() {
      const date = new Date(this.shift.date);
      return `/shifts?year=${date.getFullYear()}&month=${date.getMonth() + 1}`;
    },
    getDate() {
      return `${new Date(this.shift.date).toDateString()}`;
    }
  }
});
