// @ts-check

import { find, first, get, includes, isNil, isNumber, map, toNumber } from 'lodash';
import Vue from 'vue';
import { mapGetters, mapState } from 'vuex';
import { BaseKeyboardEventMixin as KeyboardEventMixin } from '@cern/base-vue';
import { sources } from '../../store';
import RouteUtilsMixin from '../../mixins/RouteUtilsMixin';
import InstitutesList from '../Institutes/InstitutesList.vue';
import InstituteLink from '../Institutes/InstituteLink.vue';
import ShiftsList from '../Shifts/ShiftsList.vue';
import { createPutParam, userDesc } from '../../interfaces/database';

/**
 * @typedef {{ }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> &
 *   V.Instance<ReturnType<KeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin>
 * } Instance
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'UserInfo',
  components: { InstitutesList, InstituteLink, ShiftsList },
  mixins: [ KeyboardEventMixin({ local: false }), RouteUtilsMixin ],
  /**
   * @return {{ inEdit: boolean, operYear: number }}
   */
  data() {
    return {
      inEdit: get(this.$route, [ 'meta', 'isAdmin' ]),
      operYear: (new Date()).getFullYear()
    };
  },
  computed: {
    /** @return {number} */
    id() { return toNumber(get(this.$route, [ 'params', 'id' ])); },
    .../** @type {{ username(): string }} */(mapGetters([ 'username' ])),
    .../** @type {{ showKeyHints(): boolean }} */(mapState('ui', [ 'showKeyHints' ])),
    .../** @type {{ loading(): boolean, user(): any }} */(mapState('users', {
      loading: (state) => state.loading,
      user(state) { return get(state, [ 'db', this.id ]); }
    })),
    /**
     * @this {Instance}
     * @return {boolean}
     */
    canEdit() {
      return this.isAdmin || get(this.user, [ 'login' ]) === this.username;
    },
    .../** @type {{
      institutesLoading(): boolean,
      institutes(): { [id:string]: AppStore.Institute },
      institutesOptions(): AppStore.Institute[]
    }} */(mapState('institutes', {
      institutes: 'db', institutesLoading: 'loading', institutesOptions: 'rows'
    })),
    .../** @type {{ associationLoading(): boolean, association(): any }} */(mapState('associations', {
      associationLoading: (state) => state.loading,
      association(state) { return find(state.rows, { userId: this.id, operYear: this.operYear }); }
    })),
    /** @return {number} */
    instituteId() {
      return get(this.association, [ 'institute' ]);
    },
    /** @return {AppStore.Institute} */
    institute() {
      return get(this.institutes, [ this.instituteId ]);
    }
  },
  watch: {
    id: {
      immediate: true,
      handler() {
        this.fetch();
      }
    },
    association() {
      if (this.association) {
        sources.institutesCache.fetchMore([ this.instituteId ]);
      }
    }
  },
  /** @this {Instance} */
  mounted() {
    this.fetchInstitutes();
    this.onKey('ctrl-s-keydown', this.doSubmit);
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
  },
  methods: {
    /**
     * @param  {boolean} [nocache=false]
     */
    async fetch(nocache = false) {
      sources.users.fetch({ max: 1, query: { id: this.id } }, nocache);
      const operYear = new Date().getFullYear();
      const periods = await sources.operationPeriods.get({ sort: 'year:desc' })
      .then((res) => map(res, 'year'));
      if (includes(periods, operYear)) {
        this.operYear = operYear;
      }
      else {
        this.operYear = /** @type number*/(first(periods));
      }
      sources.associations.fetch({
        max: 1,
        query: {
          userId: this.id, operYear: this.operYear
        }
      }, nocache);
    },
    fetchInstitutes() {
      sources.institutes.fetch({
        query: { },
        sort: 'alias:asc'
      });
    },
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value) {
      if (this.canEdit) {
        this.inEdit = value;
      }
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doSubmit(event) {
      if (event) {
        event.preventDefault();
      }
      try {
        const params = createPutParam(this, this.user, userDesc);
        await sources.users.put(this.user.id, params, this.isAdmin);

        const instituteId = get(this.$refs, [ 'institute', 'editValue' ]);
        if (!isNumber(instituteId)) {
          if (!isNil(this.association)) {
            await sources.associations.delete(this.association.id, this.isAdmin);
          }
        }
        else if (this.association) {
          if (this.instituteId !== instituteId) {
            await sources.associations.put(this.association.id,
              { institute: instituteId }, this.isAdmin);
          }
        }
        else {
          await sources.associations.post({
            operYear: this.operYear, userId: this.user.id,
            institute: instituteId
          }, this.isAdmin);
        }

        this.inEdit = false;
        await this.fetch(true);
      }
      catch (e) { /* noop */ }
    }
  }
});
export default component;
