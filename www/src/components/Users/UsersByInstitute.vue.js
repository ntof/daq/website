// @ts-check

import { assign, get, map, set, toNumber } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';
import UserLink from './UserLink.vue';

import DBSearchMixin from '../../mixins/DBSearchMixin';
import { sources } from '../../store';

/**
 * @typedef {typeof import('../../mixins/RouteUtilsMixin').default} RouteUtilsMixin
 * @typedef {import('@cern/base-vue').BaseKeyboardEventMixin} KeyboardEventMixin
 * @typedef {V.Instance<component, V.ExtVue<options, {}>> &
 *   V.Instance<typeof DBSearchMixin> &
 *   V.Instance<ReturnType<KeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin>
 * } Instance
 */

const options = { source: 'users' };

const component = /** @type {V.Constructor<options, {}> } */ (Vue).extend({
  name: 'UsersByInstitute',
  components: { UserLink },
  mixins: [ DBSearchMixin ],
  props: {
    associations: { type: Object, default: null },
    maxRows: { type: Number, default: 40 }
  },
  ...options,
  /**
   * @return {{ operYear: number, institute: number|null }}
   */
  data() {
    return { operYear: (new Date()).getFullYear(), institute: null };
  },
  computed: {
    .../** @type {{
      institutes(): { [id:string]: any },
      institutesLoading(): boolean
    }} */(mapState('institutesCache', {
      institutes: 'db', institutesLoading: 'loading'
    }))
  },
  watch: {
    /** @this {Instance} */
    maxRows(v) { this.max = v; },
    /** @this {Instance} */
    associations() {
      this.fetch();
    }
  },
  methods: {
    /** @this {Instance} */
    loadRoute() {
      try {
        const query = this.$route.query;
        this.page = toNumber(get(query, 'page', 0));

        this.operYear = toNumber(get(query, 'year')) ||
          (new Date()).getFullYear();
      }
      catch (e) {
        console.log(e);
      }
    },
    /** @this {Instance} */
    fetch() {
      this.loadRoute();
      /** @type {any} */
      const params = {
        max: this.max, offset: this.max * this.page
      };
      this.prepareQuery(params);
      get(sources, this.$options.source).fetch(params);
      sources.institutesCache.fetchMore(map(this.associations, 'institute'));
    },
    /**
     * @this {Instance}
     * @param  {any} params
     * @return {any}
     */
    prepareQuery(params) {
      params = assign(params, {
        query: { },
        sort: 'lastName:asc,firstName:asc'
      });
      if (!this.isAdmin) {
        set(params.query, [ 'status' ], 1);
      }
      set(params.query, [ 'id', '$in' ], map(this.associations, 'userId'));
      return params;
    },
    /**
     * @param  {number} userId
     * @return {AppStore.Institute|null}
     */
    getUserInstitute(userId) {
      const asso = get(this.associations, userId);
      return get(this.institutes, [ get(asso, 'institute') ], null);
    }
  }
});

export default component;
