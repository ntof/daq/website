// @ts-check

import { assign, get, keyBy, map, set, toNumber } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';
import UserLink from './UserLink.vue';

import DBSearchMixin from '../../mixins/DBSearchMixin';
import { sources } from '../../store';

/**
 * @typedef {typeof import('../../mixins/RouteUtilsMixin').default} RouteUtilsMixin
 * @typedef {import('@cern/base-vue').BaseKeyboardEventMixin} KeyboardEventMixin
 * @typedef {V.Instance<component, V.ExtVue<options, {}>> &
 *   V.Instance<typeof DBSearchMixin> &
 *   V.Instance<ReturnType<KeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin>
 * } Instance
 */

const options = { source: 'users' };

const component = /** @type {V.Constructor<options, {}> } */ (Vue).extend({
  name: 'UsersAll',
  components: { UserLink },
  mixins: [ DBSearchMixin ],
  props: {
    maxRows: { type: Number, default: 40 }
  },
  ...options,
  /**
   * @return {{ operYear: number }}
   */
  data() {
    return { operYear: (new Date()).getFullYear() };
  },
  computed: {
    .../** @type {{
      associationsLoading(): boolean,
      associations(): { [id:string]: any }
    }} */(mapState('associations', {
      associationsLoading: (state) => state.loading,
      associations: (state) => keyBy(state.rows, 'userId')
    })),
    .../** @type {{
      institutes(): { [id:string]: any }
    }} */(mapState('institutesCache', {
      institutes: 'db', institutesLoading: 'loading'
    }))
  },
  watch: {
    /** @this {Instance} */
    maxRows(v) { this.max = v; },
    rows() { this.fetchAssociations(); },
    operYear() { this.fetchAssociations(); },
    associations() {
      sources.institutesCache.fetchMore(map(this.associations, 'institute'));
    }
  },
  methods: {
    /** @this {Instance} */
    loadRoute() {
      try {
        const query = this.$route.query;
        this.page = toNumber(get(query, 'page', 0));
        this.operYear = toNumber(get(query, 'year')) ||
          (new Date()).getFullYear();
      }
      catch (e) {
        console.log(e);
      }
    },
    /**
     * @this {Instance}
     * @param  {any} params
     * @return {any}
     */
    prepareQuery(params) {
      params = assign(params, {
        query: { },
        sort: 'lastName:asc,firstName:asc'
      });
      if (!this.isAdmin) {
        set(params.query, [ 'status' ], 1);
      }
      return params;
    },
    /**
     * @param  {number} userId
     * @return {AppStore.Institute|null}
     */
    getUserInstitute(userId) {
      const asso = get(this.associations, userId);
      return get(this.institutes, [ get(asso, 'institute') ], null);
    },
    /** @this {Instance} */
    fetchAssociations() {
      if (!this.rows) { return; }
      const query = { operYear: this.operYear };
      set(query, [ 'userId', '$in' ], map(this.rows, (r) => r.id));
      sources.associations.fetch({ query });
    }
  }
});

export default component;
