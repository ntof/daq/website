// @ts-check

import { get, isEmpty, isEqual, keyBy, noop, set,
  toNumber, toString } from 'lodash';
import Vue from 'vue';
import { mapGetters, mapState } from 'vuex';
import { BaseKeyboardEventMixin as KeyboardEventMixin } from '@cern/base-vue';
import OperYearSelector from '../OperYearSelector.vue';
import UsersAll from './UsersAll.vue';
import UsersByInstitute from './UsersByInstitute.vue';
import { sources } from '../../store';
import RouteUtilsMixin from '../../mixins/RouteUtilsMixin';

/**
 * @typedef {{
 *   operYearInput: V.Instance<typeof OperYearSelector>
 * }} Refs
 *
 * @typedef {V.Instance<component, V.ExtVue<{}, Refs>> &
 *   V.Instance<typeof RouteUtilsMixin> &
 *   V.Instance<ReturnType<KeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin>
 * } Instance
 */

const component = /** @type {V.Constructor<{}, Refs> } */ (Vue).extend({
  name: 'Users',
  components: {
    UsersAll, UsersByInstitute, OperYearSelector
  },
  mixins: [ KeyboardEventMixin({ local: false }), RouteUtilsMixin ],
  /**
   * @return {{
   *  max: number,
   *  operYear: number,
   *  institute: number|null,
   *  page: number
   * }}
   */
  data() {
    return {
      max: get(this.$store, [ 'state', 'ui', 'max' + this.$route.name ], 40),
      operYear: (new Date()).getFullYear(), institute: null, page: 0
    };
  },
  computed: {
    .../** @type {{ showKeyHints(): boolean }} */mapState('ui', {
      showKeyHints: (state) => state.showKeyHints
    }),
    .../** @type {{ username(): string, canEdit(): boolean }} */(mapGetters([ 'username', 'canEdit' ])),
    .../** @type {{ associationsLoading(): boolean, associations(): { [id:string]: any } }} */(mapState('associations', {
      associationsLoading: (state) => state.loading,
      associations: (state) => keyBy(state.rows, 'userId')
    })),
    .../** @type {{
      institutes(): { [id:string]: AppStore.Institute },
      institutesLoading(): boolean
      institutesOptions(): AppStore.Institute[]
    }} */(mapState('institutes', {
      institutes: 'db', institutesLoading: 'loading', institutesOptions: 'rows'
    })),
    .../** @type {{ usersLoading(): boolean } }} */(mapState('users', {
      usersLoading: 'loading'
    }))
  },
  watch: {
    '$route.query': function(query, old) {
      if (!isEqual(query, old)) {
        this.fetch();
      }
    }
  },
  /** @this {Instance} */
  async mounted() {
    this.onKey('ctrl-s-keydown', this.doSearch);

    await this.fetch();
    this.fetchInstitutes();
  },
  methods: {
    /** @this {Instance} */
    async loadRoute() {
      try {
        const query = this.$route.query;
        this.page = toNumber(get(query, 'page', 0));

        this.operYear = toNumber(get(query, 'year') || (new Date()).getFullYear());
        await this.$refs.operYearInput.setEditValue(this.operYear).catch(noop);

        const institute = get(query, 'institute');
        this.institute = isEmpty(institute) ? null : toNumber(institute);
        set(this.$refs, [ 'instituteInput', 'editValue' ], this.institute);
      }
      catch (e) {
        console.log(e);
      }
    },
    fetchInstitutes() {
      sources.institutes.fetch({
        query: { },
        sort: 'alias:asc'
      });
    },
    async fetch() {
      await this.loadRoute();
      if (!this.institute) { return; }

      const query = {
        operYear: get(this.$refs, [ 'operYearInput', 'editValue' ]) || (new Date()).getFullYear()
      };
      set(query, [ 'institute' ], this.institute);
      sources.associations.fetch({ query });
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    doSearch(event) {
      if (event) {
        event.preventDefault();
      }
      this.addQuery({
        year: toString(get(this.$refs, [ 'operYearInput', 'editValue' ])),
        institute: toString(get(this.$refs, [ 'instituteInput', 'editValue' ])),
        page: "0"
      });
    },
    /**
     * @this {Instance}
     * @param  {number} num
     */
    goToPage(num) {
      this.addQuery({ page: toString(num) });
    }
  }
});

export default component;
