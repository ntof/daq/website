// @ts-check
import Vue from 'vue';
import RouteUtilsMixin from '../../mixins/RouteUtilsMixin';

export default Vue.extend({
  name: 'UserLink',
  mixins: [ RouteUtilsMixin ],
  props: {
    user: { type: Object, default: null },
    id: { type: [ Number, String ], default: '-' }
  }
});
