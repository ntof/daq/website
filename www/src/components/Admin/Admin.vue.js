// @ts-check

import Vue from 'vue';

const component = Vue.extend({
  name: 'Admin'
});

export default component;
