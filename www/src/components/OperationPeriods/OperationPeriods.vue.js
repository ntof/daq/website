// @ts-check

import { assign, get, noop, toNumber } from 'lodash';
import Vue from 'vue';
import AddOperationPeriodDialog from './AddOperationPeriodDialog.vue';
import DBSearchMixin from '../../mixins/DBSearchMixin';

/**
 * @typedef {typeof import('../../mixins/RouteUtilsMixin').default} RouteUtilsMixin
 * @typedef {import('@cern/base-vue').BaseKeyboardEventMixin} KeyboardEventMixin
 * @typedef {{ addDialog: V.Instance<typeof AddOperationPeriodDialog> }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<options, Refs>> &
 *   V.Instance<typeof DBSearchMixin> &
 *   V.Instance<ReturnType<KeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin>
 * } Instance
 */

const options = { source: 'operationPeriods' };

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'OperationPeriods',
  components: { AddOperationPeriodDialog },
  mixins: [ DBSearchMixin ],
  ...options,
  methods: {
    /** @this {Instance} */
    loadRoute() {
      try {
        const query = this.$route.query;
        this.page = toNumber(get(query, 'page', 0));
      }
      catch (e) {
        console.log(e);
      }
    },
    /**
     * @this {Instance}
     * @param  {any} params
     * @return {any}
     */
    prepareQuery(params) {
      params = assign(params, {
        query: {},
        sort: 'year:desc'
      });
      return params;
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doAdd(event) {
      if (event) { event.preventDefault(); }
      await this.$refs.addDialog.request().catch(noop);
    }
  }
});
export default component;
