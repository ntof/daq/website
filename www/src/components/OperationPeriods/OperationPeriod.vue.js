// @ts-nocheck
import { get, isNil } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';
import moment from 'moment';
import OperationPeriodInfo from './OperationPeriodInfo.vue';
import ShiftPoints from './ShiftPoints.vue';
import ShiftDuePoints from './ShiftDuePoints.vue';

import { sources } from '../../store';

/**
 * @typedef {V.Instance<typeof component, V.ExtVue<any, any>>} Instance
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'OperationPeriod',
  components: { OperationPeriodInfo, ShiftPoints, ShiftDuePoints },
  computed: {
    /** @return {string} */
    year() { return get(this.$route, [ 'params', 'id' ]); },
    .../** @type {{ nOperationWeeks(): number }} */(mapState('nOperationWeeks', {
      nOperationWeeks(state) { return get(state, [ this.operationPeriod.year ]); }
    })),
    .../** @type {{ loading(): boolean, operationPeriod(): any }} */(mapState('operationPeriods', {
      loading: (state) => state.loading,
      operationPeriod(state) { return get(state, [ 'db', this.year ]); }
    }))
  },
  watch: {
    year: {
      immediate: true,
      handler() { this.fetch(); }
    },
    operationPeriod() {
      if (isNil(this.operationPeriod)) { return; }
      if (!isNil(this.nOperationWeeks)) { return; }
      // Calculate default number of weeks
      const start = moment(this.operationPeriod.start);
      const stop = moment(this.operationPeriod.stop);
      const daysDiff = stop.diff(start, 'day');
      const roundedNWeeks = Math.ceil(daysDiff / 7);
      this.$store.commit('nOperationWeeks/setWeeks', {
        year: this.operationPeriod.year, value: roundedNWeeks
      });
    }
  },
  methods: {
    /**
     * @param  {boolean} [nocache=false]
     */
    async fetch(nocache = false) {
      const id = get(this.$route, [ 'params', 'id' ]);
      const params = { max: 1, query: { year: id } };
      sources.operationPeriods.fetch(params, nocache);
    },
    /** @this {Instance} */
    async onOperYearUpdate() {
      await this.fetch(true);
    },
    /** @this {Instance} */
    onShiftPointsUpdate() {
      const shiftDuePoints = get(this.$refs, 'shiftDuePoints');
      if (shiftDuePoints) {
        shiftDuePoints.loadPointsShifts(false /* forceSubmit */);
      }
    }
  }
});
export default component;
