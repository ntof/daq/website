// @ts-check

import { toNumber } from 'lodash';
import moment from 'moment';
import { sources } from '../../store';
import Vue from 'vue';
import d from 'debug';

const debug = d('app:comp');

/**
 * @typedef {{
 *  dialog: V.Instance<typeof BaseVue.BaseDialog>,
 *  year: V.Instance<typeof BaseVue.BaseParamInput>,
 *  start: V.Instance<typeof BaseVue.BaseParamDate>,
 *  stop: V.Instance<typeof BaseVue.BaseParamDate>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>>} Instance
 */
const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'AddOperationYearDialog',
  /**
   * @return {{ startDateOK: boolean, stopDateOK: boolean }}
   */
  data() {
    return { startDateOK: true, stopDateOK: true };
  },
  computed: {
    /** @return {boolean} */
    disableSubmit() {
      return !this.startDateOK || !this.stopDateOK;
    }
  },
  methods: {
    /**
     * @this {Instance}
     */
    async request() {
      this.$refs.year.editValue = (new Date()).getFullYear().toString();
      try {
        if (!await this.$refs.dialog.request()) { return; }
        const id = await sources.operationPeriods.post({
          year: toNumber(this.$refs.year.editValue),
          start: toNumber(this.$refs.start.editTimestamp) * 1000,
          stop: toNumber(this.$refs.stop.editTimestamp) * 1000
        });
        this.$router.push('/admin/operationPeriods/' + id);
      }
      catch (err) {
        debug('doAdd error:', err);
      }
    },
    onYearChanged() {
      const year = moment([ toNumber(this.$refs.year.editValue) ]);
      if (year.isValid()) {
        this.$refs.start.editTimestamp = year.startOf('year').unix();
        this.checkStartDate();
        this.$refs.stop.editTimestamp = year.endOf('year').unix();
        this.checkStopDate();
      }
    },
    /**
     * @param {string} ref
     */
    checkStartDate() {
      this.startDateOK = this.checkDate(this.$refs.start);
      return this.startDateOK;
    },
    /**
     * @param {string} ref
     */
    checkStopDate() {
      this.stopDateOK =  this.checkDate(this.$refs.stop);
      return this.stopDateOK;
    },
    /**
     * @param {any} ref
     */
    checkDate(ref) {
      const year = moment([ toNumber(this.$refs.year.editValue) ]).year();
      const yearData = moment((ref.editTimestamp || 0) * 1000).year();
      if (yearData !== year) {
        ref.addError('year', 'Date must be within the operational year');
        return false;
      }
      else {
        ref.removeError('year');
        return true;
      }
    }
  }
});
export default component;
