// @ts-check
import { filter, forEach, get, isEmpty, isNil,
  map, omit, sortBy, sumBy, times, toNumber } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';
import {
  BaseKeyboardEventMixin as KeyboardEventMixin,
  BaseLogger as logger } from '@cern/base-vue';
import { sources } from '../../store';
import { ShiftKindType } from '../../Consts';
import { delay } from '@cern/prom';
import d from 'debug';

const debug = d('app:comp');

/**
 * @typedef {V.Instance<typeof component, V.ExtVue<any, any>> &
 *   V.Instance<ReturnType<KeyboardEventMixin>>
 * } Instance
 */

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'ShiftPoints',
  mixins: [ KeyboardEventMixin({ local: true }) ],
  props: {
    year: { type: Number, required: true },
    showLeaderPoints: { type: Boolean, default: true },
    editable: { type: Boolean, default: true }
  },
  /**
   * @return {{
   *   inEdit: boolean,
   *   processing: boolean
   *   dirtyValue: boolean
   *   ShiftKindType: typeof ShiftKindType
   * }}
   */
  data() {
    return {
      inEdit: false,
      processing: false,
      dirtyValue: false,
      ShiftKindType
    };
  },
  computed: {
    .../** @type {{ showKeyHints(): boolean }} */(mapState('ui', [ 'showKeyHints' ])),
    .../** @type {{ pointsLoading(): boolean, pointsShifts(): any }} */(mapState('pointsShifts', {
      pointsLoading: (state) => state.loading,
      pointsShifts(state) { return get(state, [ 'rows' ]); }
    })),
    /** @return {boolean} */
    loading() {
      return this.pointsLoading || this.processing;
    },
    /** @return {number} */
    totPointsPerWeek() {
      return sumBy(this.pointsShifts, (o) => toNumber(o.value));
    },
    /** @return {number} */
    totNonLeaderPointsPerWeek() {
      const nonLeaderP = filter(this.pointsShifts, (p) => p.shiftKind !== ShiftKindType.LEADER);
      return sumBy(nonLeaderP, (o) => toNumber(o.value));
    }
  },
  watch: {
    year() { this.fetch(); },
    pointsShifts() {
      if (isNil(this.pointsShifts)) { return; }
      if (isEmpty(this.pointsShifts) && this.editable) {
        this.createPoints();
      }
    }
  },
  /** @this {Instance} */
  mounted() {
    if (this.editable) {
      this.onKey('ctrl-s-keydown', this.doSubmit);
      this.onKey('e', () => this.setEdit(true));
      this.onKey('esc', () => this.setEdit(false));
    }
    this.fetch();
  },
  methods: {
    /**
     * @param  {boolean} [nocache=false]
     */
    async fetch(nocache = false) {
      const params = { max: 50, query: { operYear: this.year } };
      sources.pointsShifts.fetch(params, nocache);
    },
    /**
     * @this {Instance}
     */
    async createPoints() {
      try {
        debug('creating shift points');
        this.processing = true;
        const prevYearPoints = await this._getPrevYearData();
        /** @type Array<Partial<AppStore.PointsShifts>> */
        let newPoints = [];
        if (isEmpty(prevYearPoints)) {
          // no prev year, create empty one
          forEach(ShiftKindType, (shiftKind) => {
            forEach(times(7, Number), (day) => {
              newPoints.push({ day, shiftKind, operYear: this.year, value: 0 });
            });
          });
        }
        else {
          // Remove id and update operYear
          newPoints = map(prevYearPoints, (p) => {
            const newP = omit(p, 'id');
            newP.operYear = this.year;
            return newP;
          });
        }
        await sources.pointsShifts.post(newPoints);
        await this.fetch(true);
        debug('shift points created');
      }
      catch (e) { /* noop */ }
      finally {
        this.processing = false;
      }
    },
    /**
     * @this {Instance}
     * @param {string} shiftKind
     * @return {Array<Partial<AppStore.PointsShifts>>}
     */
    getRowsByShiftType(shiftKind) {
      return sortBy(filter(this.pointsShifts, (p) => p.shiftKind === shiftKind), 'day');
    },
    /**
     * @this {Instance}
     * @param {string} value
     * @param {AppStore.PointsShifts} row
     */
    onEdit(value, row) {
      if (toNumber(value) !== row.value) {
        this.dirtyValue = true;
      }
    },
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value) {
      this.inEdit = value;
    },
    /**
     * @this {Instance}
     */
    async copyPreviousYear() {
      try {
        this.processing = true;
        // get previous year
        const prevYearPoints = await this._getPrevYearData();
        if (isEmpty(prevYearPoints)) {
          logger.error("There is no configuration available for previous years");
          throw false;
        }
        // set editValues
        forEach(prevYearPoints, (p) => {
          const ref = get(this.$refs, [ `${p.shiftKind}-${p.day}`, 0 ]);
          if (ref) { ref.editValue = p.value; }
        });
      }
      catch (e) { /* noop */ }
      finally {
        this.processing = false;
      }
    },
    /**
     * @this {Instance}
     */
    async _getPrevYearData() {
      const query = { year: { '$lt': this.year } };
      const prevYear = await sources.operationPeriods.get({ max: 1, query, sort: 'year:desc' })
      .then((res) => get(res, [ 0, 'year' ], null));
      if (!prevYear) { return []; }
      return sources.pointsShifts.getRelations('operationPeriods', { operYear: prevYear }, { max: 50 });
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doSubmit(event) {
      if (event) {
        event.preventDefault();
      }
      try {
        this.processing = true;
        let refresh = false;
        forEach(this.pointsShifts, async (p) => {
          const ref = get(this.$refs, [ `${p.shiftKind}-${p.day}`, 0 ]);
          const editValue = get(ref, 'editValue');
          if (ref && p.value !== toNumber(editValue)) {
            refresh = true;
            await sources.pointsShifts.put(p.id, { value: editValue });
          }
        });
        // Oracle fix... it needs time to commit.
        if (refresh) {
          await delay(200);
          await this.fetch(true);
          // Emit update signal
          this.$emit('update');
        }
      }
      catch (e) { /* noop */ }
      finally {
        this.processing = false;
        this.setEdit(false);
        this.dirtyValue = false;
      }
    }
  }
});
export default component;
