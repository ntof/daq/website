/* eslint-disable max-lines */
// @ts-check
import { debounce, filter, first, forEach, get, isEmpty, isNil,
  noop,
  orderBy, set, sumBy, toNumber, transform, values } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';
import {
  BaseKeyboardEventMixin as KeyboardEventMixin,
  BaseLogger as logger } from '@cern/base-vue';
import InstituteLink from '../Institutes/InstituteLink.vue';
import ShiftDuePointsFormulaDialog from './ShiftDuePointsFormulaDialog.vue';
import { sources } from '../../store';
import { ShiftKindType } from '../../Consts';
import { delay } from '@cern/prom';
import d from 'debug';

const debug = d('app:comp');

/**
 * @typedef {V.Instance<typeof component, V.ExtVue<any, any>> &
 *   V.Instance<ReturnType<KeyboardEventMixin>>
 * } Instance
 */
/**
 * @param {number} value
 * @param {number} step
 */
const cCeil = (value, step) => {
  step = step || 1.0;
  var inv = 1.0 / step;
  return Math.ceil(value * inv) / inv;
};

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'ShiftDuePoints',
  components: { InstituteLink, ShiftDuePointsFormulaDialog },
  mixins: [ KeyboardEventMixin({ local: true }) ],
  props: {
    year: { type: Number, required: true },
    editable: { type: Boolean, default: true }
  },
  /**
   * @return {{
   *  inEdit: boolean,
   *  processing: boolean,
   *  dirtyValue: boolean,
   *  totNUsersInEdit: number,
   *  totDueShiftsInEdit: number,
   *  debounceUpdateShiftPoints: any,
   *  debounceLeaderOnEdit: any,
   *  pointsShifts: any,
   *  loadingPoints: boolean,
   *  valuesOutdated: boolean
   *  forceSubmit: boolean
   * }}
   */
  data() {
    return {
      inEdit: false,
      processing: false,
      dirtyValue: false,
      totNUsersInEdit: 0,
      totDueShiftsInEdit: 0,
      debounceUpdateShiftPoints: null,
      debounceLeaderOnEdit: null,
      pointsShifts: [],
      loadingPoints: false,
      valuesOutdated: false, // When we load from db but computation differs
      forceSubmit: false // Set by loadPointsShifts() to force submit when external update
    };
  },
  computed: {
    .../** @type {{ showKeyHints(): boolean }} */(mapState('ui', [ 'showKeyHints' ])),
    .../** @type {{ nOperationWeeks(): number }} */(mapState('nOperationWeeks', {
      nOperationWeeks(state) { return get(state, [ this.year ]); }
    })),
    .../** @type {{ duePointsLoading(): boolean, duePoints(): Array<AppStore.DuePoints> }} */(mapState('duePoints', {
      duePointsLoading: (state) => state.loading,
      duePoints(state) { return get(state, [ 'rows' ]); }
    })),
    .../** @type {{ institutesLoading(): boolean, institutes(): Array<AppStore.Institute> }} */(mapState('institutes', {
      institutesLoading: (state) => state.loading,
      institutes(state) { return get(state, [ 'db' ]); }
    })),
    /** @return {boolean} */
    loading() {
      return this.duePointsLoading || this.institutesLoading || this.processing || this.loadingPoints;
    },
    /** @return { Array<AppStore.DuePoints> } */
    activeDuePoints() {
      if (isEmpty(this.institutes)) { return []; }
      return filter(this.duePoints, (dp) => {
        const inst = get(this.institutes, dp.institute);
        return inst && inst.status === 1;
      });
    },
    /** @return { Array<Partial<AppStore.DuePoints>> } */
    sortedInstitutes() {
      return orderBy(values(this.institutes), [ 'status', 'alias' ], [ 'desc', 'asc' ]);
    },
    /** @return {number} */
    totalNonLeaderPoints() {
      if (isEmpty(this.pointsShifts)) { return 0; }
      const nonLeaderP = filter(this.pointsShifts, (p) => p.shiftKind !== ShiftKindType.LEADER);
      const totalWeekPoints = sumBy(nonLeaderP, 'value');
      return this.nOperationWeeks * totalWeekPoints;
    },
    /** @return {number} */
    sumUsers() {
      if (isEmpty(this.activeDuePoints)) { return 0; }
      return sumBy(this.activeDuePoints, (p) => toNumber(p.nUsers));
    },
    /** @return {number} */
    sumPoints() {
      if (isEmpty(this.activeDuePoints)) { return 0; }
      return sumBy(this.activeDuePoints, (p) => toNumber(p.shiftPoints));
    },
    /** @return {number} */
    duePointsPerUser() {
      return toNumber(((this.sumPoints || 0) / (this.sumUsers || 1)).toFixed(2));
    }
  },
  watch: {
    year() { this.fetch(); },
    duePoints(value) {
      if (isNil(value)) {
        this.setEdit(false); // It happens when shiftpoints changes
        return;
      }
      if (isEmpty(value) && this.editable) {
        this.createDuePoints();
      }
    },
    activeDuePoints(value) {
      if (isNil(value)) { return; }
      if (!isEmpty(value) && !this.duePointsLoading) {
        this.debounceUpdateShiftPoints();
      }
    },
    totalNonLeaderPoints(value) {
      if (isNil(value) || value === 0) { return; }
      // In this case we disable edit mode and force update.
      this.setEdit(false);
      this.debounceUpdateShiftPoints();
    },
    nOperationWeeks(value) {
      if (isNil(value)) { return; }
      this.debounceUpdateShiftPoints();
    }
  },
  /** @this {Instance} */
  async mounted() {
    if (this.editable) {
      this.onKey('ctrl-s-keydown', this.doSubmit);
      this.onKey('e', () => this.setEdit(true));
      this.onKey('esc', () => this.setEdit(false));
    }
    this.debounceUpdateShiftPoints = debounce(this.updateShiftPoints, 200);
    await this.loadPointsShifts();
    this.fetch();
  },
  methods: {
    /**
     * @param  {boolean} [nocache=false]
     */
    async fetch(nocache = false) {
      // Fetch all institutes
      sources.institutes.fetch({ max: 200, query: {} }, nocache);
      // Fetch due points for this year
      const paramsDP = { max: 200, query: { operYear: this.year } };
      sources.duePoints.fetch(paramsDP, nocache);
      if (nocache) { this.dirtyValue = false; }
    },
    /** @this {Instance} */
    async showInfoModal() {
      await this.$refs.infoDialog.request().catch(noop);
    },
    /**
     * @this {Instance}
     * @param  {boolean} [forceSubmit=false]
     */
    async loadPointsShifts(forceSubmit = false) {
      // Fetch shift points for this year
      try {
        this.loadingPoints = true;
        this.forceSubmit = forceSubmit;
        const paramsSP = { query: { operYear: this.year } };
        this.pointsShifts = await sources.pointsShifts.get(paramsSP);
      }
      catch (e) { /* noop */ }
      finally {
        this.loadingPoints = false;
      }
    },
    /**
     * @this {Instance}
     * @param {number} id
     * @return {AppStore.DuePoints | undefined}
     */
    getInstituteDuePoints(id) {
      const dp = first(filter(this.activeDuePoints, (p) => p.institute === id));
      return dp;
    },
    /**
     * @this {Instance}
     * @param {string} value
     * @param {AppStore.DuePoints} row
     */
    onEditNUsers(value, row) {
      if (toNumber(value) !== row.nUsers) {
        this.dirtyValue = true;
      }
      this.debounceUpdateShiftPoints();
    },
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value) {
      this.inEdit = value;
    },
    /**
     * @this {Instance}
     */
    async updateShiftPoints() {
      if (isEmpty(this.activeDuePoints)) { return; }
      // Get total of users
      let sumUsers = this.sumUsers;
      this.totNUsersInEdit = this._sumUsersInEdit();
      if (this.inEdit)  { sumUsers = this.totNUsersInEdit; }
      this.totDueShiftsInEdit = 0;
      this.dirtyValue = false;
      // Itareate over points
      forEach(this.activeDuePoints, (row) => {
        let nUsers = row.nUsers;
        if (this.inEdit) {
          const refNU = get(this.$refs, [ `inst-${row.institute}-nu`, 0 ]);
          nUsers = get(refNU, 'editValue');
        }
        const newValueSP = this._calcualteShiftPoints(nUsers, sumUsers);
        const refSP = get(this.$refs, [ `inst-${row.institute}-sp`, 0 ]);
        set(refSP, 'editValue', newValueSP);
        if (row.shiftPoints !== newValueSP) {
          this.dirtyValue = true;
        }
        this.totDueShiftsInEdit += newValueSP;
      });
      // We do commit the changes on db if we are not in edit mode
      if (!this.inEdit && this.dirtyValue) {
        if (this.forceSubmit) { await this.doSubmit(); }
        else { this.valuesOutdated = true; }
      }
      else {
        this.valuesOutdated = false;
      }
    },
    /** @return {number} */
    _sumUsersInEdit() {
      if (isEmpty(this.activeDuePoints)) { return 0; }
      return sumBy(this.activeDuePoints, (p) => {
        const refNU = get(this.$refs, [ `inst-${p.institute}-nu`, 0 ]);
        return toNumber(get(refNU, 'editValue', 0));
      });
    },
    /** @return {number} */
    _sumDuePointsInEdit() {
      if (isEmpty(this.activeDuePoints)) { return 0; }
      return sumBy(this.activeDuePoints, (p) => {
        const refLP = get(this.$refs, [ `inst-${p.institute}-sp`, 0 ]);
        return toNumber(get(refLP, 'editValue', 0));
      });
    },
    /**
     * @this {Instance}
     * @param {number} nUsers
     * @param {number} sumUsers
     * @return {number}
     */
    _calcualteShiftPoints(nUsers, sumUsers) {
      let ret = 0;
      if (this.totalNonLeaderPoints > 0 && sumUsers > 0) {
        const ratio = this.totalNonLeaderPoints / sumUsers;
        ret = cCeil(ratio, 0.5) * nUsers;
      }
      return ret;
    },
    /** @this {Instance} */
    async createDuePoints() {
      try {
        debug('creating duePoints');
        this.processing = true;

        // get institutes from db
        const institutes = await sources.institutes.get({ max: 200, query: {} });

        // Create empty object starting from all institutes (also not active)
        // Also not active, in this case if an institute is activated, is ready to go
        /** @type {{ [id: string]: Partial<AppStore.DuePoints>}} */
        const newDueP = transform(institutes,
          (/** @type {{ [id: string]: Partial<AppStore.DuePoints> }} */ res, inst) => {
            res[inst.id] = {
              operYear: this.year, institute: inst.id, nUsers: 0, shiftPoints: 0
            };
          }, {});
        // Get previous year data
        const prevYearDuePoints = await this._getPrevYearData();
        if (!isEmpty(prevYearDuePoints)) {
          // Copy previous years values as is
          forEach(prevYearDuePoints, (pydp) => {
            const newP = get(newDueP, pydp.institute);
            if (!isNil(newP)) {
              newP.nUsers = pydp.nUsers;
              newP.shiftPoints = pydp.shiftPoints;
            }
          });
        }
        // Finally save on DB
        await sources.duePoints.post(values(newDueP));
        await this.fetch(true);
        debug('duePoints created');
      }
      catch (e) { /* noop */ }
      finally {
        this.processing = false;
        this.setEdit(false);
      }
    },
    /** @this {Instance} */
    async copyPreviousYear() {
      try {
        this.processing = true;
        const prevYearDuePoints = await this._getPrevYearData();
        if (isEmpty(prevYearDuePoints)) {
          logger.error("There is no configuration available for previous years");
          throw false;
        }
        forEach(prevYearDuePoints, (pp) => {
          const refNU = get(this.$refs, [ `inst-${pp.institute}-nu`, 0 ]);
          set(refNU, 'editValue', pp.nUsers);
        });
        this.dirtyValue = true;
      }
      catch (e) { /* noop */ }
      finally {
        this.processing = false;
      }
    },
    /** @this {Instance} */
    async _getPrevYearData() {
      const prevYear = await sources.operationPeriods.get({
        max: 1, query: { year: { '$lt': this.year } }, sort: 'year:desc'
      }).then((res) => get(res, [ 0, 'year' ], null));
      if (!prevYear) { return []; }
      return sources.duePoints.get({ max: 100, query: { operYear: prevYear } });
    },
    /**
     * @this {Instance}
     * @param {number} instId
     */
    async createDuePointsRow(instId) {
      try {
        this.processing = true;
        const item = { operYear: this.year, institute: instId, nUsers: 0, shiftPoints: 0 };
        await sources.duePoints.post(item);
        await this.fetch(true);
      }
      catch (e) { /* noop */ }
      finally {
        this.processing = false;
      }
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doSubmit(event) {
      if (event) {
        event.preventDefault();
      }
      try {
        debug('submitting due shift points');
        this.processing = true;
        let refresh = false;
        // Iterate on active due points
        forEach(this.activeDuePoints, async (dp) => {
          const updateObj = {};
          // nUsers
          const refNU = get(this.$refs, [ `inst-${dp.institute}-nu`, 0 ]);
          const editValueNU = get(refNU, 'editValue');
          if (!isNil(editValueNU) && dp.nUsers !== toNumber(editValueNU)) {
            set(updateObj, 'nUsers', editValueNU);
          }

          // Shift Points
          const refSP = get(this.$refs, [ `inst-${dp.institute}-sp`, 0 ]);
          const editValueSP = get(refSP, 'editValue');
          if (!isNil(editValueSP) && dp.shiftPoints !== toNumber(editValueSP)) {
            set(updateObj, 'shiftPoints', editValueSP);
          }

          if (!isEmpty(updateObj)) {
            refresh = true;
            await sources.duePoints.put(dp.id, updateObj);
          }
        });
        // Oracle fix... it needs time to commit.
        if (refresh) {
          await delay(1000);
          await this.fetch(true);
        }
      }
      catch (e) { /* noop */ }
      finally {
        this.processing = false;
        this.setEdit(false);
        this.dirtyValue = false;
        this.valuesOutdated = false;
        this.forceSubmit = false;
      }
    }
  }
});
export default component;
