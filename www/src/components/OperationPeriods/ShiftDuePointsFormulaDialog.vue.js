// @ts-check
import Vue from 'vue';

/**
 * @typedef {V.Instance<typeof component, V.ExtVue<any, any>>} Instance
 */
const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'ShiftDuePointsFormulaDialog',
  methods: {
    /**
     * @this {Instance}
     */
    async request() {
      return this.$refs.dialog.request();
    }
  }
});
export default component;
