// @ts-nocheck
import { difference, forEach, get, isEmpty, keys, map,
  toNumber, toString, transform } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';
import {
  BaseKeyboardEventMixin as KeyboardEventMixin,
  BaseLogger as logger } from '@cern/base-vue';
import moment from 'moment';
import { ShiftKind } from '../../Consts';
import { sources } from '../../store';

/**
 * @typedef {{
 *  start: V.Instance<typeof BaseVue.BaseParamDate>,
 *  stop: V.Instance<typeof BaseVue.BaseParamDate>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> &
 *   V.Instance<ReturnType<KeyboardEventMixin>>
 * } Instance
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'OperationPeriodInfo',
  filters: { toString },
  mixins: [ KeyboardEventMixin({ local: true }) ],
  props: {
    operationPeriod: { type: Object, required: true }
  },
  /**
   * @return {{ inEdit: boolean, processing: boolean,
   *   startDateOK: boolean, stopDateOK: boolean }}
   */
  data() {
    return { inEdit: false, processing: false,
      startDateOK: true, stopDateOK: true };
  },
  computed: {
    .../** @type {{ showKeyHints(): boolean }} */(mapState('ui', [ 'showKeyHints' ])),
    .../** @type {{ nOperationWeeks(): number }} */(mapState('nOperationWeeks', {
      nOperationWeeks(state) { return get(state, [ this.operationPeriod.year ]); }
    })),
    /** @return {boolean} */
    disableSubmit() {
      return !this.startDateOK || !this.stopDateOK;
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('ctrl-s-keydown', this.doSubmit);
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
  },
  methods: {
    /**
     * @this {Instance}
     * @param {string} ref
     */
    checkStartDate() {
      this.startDateOK = this.checkDate(this.$refs.start);
      return this.startDateOK;
    },
    /**
     * @this {Instance}
     * @param {string} ref
     */
    checkStopDate() {
      this.stopDateOK =  this.checkDate(this.$refs.stop);
      return this.stopDateOK;
    },
    /**
     * @this {Instance}
     * @param {any} ref
     */
    checkDate(ref) {
      const year = moment((ref.editTimestamp || 0) * 1000).year();
      if (year !== this.operationPeriod.year) {
        ref.addError('year', 'Date must be within the operational year');
        return false;
      }
      else {
        ref.removeError('year');
        return true;
      }
    },
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value) {
      this.inEdit = value;
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doSubmit(event) {
      if (event) {
        event.preventDefault();
      }
      try {
        if (this.disableSubmit) { return; }
        // update nOperationWeeks
        const nw = toNumber(this.$refs.weeks.editValue);
        this.$store.commit('nOperationWeeks/setWeeks', {
          year: this.operationPeriod.year, value: nw
        });
        // update operationPeriods
        await sources.operationPeriods.put(this.operationPeriod.year, {
          start: toNumber(this.$refs.start.editTimestamp) * 1000,
          stop: toNumber(this.$refs.stop.editTimestamp) * 1000
        });
        this.setEdit(false);
        this.$emit('update');
      }
      catch (e) { /* noop */ }
    },
    /**
     * @param  {moment.Moment}  from
     * @param  {number}  days
     * @details from is modified by this function
     */
    async _getShifts(from, days) {
      const startStamp = from.valueOf();
      const endStamp = from.add(days, 'day').valueOf();
      const shifts = await sources.shifts.get(
        { query: { date: { "$gte": startStamp, "$lt": endStamp } } });
      return transform(shifts,
        (ret, s) => ret.add(moment(s.date).startOf('day').valueOf() + ':' + s.shiftKind),
        /** @type {Set<string>} */ (new Set()));
    },
    /**
     * @this {Instance}
     */
    // eslint-disable-next-line max-statements
    async doGenerateShifts() {
      try {
        this.processing = true;
        const startUnixTS = get(this.operationPeriod, [ 'start' ]) / 1000;
        const start = moment.unix(startUnixTS).startOf('day');
        const stopUnixTS = get(this.operationPeriod, [ 'stop' ]) / 1000;
        const stop = moment.unix(stopUnixTS).endOf('day');

        /** @type {any[]} */
        let buffer = [];
        const cacheEnd = start.clone();
        /** @type {Set<string>} */
        let cache = await this._getShifts(cacheEnd, 15);

        for (let current = start.clone(); !current.isAfter(stop); current.add(1, 'day')) {
          if (!current.isBefore(cacheEnd)) {
            cache = await this._getShifts(cacheEnd, 15);
          }

          for (const kind of keys(ShiftKind)) {
            if (!cache.has(`${current.valueOf()}:${kind}`)) {
              buffer.push({ date: current.valueOf(), shiftKind: kind });
            }
          }

          if (buffer.length > 100) {
            await sources.shifts.post(buffer);
            buffer = [];
          }
        }

        if (buffer.length > 0) {
          await sources.shifts.post(buffer);
        }
      }
      finally {
        this.processing = false;
      }
    },
    /**
     * @param  {number[]}  userIds
     * @param  {number}  operYear
     */
    async _getAssoUsers(userIds, operYear) {
      const newAsso = await sources.associations.get({
        max: 100, query: { userId: { "$in": userIds }, operYear }
      });
      return transform(newAsso,
        (ret, a) => ret.add(a.userId), /** @type {Set<number>} */ (new Set()));
    },
    /**
     * @this {Instance}
     */
    async doAssociations() {
      try {
        this.processing = true;
        const year = get(this.operationPeriod, [ 'year' ]);
        const prevYear = await sources.operationPeriods.get({
          max: 1, query: { year: { '$lt': year } }, sort: 'year:desc'
        })
        .then((res) => get(res, [ 0, 'year' ], null));
        if (!prevYear) {
          logger.error("There is no previous year to copy associations from.");
          throw 'no prevYear to copy associations from.';
        }

        const max = 20;
        for (let offset = 0; ; offset += max) {
          const oldAsso = await sources.associations.get(
            { max: max, offset, query: { operYear: prevYear }, sort: 'id:desc' });
          if (isEmpty(oldAsso)) { break; }

          const existUsers = await this._getAssoUsers(map(oldAsso, 'userId'), year);
          const buffer = [];
          for (const asso of oldAsso) {

            if (!existUsers.has(asso.userId)) {
              buffer.push({
                operYear: year, userId: asso.userId, institute: asso.institute
              });
            }
          }
          if (!isEmpty(buffer)) {
            await sources.associations.post(buffer);
          }
        }
        await this.createSpecialAssociations(year);
      }
      catch (e) { /* noop */ }
      finally {
        this.processing = false;
      }
    },
    /** @this {Instance} */
    async createSpecialAssociations(operYear) {
      // Create utility association for user Cancelled And Reserved if not exists
      // Get special users
      const specialIds = await sources.users.get({ max: 2,
        query: { firstName: { "$in": [ "Cancelled", "Reserved" ] } }
      }).then((res) => map(res, 'id'));

      const specialAssoUsers = await sources.associations.get({ max: 2,
        query: { operYear, userId: { '$in': specialIds } }
      }).then((res) => map(res, 'userId'));

      const missingAsso = difference(specialIds, specialAssoUsers);
      const buffer = transform(missingAsso, (res, id) => {
        res.push({ operYear, userId: id, institute: null });
      }, []);
      if (!isEmpty(buffer)) {
        // Get Ntof institute
        const ntofId = await sources.institutes.get({ max: 1,
          query: { alias: "__nTOF" }
        }).then((res) => get(res, [ 0, 'id' ], null));
        if (!ntofId) {
          logger.error("Can't find nTOF Institute with alias '__nTOF'");
          return;
        }
        // Add ntof id to buffer items
        forEach(buffer, (b) => { b.institute = ntofId; });
        await sources.associations.post(buffer);
      }
    }
  }
});
export default component;
