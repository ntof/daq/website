// @ts-check

import { filter, first, get, map, orderBy, toNumber, values } from 'lodash';
import Vue from 'vue';
import moment from 'moment';
import { mapState } from 'vuex';
import { BaseKeyboardEventMixin as KeyboardEventMixin } from '@cern/base-vue';
import ShiftLink from '../Shifts/ShiftLink.vue';
import UserLink from '../Users/UserLink.vue';
import { sources } from '../../store';
import RouteUtilsMixin from '../../mixins/RouteUtilsMixin';
import { createPutParam, instituteDesc } from '../../interfaces/database';

/**
 * @typedef {{ }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> &
 *   V.Instance<ReturnType<KeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin>
 * } Instance
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'InstituteInfo',
  components: { ShiftLink, UserLink },
  mixins: [ KeyboardEventMixin({ local: false }), RouteUtilsMixin ],
  /**
   * @return {{ inEdit: boolean }}
   */
  data() {
    return {
      inEdit: get(this.$route, [ 'meta', 'isAdmin' ])
    };
  },
  computed: {
    /** @return {number} */
    id() { return toNumber(get(this.$route, [ 'params', 'id' ])); },
    .../** @type {{ showKeyHints(): boolean }} */(mapState('ui', [ 'showKeyHints' ])),
    .../** @type {{ instituteLoading(): boolean, institute(): AppStore.Institute }} */(mapState('institutes', {
      instituteLoading: (state) => state.loading,
      institute(state) { return get(state, [ 'db', this.id ]); }
    })),
    .../** @type {{ associationsLoading(): boolean, associations(): any }} */(mapState('associations', {
      associationsLoading: (state) => state.loading,
      associations(state) { return get(state, [ 'db' ]); }
    })),
    .../** @type {{ shiftLoading(): boolean, shifts(): any }} */(mapState('shifts', {
      shiftLoading: (state) => state.loading,
      shifts(state) { return get(state, [ 'rows' ]); }
    })),
    .../** @type {{ users(): any }} */(mapState('usersCache', { users: 'db' })),
    /** @return {boolean} */
    loading() {
      return this.instituteLoading || this.associationsLoading || this.shiftLoading;
    },
    /** @return {number} */
    usersLength() {
      // We use associations to know how many users it has
      return values(this.associations).length;
    },
    /**
     * @this {Instance}
     * @return {boolean}
     */
    canEdit() { return this.isAdmin; }
  },
  watch: {
    id: {
      immediate: true,
      handler() {
        this.fetch();
      }
    },
    associations() {
      sources.usersCache.fetchMore(map(this.associations, 'userId'));
      const assoIds = map(this.associations, 'id');
      sources.shifts.fetch({ max: 2000, query: { assoId: { $in: assoIds } }, sort: 'date:asc' });
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('ctrl-s-keydown', this.doSubmit);
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
  },
  methods: {
    /**
     * @param  {boolean} [nocache=false]
     */
    async fetch(nocache = false) {
      sources.institutes.fetch({ max: 1, query: { id: this.id } }, nocache);
      // Retrieve first operYear
      const operYear = await sources.operationPeriods.get({ max: 1, sort: 'year:desc' })
      .then((res) => first(map(res, 'year')));
      sources.associations.fetch({
        max: 200,
        query: { institute: this.id, operYear }
      }, nocache);
    },
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value) {
      if (this.canEdit) {
        this.inEdit = value;
      }
    },
    /** @this {Instance} */
    getUpcomingShifts() {
      const now = moment().startOf('day');
      return filter(this.shifts, (s) => s.date >= now);
    },
    /** @this {Instance} */
    getPastShifts() {
      const now = moment().startOf('day');
      return orderBy(filter(this.shifts, (s) => s.date < now), 'date', 'desc');
    },
    /**
     * @this {Instance}
     * @param {number} id
     */
    getUser(id) {
      return get(this.users, [ id ]);
    },
    /**
     * @this {Instance}
     * @param {number} id
     */
    getAssociation(id) {
      return get(this.associations, [ id ]);
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doSubmit(event) {
      if (event) {
        event.preventDefault();
      }
      if (!this.inEdit) { return; }
      try {
        const params = createPutParam(this, this.institute, instituteDesc);
        await sources.institutes.put(this.id, params);

        this.inEdit = false;
        await this.fetch(true);
      }
      catch (e) { /* noop */ }
    }
  }
});
export default component;
