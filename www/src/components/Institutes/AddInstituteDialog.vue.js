// @ts-check

import { sources } from '../../store';
import { createPostParam, instituteDesc } from '../../interfaces/database';
import Vue from 'vue';
import d from 'debug';

const debug = d('app:comp');

const component = Vue.extend({
  name: 'AddInstituteDialog',
  methods: {
    async request() {
      this.$refs.name.editValue = '';
      this.$refs.city.editValue = '';
      this.$refs.country.editValue = '';
      this.$refs.alias.editValue = '';
      this.$refs.status.editValue = true;
      try {
        if (!await this.$refs.dialog.request()) { return; }
        const id = await sources.institutes.post(
          createPostParam(this, instituteDesc));
        this.$router.push('/admin/institutes/' + id);
      }
      catch (err) {
        debug('doAdd error:', err);
      }
    }
  }
});
export default component;
