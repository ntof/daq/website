// @ts-check
import Vue from 'vue';
import RouteUtilsMixin from '../../mixins/RouteUtilsMixin';

export default Vue.extend({
  name: 'InstituteLink',
  mixins: [ RouteUtilsMixin ],
  props: {
    institute: { type: Object, default: null },
    id: { type: [ Number, String ], default: '-' }
  }
});
