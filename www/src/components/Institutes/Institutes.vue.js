// @ts-check

import { assign, get, noop, set, toNumber } from 'lodash';
import Vue from 'vue';
import DBSearchMixin from '../../mixins/DBSearchMixin';
import InstituteLink from './InstituteLink.vue';
import AddInstituteDialog from './AddInstituteDialog.vue';

/**
 * @typedef {typeof import('../../mixins/RouteUtilsMixin').default} RouteUtilsMixin
 * @typedef {import('@cern/base-vue').BaseKeyboardEventMixin} KeyboardEventMixin
 * @typedef {{ addDialog: V.Instance<AddInstituteDialog> }} Refs
 * @typedef {V.Instance<component, V.ExtVue<options, Refs>> &
 *   V.Instance<typeof DBSearchMixin> &
 *   V.Instance<ReturnType<KeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin>
 * } Instance
 */

const options = { source: 'institutes' };

const component = /** @type {V.Constructor<options, Refs> } */ (Vue).extend({
  name: 'Institutes',
  components: { InstituteLink, AddInstituteDialog },
  mixins: [ DBSearchMixin ],
  ...options,
  /**
   * @return {{  }}
   */
  data() {
    return {  };
  },
  methods: {
    /** @this {Instance} */
    loadRoute() {
      try {
        const query = this.$route.query;
        this.page = toNumber(get(query, 'page', 0));
      }
      catch (e) {
        console.log(e);
      }
    },
    /**
     * @param  {any} params
     * @return {any}
     */
    prepareQuery(params) {
      params = assign(params, { query: {}, sort: 'alias:asc' });

      if (!this.isAdmin) {
        set(params.query, [ 'status' ], 1);
      }
      return params;
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doAdd(event) {
      if (event) { event.preventDefault(); }
      await this.$refs.addDialog.request().catch(noop);
    }
  }
});

export default component;
