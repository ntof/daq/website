// @ts-check

import { get } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';
import { sources } from '../../store';
import InstituteLink from './InstituteLink.vue';
import DBCollapsible from '../DBCollapsible.vue';

const component = Vue.extend({
  name: 'InstitutesList',
  components: { DBCollapsible, InstituteLink },
  props: {
    user: { type: Object, default: null }
  },
  computed: {
    .../** @type {{
      institutes(): { [id:string]: AppStore.Institute },
    }} */(mapState('institutesCache', {
      institutes: 'db'
    }))
  },
  methods: {
    /**
     * @param  {number} id institute id
     */
    getInstitute(id) {
      sources.institutesCache.fetchMore([ id ]);
      return get(this.institutes, [ id ]);
    }
  }
});
export default component;
