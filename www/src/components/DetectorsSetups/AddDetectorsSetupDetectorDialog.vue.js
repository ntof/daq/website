// @ts-check

import { get, isFinite, isNil, set, size, toNumber } from 'lodash';
import { sources } from '../../store';
import Vue from 'vue';
import { mapState } from 'vuex';

/**
 * @typedef {import('@cern/base-vue').BaseDialog} BaseDialog
 * @typedef {{ dialog: V.Instance<BaseDialog> }} Refs
 * @typedef {V.Instance<component, V.ExtVue<any, Refs>>} Instance
 */

const component = /** @type {V.Constructor<any, Refs> } */ (Vue).extend({
  name: 'AddDetectorsSetupDetectorDialog',
  props: {
    ids: { type: Array, default: () => [] }
  },
  /**
   * @return {{ type: string, max: number, page: number }}
   */
  data() {
    return { type: 'filter', page: 0, max: 20 };
  },
  computed: {
    .../** @type {{ rows(): any[], loading(): boolean }} */mapState('detectors', [ 'rows', 'loading' ])
  },
  watch: {
    max(m, old) {
      if (m === old) { return; }
      this.page = 0;
      this.fetch();
    },
    rows(r) {
      if (!this.loading && size(r) === 0 && this.page > 0) {
        this.goToPage(this.page - 1);
      }
    }
  },
  methods: {
    fetch() {
      /** @type {any} */
      const params = {
        max: this.max, offset: this.max * this.page,
        query: { }
      };
      set(params, [ 'query', 'id', '$nin' ], this.ids);
      /** @type {any} */
      var value = get(this.$refs, [ 'name', 'editValue' ]);
      if (!isNil(value)) {
        params.query.name = value;
      }
      value = get(this.$refs, [ 'daqName', 'editValue' ]);
      if (!isNil(value)) {
        params.query.daqName = value;
      }
      value = get(this.$refs, [ 'trecNumber', 'editValue' ]);
      if (isFinite(toNumber(value))) {
        params.query.trecNumber = value;
      }
      sources.detectors.fetch(params);
    },
    doSearch() {
      this.fetch();
    },
    /**
     * @param {number} num
     */
    goToPage(num) {
      this.page = num;
      this.fetch();
    },
    async request() {
      set(this.$refs, [ 'name', 'editValue' ], undefined);
      set(this.$refs, [ 'daqName', 'editValue' ], undefined);
      set(this.$refs, [ 'trecNumber', 'editValue' ], undefined);
      this.fetch();
      return await this.$refs.dialog.request();
    }
  }
});
export default component;
