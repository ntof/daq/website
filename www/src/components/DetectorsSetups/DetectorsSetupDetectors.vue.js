// @ts-check

import { clone, concat, filter, find, findIndex,
  get, isNil, map, pullAt, toNumber } from 'lodash';
import { mapState } from 'vuex';
import Vue from 'vue';
import d from 'debug';
import { sources } from '../../store';
import AddDetectorDialog from './AddDetectorsSetupDetectorDialog.vue';
import AddContainerDialog from './AddDetectorsSetupContainerDialog.vue';
import RouteUtilsMixin from '../../mixins/RouteUtilsMixin';

const debug = d('app:compo');

/**
 * @typedef {{
 *   addDetectorDialog: V.Instance<typeof AddDetectorDialog>
 *   addContainerDialog: V.Instance<typeof AddContainerDialog>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> &
 *  V.Instance<RouteUtilsMixin>
 * } Instance
 */

const component = /** @type {V.Constructor<any, Refs> } */ (Vue).extend({
  name: 'DetectorsSetupDetectors',
  components: { AddDetectorDialog, AddContainerDialog },
  mixins: [ RouteUtilsMixin ],
  props: {
    detectorSetupId: { type: [ String, Number ], default: null },
    inEdit: { type: Boolean, default: false }
  },
  /**
   * @return {{ loaded: boolean, infoData: any[]|null, detectorsData: any|null, containersData: any|null }}
   */
  data() {
    return { loaded: false, infoData: null, detectorsData: null, containersData: null };
  },
  computed: {
    .../** @type {{ infoLoading(): boolean, info(): any[] }} */(mapState('detectorsSetupInfo', {
      infoLoading: 'loading', info: 'rows'
    })),
    .../** @type {{ detectorsLoading(): boolean, detectors(): { [id:string]: any } }} */(mapState('detectors', {
      detectorsLoading: 'loading', detectors: 'db'
    })),
    .../** @type {{ containersLoading(): boolean, containers(): { [id:string]: any } }} */(mapState('containers', {
      containersLoading: 'loading', containers: 'db'
    })),
    /** @return {boolean} */
    loading() { return this.infoLoading || this.detectorsLoading || this.containersLoading; },
    /** @return {number[]} */
    detectorIds() {
      /** @type {number[]} */
      return map(this.infoData || [], 'detectorId');
    }
  },
  watch: {
    info() { this.copyData(); },
    detectors() { this.copyData(); },
    containers() { this.copyData(); },
    detectorSetupId() { this.fetch(); },
    inEdit(value) {
      this.fetch(value === false);
    }
  },
  /** @this {Instance} */
  beforeMount() {
    this.fetch(true);
  },
  methods: {
    async fetch(nocache = false) {
      this.loaded = nocache ? false : this.loaded;
      sources.detectorsSetupInfo.fetch(
        { query: { detectorSetupId: this.detectorSetupId } }, nocache);
      sources.detectors.fetchRelated('detectorsSetups',
        { id: this.detectorSetupId }, {}, nocache);
      sources.containers.fetchRelated('detectorsSetups',
        { id: this.detectorSetupId }, {}, nocache);
    },
    /** @this {Instance} */
    copyData() {
      if (this.loading || this.loaded) { return; }
      this.infoData = clone(this.info);
      this.detectorsData = clone(this.detectors);
      this.containersData = clone(this.containers);
      this.loaded = true;
    },
    /**
     * @this {Instance}
     * @param {number} id
     */
    getDetector(id) {
      return get(this.detectorsData, id);
    },
    /**
     * @this {Instance}
     * @param {number} id
     */
    getContainer(id) {
      return get(this.containersData, id);
    },
    /**
     * @this {Instance}
     * @param {number} detId detector id
     */
    async setContainer(detId) {
      try {
        const setup = find(this.infoData, (i) => i.detectorId === detId);
        const container = await this.$refs.addContainerDialog.request();
        if (!this.containersData[container.id]) {
          this.containersData[container.id] = container;
        }
        setup.containerId = container.id;
      }
      catch (err) {
        debug('add error:', err);
      }
    },
    /**
     * @this {Instance}
     * @param {number} detId detector id
     */
    removeContainer(detId) {
      const setup = find(this.infoData, (i) => i.detectorId === detId);
      setup.containerId = null;
    },
    /**
     * @this {Instance}
     */
    async addDetector() {
      try {
        const detector = await this.$refs.addDetectorDialog.request();
        if (!this.detectorsData[detector.id]) {
          this.detectorsData[detector.id] = detector;
        }
        const newSetup = {
          detectorId: detector.id,
          detectorSetupId: toNumber(this.detectorSetupId),
          containerId: null
        };
        Vue.set(this, 'infoData', concat(this.infoData || [], newSetup));
      }
      catch (err) {
        debug('add error:', err);
      }
    },
    /**
     * @this {Instance}
     * @param {number} detId detector id
     */
    removeDetector(detId) {
      Vue.set(this, 'infoData', filter(this.infoData, (d) => d.detectorId !== detId));
    },
    /**
     * @param  {any[]}  info
     */
    async _submit(info) {
      for (const set of this.infoData || []) {
        var infoIdx = findIndex(info, (i) => (i.detectorId === set.detectorId));
        if (infoIdx >= 0 && (set.containerId !== info[infoIdx].containerId)) {
          debug('removing detector (temporarily): %s', set.detectorId);
          // delete to upload again
          await sources.detectors.deleteRelation('detectorsSetups',
            { id: this.detectorSetupId }, set.detectorId);
          pullAt(info, infoIdx);
          infoIdx = -1;
        }
        if (infoIdx < 0) {
          debug('adding detector: %s', set.detectorId);
          await sources.detectorsSetupInfo.post({
            detectorId: set.detectorId,
            detectorSetupId: toNumber(this.detectorSetupId),
            containerId: set.containerId
          });
        }
        else {
          debug('no changes for detector: %s', set.detectorId);
          pullAt(info, infoIdx);
        }
      }
      return info;
    },
    async doSubmit() {
      var info = clone(this.info);
      info = await this._submit(info);
      for (var i of info) {
        debug('removing detector: %s:', i.detectorId);
        await sources.detectors.deleteRelation('detectorsSetups',
          { id: this.detectorSetupId }, i.detectorId);
        if (!isNil(i.containerId)) {
          debug('removing container: %s:', i.containerId);
          await sources.containers.deleteRelation('detectorsSetups',
            { id: this.detectorSetupId }, i.containerId);
        }
      }
    },
    async doDeleteAll() {
      for (var i of this.info) {
        await sources.detectors.deleteRelation('detectorsSetups',
          { id: this.detectorSetupId }, i.detectorId);
        if (!isNil(i.containerId)) {
          await sources.containers.deleteRelation('detectorsSetups',
            { id: this.detectorSetupId }, i.containerId);
        }
      }
    }
  }
});
export default component;
