// @ts-check
import { get, isEmpty, isNil, map, set, toString } from 'lodash';
import Vue from 'vue';
import { mapGetters, mapState } from 'vuex';
import { BaseKeyboardEventMixin as KeyboardEventMixin } from '@cern/base-vue';
import OperYearSelector from '../OperYearSelector.vue';
import DetectorsSetupDetectors from './DetectorsSetupDetectors.vue';
import DocumentsList from '../Documents/DocumentsList.vue';
import DBCollapsible from '../DBCollapsible.vue';
import { createPostParam, createPutParam, setupDesc } from '../../interfaces/database';
import { sources } from '../../store';
import RouteUtilsMixin from '../../mixins/RouteUtilsMixin';

/**
 * @typedef {{
 *  deleteDialog: V.Instance<typeof BaseVue.BaseDialog>,
 *  detectors: V.Instance<typeof DetectorsSetupDetectors>,
 *  titleDialog: V.Instance<typeof BaseVue.BaseDialog>,
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> &
 *   V.Instance<ReturnType<KeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin>
 * } Instance
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'DetectorsSetupInfo',
  components: {
    DetectorsSetupDetectors, DocumentsList, DBCollapsible, OperYearSelector },
  filters: { toString },
  mixins: [ KeyboardEventMixin({ local: false }), RouteUtilsMixin ],
  /**
   * @return {{ editTitle: string, inEdit: boolean }}
   */
  data() {
    return {
      editTitle: '',
      inEdit: get(this.$route, [ 'meta', 'isAdmin' ]) // starts in edit when in admin pages
    };
  },
  computed: {
    /** @return {string} */
    id() { return get(this.$route, [ 'params', 'id' ]); },
    .../** @type {{ showKeyHints(): boolean }} */(mapState('ui', [ 'showKeyHints' ])),

    .../** @type {{ setupLoading(): boolean, setup(): any }} */(mapState('detectorsSetups', {
      setupLoading: (state) => state.loading,
      setup(state) { return get(state, [ 'db', this.id ]); }
    })),
    .../** @type {{ infoLoading(): boolean }} */(mapState('detectorsSetupInfo',
      { infoLoading: 'loading' })),
    .../** @type {{ runsLoading(): boolean, runs(): any[] }} */(mapState('runs',
      { runsLoading: 'loading', runs: 'rows' })),
    .../** @type {{ canEdit(): boolean }} */(mapGetters([ 'canEdit' ])),
    /** @return {boolean} */
    loading() {
      return this.setupLoading || this.infoLoading || this.runsLoading;
    }
  },
  watch: {
    id() {
      if (isNil(this.setup)) {
        this.fetch();
      }
    },
    inEdit(value) {
      this.fetch(value === false);
    }
  },
  /** @this {Instance} */
  beforeMount() {
    if (isNil(this.setup)) {
      this.fetch();
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('ctrl-s-keydown', this.doSubmit);
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
  },
  methods: {
    /**
     * @param  {boolean} [nocache=false]
     */
    async fetch(nocache = false) {
      const id = get(this.$route, [ 'params', 'id' ]);
      const params = { max: 1, query: { id } };
      sources.detectorsSetups.fetch(params, nocache);
    },
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value) {
      if (this.canEdit) {
        this.inEdit = value;
      }
    },
    _genTitle() {
      var title = get(this.$refs, [ 'operYear', 'editValue' ]) ||
        get(this.setup, 'operYear');
      title += ' EAR' + (get(this.$refs, [ 'earNumber', 'editValue' ]) ||
        get(this.setup, 'earNumber'));

      const setups = get(this.$refs, [ 'detectors', 'infoData' ]);
      const detectors = get(this.$refs, [ 'detectors', 'detectorsData' ]);
      const containers = get(this.$refs, [ 'detectors', 'containersData' ]);
      if (!isEmpty(setups)) {
        title += map(setups, (s) => {
          const detName = get(detectors, [ s.detectorId, 'name' ]);
          const detStr = ` D: ${detName}`;
          let contStr = '';
          if (s.containerId) {
            const contName = get(containers, [ s.containerId, 'name' ]);
            contStr = ` C: ${contName}`;
          }
          return `${detStr}${contStr}`;
        }).join(',');
      }
      return title;
    },
    /**
     * @param  {boolean} isCopy
     */
    async _checkTitle(isCopy = false) {
      const title = this._genTitle();
      this.editTitle = get(this.$refs, [ 'name', 'editValue' ]);
      if (isCopy && (title === get(this.setup, 'name'))) {
        if (this.editTitle === title) {
          set(this.$refs, [ 'titleDialogEdit', 'editValue' ], title + ' - Copy');
          if (await this.$refs.titleDialog.request()) {
            set(this.$refs, [ 'name', 'editValue' ],
              get(this.$refs, [ 'titleDialogEdit', 'editValue' ]));
          }
          else {
            throw 'titleDialog returned false'; // it will be catch by doCopy and do nothing.
          }
        }
      }
      else if (title !== this.editTitle) {
        this.editTitle = this.editTitle;
        set(this.$refs, [ 'titleDialogEdit', 'editValue' ], title);
        if (await this.$refs.titleDialog.request()) {
          set(this.$refs, [ 'name', 'editValue' ],
            get(this.$refs, [ 'titleDialogEdit', 'editValue' ]));
        }
        else {
          throw 'titleDialog returned false'; // it will be catch by doSubmit and do nothing.
        }
      }
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doSubmit(event) {
      if (event) {
        event.preventDefault();
      }
      try {
        await this._checkTitle();
        const params = createPutParam(this, this.setup, setupDesc);
        await sources.detectorsSetups.put(this.setup.id, params);
        await this.$refs.detectors.doSubmit();
        this.inEdit = false;
      }
      catch (e) { /* noop */ }
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doDelete(event) {
      if (event) { event.preventDefault(); }
      try {
        if (!await this.$refs.deleteDialog.request()) { return; }
        await this.$refs.detectors.doDeleteAll();
        await sources.detectorsSetups.delete(this.setup.id);
        sources.detectorsSetups.refresh();
        this.$router.push('/admin/detectors/setups');
      }
      catch (e) { /* noop */ }
    },
    /**
     * @param  {string|number} id
     * @param  {any[]} detectorsSetups
     */
    async _copyDetectors(id, detectorsSetups) {
      for (const setup of detectorsSetups) {
        await sources.detectorsSetupInfo.post({
          detectorId: setup.detectorId, containerId: setup.containerId, detectorSetupId: id
        });
      }
    },
    /**
     * @param  {KeyboardEvent=} event
     */
    async doCopy(event) {
      if (event) { event.preventDefault(); }
      try {
        await this._checkTitle(true);
        const id = await sources.detectorsSetups.post(
          createPostParam(this, setupDesc));
        await this._copyDetectors(id, this.$refs.detectors.infoData || []);
        this.$router.push('/admin/detectors/setups/' + id);
      }
      catch (e) { /* noop */ }
    }
  }
});
export default component;
