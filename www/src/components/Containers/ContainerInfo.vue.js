// @ts-check
import { get, isNil } from 'lodash';
import Vue from 'vue';
import { mapGetters, mapState } from 'vuex';
import { BaseKeyboardEventMixin as KeyboardEventMixin } from '@cern/base-vue';
import { containerDesc, createPutParam } from '../../interfaces/database';
import DocumentsList from '../Documents/DocumentsList.vue';
import DBCollapsible from '../DBCollapsible.vue';
import { sources } from '../../store';
import RouteUtilsMixin from '../../mixins/RouteUtilsMixin';

/**
 * @typedef {{ deleteDialog: V.Instance<typeof Dialog> }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> &
 *   V.Instance<ReturnType<KeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin>
 * } Instance
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'ContainerInfo',
  components: { DocumentsList, DBCollapsible },
  mixins: [ KeyboardEventMixin({ local: false }), RouteUtilsMixin ],
  /**
   * @return {{ inEdit: boolean }}
   */
  data() {
    return { inEdit: get(this.$route, [ 'meta', 'isAdmin' ]) };
  },
  computed: {
    /** @return {string} */
    id() { return get(this.$route, [ 'params', 'id' ]); },
    .../** @type {{ showKeyHints(): boolean }} */(mapState('ui', [ 'showKeyHints' ])),
    .../** @type {{ containerLoading(): boolean, container(): any }} */(mapState('containers', {
      containerLoading: (state) => state.loading,
      container(state) { return get(state, [ 'db', this.id ]); }
    })),
    .../** @type {{ setupsLoading(): boolean, setups(): any[] }} */(mapState('detectorsSetups',
      { setups: 'rows', setupsLoading: 'loading' })),
    .../** @type {{ canEdit(): boolean }} */(mapGetters([ 'canEdit' ])),
    /** @return {boolean} */
    loading() {
      return this.containerLoading || this.setupsLoading;
    }
  },
  watch: {
    id() {
      if (isNil(this.container)) {
        this.fetch();
      }
    }
  },
  /** @this {Instance} */
  beforeMount() {
    if (isNil(this.container)) {
      this.fetch();
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('ctrl-s-keydown', this.doSubmit);
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
  },
  methods: {
    /**
     * @param  {boolean} [nocache=false]
     */
    async fetch(nocache = false) {
      const id = get(this.$route, [ 'params', 'id' ]);
      const params = { max: 1, query: { id } };
      sources.containers.fetch(params, nocache);
      sources.detectorsSetups.fetchRelated('containers', { id }, { max: 100 }, nocache);
    },
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value) {
      if (this.canEdit) {
        this.inEdit = value;
      }
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doSubmit(event) {
      if (event) {
        event.preventDefault();
      }
      try {
        const params = createPutParam(this, this.container, containerDesc);
        await sources.containers.put(this.container.id, params);
        this.inEdit = false;
        await this.fetch(true);
      }
      catch (e) { /* noop */ }
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doDelete(event) {
      if (event) {
        event.preventDefault();
      }
      try {
        if (!await this.$refs.deleteDialog.request()) { return; }
        await sources.containers.delete(this.container.id);
        sources.containers.refresh();
        this.$router.push('/admin/containers');
      }
      catch (e) { /* noop */ }
    }
  }
});
export default component;
