// @ts-check

import { assign } from 'lodash';
import { sources } from '../../store';
import { containerDesc, createPostParam } from '../../interfaces/database';
import Vue from 'vue';
import d from 'debug';

const debug = d('app:comp');

const component = Vue.extend({
  name: 'AddContainerDialog',
  methods: {
    async request() {
      this.$refs.name.editValue = '';
      this.$refs.trecNumber.editValue = -1;
      try {
        if (!await this.$refs.dialog.request()) { return; }
        const id = await sources.containers.post(
          assign({ description: '' },
            createPostParam(this, containerDesc)));
        this.$router.push('/admin/containers/' + id);
      }
      catch (err) {
        debug('doAdd error:', err);
      }
    }
  }
});
export default component;
