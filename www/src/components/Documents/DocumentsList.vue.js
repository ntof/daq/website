// @ts-check

import Vue from 'vue';
import { get, isEmpty, isNil } from 'lodash';
import DBCollapsible from '../DBCollapsible.vue';
import { sources } from '../../store';
import d from 'debug';

const debug = d('app:comp');

/**
 * @typedef {{
 *   db: V.Instance<typeof DBCollapsible>,
 *   deleteModal: V.Instance<typeof BaseVue.BaseDialog>,
 *   addModal: V.Instance<typeof BaseVue.BaseDialog>,
 *   fileInput: V.Instance<typeof BaseVue.BaseParamFile>,
 *   fileSubmit: HTMLInputElement,
 *   titleInput: V.Instance<typeof BaseVue.BaseParamInput>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs> >} Instance
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'DocumentsList',
  components: { DBCollapsible },
  props: {
    inEdit: { type: Boolean, default: false },
    parentItem: { type: Object, default: null },
    parentType: { type: String, default: null }
  },
  methods: {
    /**
     * @param {KeyboardEvent|null} event
     */
    async doAdd(event) {
      if (event) {
        event.preventDefault();
      }
      /** @type {string|number|undefined} */
      var id;
      try {
        if (!await this.$refs.addModal.request()) { return; }
        else if (!this.$refs.fileInput.editValue) { return; }

        id = await sources.documents.post({
          title: get(this.$refs.titleInput, 'editValue'),
          size: get(this.$refs.fileInput, [ 'editValue', 'size' ]),
          name: get(this.$refs.fileInput, [ 'editValue', 'name' ])
        });
        if (this.parentType) {
          await sources.documents.postRelation(
            this.parentType, this.parentItem, id);
        }
        await sources.documents.putData(id, 'document',
          get(this.$refs.fileInput, 'editValue'));
        sources.documents.refresh();
      }
      catch (err) {
        debug('doAdd error:', err);
        if (!isNil(id)) {
          this.doDelete(null, id);
        }
      }
    },
    /**
     * @this {Instance}
     * @param {KeyboardEvent|null} event
     * @param {string|number} id
     */
    async doDelete(event, id) {
      if (event) {
        event.preventDefault();
      }
      try {
        if (!await this.$refs.deleteModal.request()) { return; }
        if (this.parentType) {
          await sources.documents.deleteRelation(
            this.parentType, this.parentItem, id)
          .catch((err) => debug('doDelete parent error: ', err));
        }
        await sources.documents.delete(id);
        this.$refs.db.fetch(true);
      }
      catch (err) {
        debug('doDelete error:', err);
      }
    },
    onFileInput() {
      /* enable submit button, set title if needed */
      this.$refs.fileSubmit.disabled = false;
      if (isEmpty(this.$refs.titleInput.editValue)) {
        this.$refs.titleInput.editValue = get(this.$refs,
          [ 'fileInput', 'editValue', 'name' ]);
      }
    }
  }
});
export default component;
