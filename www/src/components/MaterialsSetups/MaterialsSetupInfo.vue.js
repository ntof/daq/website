// @ts-check
import { filter, get, isEmpty, isNil, map, set, toString } from 'lodash';
import Vue from 'vue';
import { mapGetters, mapState } from 'vuex';
import { BaseKeyboardEventMixin as KeyboardEventMixin } from '@cern/base-vue';
import OperYearSelector from '../OperYearSelector.vue';
import MaterialsSetupMaterials from './MaterialsSetupMaterials.vue';
import DBCollapsible from '../DBCollapsible.vue';
import { createPostParam, createPutParam, setupDesc } from '../../interfaces/database';
import { sources } from '../../store';
import RouteUtilsMixin from '../../mixins/RouteUtilsMixin';

/**
 * @typedef {{
 *  deleteDialog: V.Instance<typeof BaseVue.BaseDialog>,
 *  materials: V.Instance<typeof MaterialsSetupMaterials>,
 *  titleDialog: V.Instance<typeof BaseVue.BaseDialog>,
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> &
 *   V.Instance<ReturnType<KeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin>
 * } Instance
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'MaterialsSetupInfo',
  components: { MaterialsSetupMaterials, DBCollapsible, OperYearSelector },
  filters: { toString },
  mixins: [ KeyboardEventMixin({ local: false }), RouteUtilsMixin ],
  /**
   * @return {{ editTitle: string, inEdit: boolean }}
   */
  data() {
    return {
      editTitle: '',
      inEdit: get(this.$route, [ 'meta', 'isAdmin' ]) // starts in edit when in admin pages
    };
  },
  computed: {
    /** @return {string} */
    id() { return get(this.$route, [ 'params', 'id' ]); },
    .../** @type {{ showKeyHints(): boolean }} */(mapState('ui', [ 'showKeyHints' ])),

    .../** @type {{ setupLoading(): boolean, setup(): any }} */(mapState('materialsSetups', {
      setupLoading: (state) => state.loading,
      setup(state) { return get(state, [ 'db', this.id ]); }
    })),
    .../** @type {{ infoLoading(): boolean }} */(mapState('materialsSetupInfo',
      { infoLoading: 'loading' })),
    .../** @type {{ runsLoading(): boolean, runs(): any[] }} */(mapState('runs',
      { runsLoading: 'loading', runs: 'rows' })),
    .../** @type {{ canEdit(): boolean }} */(mapGetters([ 'canEdit' ])),
    /** @return {boolean} */
    loading() {
      return this.setupLoading || this.infoLoading || this.runsLoading;
    }
  },
  watch: {
    id() {
      if (isNil(this.setup)) {
        this.fetch();
      }
    },
    inEdit(value) {
      this.fetch(value === false);
    }
  },
  /** @this {Instance} */
  beforeMount() {
    if (isNil(this.setup)) {
      this.fetch();
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('ctrl-s-keydown', this.doSubmit);
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
  },
  methods: {
    /**
     * @param  {boolean} [nocache=false]
     */
    async fetch(nocache = false) {
      const id = get(this.$route, [ 'params', 'id' ]);
      const params = { max: 1, query: { id } };
      sources.materialsSetups.fetch(params, nocache);
    },
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value) {
      if (this.canEdit) {
        this.inEdit = value;
      }
    },
    _genTitle() {
      this.$refs.materials.sort();
      var title = get(this.$refs, [ 'operYear', 'editValue' ]) ||
        get(this.setup, 'operYear');
      title += ' EAR' + (get(this.$refs, [ 'earNumber', 'editValue' ]) ||
        get(this.setup, 'earNumber'));
      const filters = filter(get(this.$refs, [ 'materials', 'filter' ]),
        (f) => (f.status));
      if (!isEmpty(filters)) {
        title += ' Filters: ' + map(filters, (f) => (f.compound || `#${f.id}`)).join(',');
      }
      const samples = get(this.$refs, [ 'materials', 'sample' ]);
      if (!isEmpty(samples)) {
        title += ' Samples: ' + map(samples, (f) => (f.compound || `#${f.id}`)).join(',');
      }
      const sources = get(this.$refs, [ 'materials', 'source' ]);
      if (!isEmpty(sources)) {
        title += ' Sources: ' + map(sources, (f) => (f.compound || `#${f.id}`)).join(',');
      }
      return title;
    },
    /**
     * @param  {boolean} isCopy
     */
    async _checkTitle(isCopy = false) {
      const title = this._genTitle();
      this.editTitle = get(this.$refs, [ 'name', 'editValue' ]);
      if (isCopy && (title === get(this.setup, 'name'))) {
        if (this.editTitle === title) {
          set(this.$refs, [ 'titleDialogEdit', 'editValue' ], title + ' - Copy');
          if (await this.$refs.titleDialog.request()) {
            set(this.$refs, [ 'name', 'editValue' ],
              get(this.$refs, [ 'titleDialogEdit', 'editValue' ]));
          }
          else {
            throw 'titleDialog returned false'; // it will be catch by doCopy and do nothing.
          }
        }
      }
      else if (title !== this.editTitle) {
        this.editTitle = this.editTitle;
        set(this.$refs, [ 'titleDialogEdit', 'editValue' ], title);
        if (await this.$refs.titleDialog.request()) {
          set(this.$refs, [ 'name', 'editValue' ],
            get(this.$refs, [ 'titleDialogEdit', 'editValue' ]));
        }
      }
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doSubmit(event) {
      if (event) {
        event.preventDefault();
      }
      try {
        await this._checkTitle();
        const params = createPutParam(this, this.setup, setupDesc);
        await sources.materialsSetups.put(this.setup.id, params);
        await this.$refs.materials.doSubmit();
        this.inEdit = false;
      }
      catch (e) { /* noop */ }
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doDelete(event) {
      if (event) { event.preventDefault(); }
      try {
        if (!await this.$refs.deleteDialog.request()) { return; }
        await this.$refs.materials.doDeleteAll();
        await sources.materialsSetups.delete(this.setup.id);
        sources.materialsSetups.refresh();
        this.$router.push('/admin/materials/setups');
      }
      catch (e) { /* noop */ }
    },
    /**
     * @param  {string|number} id
     * @param  {any[]} materials
     */
    async _copyMaterials(id, materials) {
      for (const mat of materials) {
        await sources.materialsSetupInfo.post({
          materialId: mat.id, status: mat.status, position: mat.position,
          materialsSetupId: id
        });
      }
    },
    /**
     * @param  {KeyboardEvent=} event
     */
    async doCopy(event) {
      if (event) { event.preventDefault(); }
      try {
        await this._checkTitle(true);
        const id = await sources.materialsSetups.post(
          createPostParam(this, setupDesc));
        await this._copyMaterials(id, this.$refs.materials.filter || []);
        await this._copyMaterials(id, this.$refs.materials.sample || []);
        await this._copyMaterials(id, this.$refs.materials.source || []);
        this.$router.push('/admin/materials/setups/' + id);
      }
      catch (e) { /* noop */ }
    }
  }
});
export default component;
