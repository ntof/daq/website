// @ts-check

import {
  assign, clone, concat, filter, find, findIndex, forEach, get,
  isEmpty, isFinite, pullAt, sortBy, toNumber, transform } from 'lodash';
import { mapState } from 'vuex';
import Vue from 'vue';
import d from 'debug';
import { sources } from '../../store';
import AddMaterialDialog from './AddMaterialsSetupMaterialDialog.vue';
import RouteUtilsMixin from '../../mixins/RouteUtilsMixin';

const debug = d('app:compo');

/**
 * @typedef {{ addDialog: V.Instance<typeof AddMaterialDialog> }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> &
 *  V.Instance<RouteUtilsMixin>
 * } Instance
 */

const component = /** @type {V.Constructor<any, Refs> } */ (Vue).extend({
  name: 'MaterialsSetupMaterials',
  components: { AddMaterialDialog },
  mixins: [ RouteUtilsMixin ],
  props: {
    materialsSetupId: { type: [ String, Number ], default: null },
    inEdit: { type: Boolean, default: false }
  },
  /**
   * @return {{
   *  filter: any[]|null, sample: any[]|null, source: any[]|null,
   *  loaded: boolean }}
   */
  data() {
    return { filter: null, sample: null, source: null, loaded: false };
  },
  computed: {
    .../** @type {{ infoLoading(): boolean, info(): any[] }} */(mapState('materialsSetupInfo', {
      infoLoading: 'loading', info: 'rows'
    })),
    .../** @type {{ materialsLoading(): boolean, materials(): { [id:string]: any } }} */(mapState('materials', {
      materialsLoading: 'loading', materials: 'db'
    })),
    /** @return {boolean} */
    loading() { return this.infoLoading || this.materialsLoading; },
    /** @return {number[]} */
    ids() {
      /** @type {number[]} */
      const ret = [];
      transform(this.filter || [], (ret, f) => ret.push(f.id), ret);
      transform(this.sample || [], (ret, f) => ret.push(f.id), ret);
      transform(this.source || [], (ret, f) => ret.push(f.id), ret);
      return ret;
    }
  },
  watch: {
    info() { this.filterRows(); },
    materials() { this.filterRows(); },
    materialsSetupId() { this.fetch(); },
    inEdit(value) {
      this.fetch(value === false);
    }
  },
  /** @this {Instance} */
  beforeMount() {
    this.fetch();
    this.filterRows();
  },
  methods: {
    async fetch(nocache = false) {
      this.loaded = nocache ? false : this.loaded;
      sources.materialsSetupInfo.fetch(
        { query: { materialsSetupId: this.materialsSetupId } }, nocache);
      sources.materials.fetchRelated('materialsSetups',
        { id: this.materialsSetupId }, {}, nocache);
    },
    /** @this {Instance} */
    filterRows() {
      if (this.loading || this.loaded) { return; }

      /** @type {any[]} */
      const filter = [];
      /** @type {any[]} */
      const sample = [];
      /** @type {any[]} */
      const source = [];
      forEach(this.info, (matInfo) => {
        var mat = get(this.materials, matInfo.materialId);
        if (!mat) {
          debug('material not found for materialSetupInfo: %o', matInfo);
          return;
        }
        mat = assign({}, mat,
          { status: matInfo.status, position: matInfo.position });
        if (mat.type === 'filter') {
          filter.push(mat);
        }
        else if (mat.type === 'sample') {
          sample.push(mat);
        }
        else if (mat.type === 'source') {
          source.push(mat);
        }
        else {
          debug('unknown material type: %o', mat);
        }
      });
      this.filter = sortBy(filter,
        [ (/** @type {any} */ f) => get(f, 'position', Infinity), 'id' ]);
      this.sample = sortBy(sample,
        [ (/** @type {any} */ f) => get(f, 'position', Infinity), 'id' ]);
      this.source = source;
      this.loaded = true;
    },
    sort() {
      this.filter = sortBy(this.filter,
        [ (/** @type {any} */ f) => get(f, 'position', Infinity), 'id' ]);
      this.sample = sortBy(this.sample,
        [ (/** @type {any} */ f) => get(f, 'position', Infinity), 'id' ]);
      this.source = sortBy(this.source, [ 'id' ]);
    },
    /** @param {boolean} state */
    stateClass(state) {
      return state ? [ "badge-primary" ] : [ "badge-secondary" ];
    },
    /**
     * @param  {string} type
     * @param  {number} id
     */
    remove(type, id) {
      const list = get(this, type);
      Vue.set(this, type, filter(list, (f) => f.id !== id));
    },
    /**
     * @this {Instance}
     * @param {string} type
     */
    async add(type) {
      try {
        this.$refs.addDialog.type = type;
        const material = await this.$refs.addDialog.request();
        if (material) {
          Vue.set(this, type, concat(get(this, type), material));
        }
      }
      catch (err) {
        debug('add error:', err);
      }
    },
    /**
     * @param {string} type
     * @param {number} id
     * @param {number|string|null} value
     */
    setPosition(type, id, value) {
      const mat = find(get(this, [ type ]), { id });
      if (!mat) {
        debug('failed to find material: %s', id);
        return;
      }
      if (isFinite(value)) {
        mat.position = value;
      }
      else {
        mat.position = (isEmpty(value)) ? null : toNumber(value);
      }
    },
    /**
     * @param {string} type
     * @param {number} id
     * @param {boolean} value
     */
    setStatus(type, id, value) {
      const mat = find(get(this, [ type ]), { id });
      if (!mat) {
        debug('failed to find material: %s', id);
        return;
      }
      mat.status = value ? 1 : 0;
      if (type === 'sample' && value) {
        /* sample exchanger can manage only one filter at a time */
        forEach(this.sample, (f) => {
          f.status = (f.id === id) ? 1 : 0;
        });
      }
    },
    /**
     * @param  {string}  type
     * @param  {any[]}  info
     */
    async _submit(type, info) {
      const mat = get(this, [ type ]);
      for (const m of mat) {
        var infoIdx = findIndex(info, (i) => (i.materialId === m.id));
        if ((infoIdx >= 0) &&
            ((m.status !== info[infoIdx].status) ||
             (m.position !== info[infoIdx].position))) {
          debug('removing material (temporarily): %s', m.id);
          /* delete to upload again */
          await sources.materials.deleteRelation('materialsSetups',
            { id: this.materialsSetupId }, m.id);
          pullAt(info, infoIdx);
          infoIdx = -1;
        }
        if (infoIdx < 0) {
          debug('adding material: %s', m.id);
          await sources.materialsSetupInfo.post({
            materialId: m.id, materialsSetupId: this.materialsSetupId,
            status: m.status, position: m.position
          });
        }
        else {
          debug('no changes for material: %s', m.id);
          pullAt(info, infoIdx);
        }
      }
      return info;
    },
    async doSubmit() {
      var info = clone(this.info);
      info = await this._submit("filter", info);
      info = await this._submit("sample", info);
      info = await this._submit("source", info);
      for (var i of info) {
        debug('removing material: %s', i.materialId);
        await sources.materials.deleteRelation('materialsSetups',
          { id: this.materialsSetupId }, i.materialId);
      }
    },
    async doDeleteAll() {
      for (var i of this.info) {
        await sources.materials.deleteRelation('materialsSetups',
          { id: this.materialsSetupId }, i.materialId);
      }
    }
  }
});
export default component;
