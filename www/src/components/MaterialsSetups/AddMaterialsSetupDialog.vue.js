// @ts-check

import OperYearSelector from '../OperYearSelector.vue';
import { sources } from '../../store';
import { createPostParam, setupDesc } from '../../interfaces/database';
import Vue from 'vue';
import d from 'debug';

const debug = d('app:comp');

const component = Vue.extend({
  name: 'AddMaterialsSetupDialog',
  components: { OperYearSelector },
  props: { earNumber: { type: [ String, Number ], default: 1 } },
  methods: {
    async request() {
      this.$refs.earNumber.editValue = this.earNumber;
      this.$refs.name.editValue = '';
      try {
        if (!await this.$refs.dialog.request()) { return; }
        const id = await sources.materialsSetups.post(
          createPostParam(this, setupDesc));
        this.$router.push('/admin/materials/setups/' + id);
      }
      catch (err) {
        debug('doAdd error:', err);
      }
    }
  }
});
export default component;
