// @ts-check

import { assign, get, isEmpty, isNil, noop, set, toNumber } from 'lodash';
import Vue from 'vue';
import OperYearSelector from '../OperYearSelector.vue';
import DBSearchMixin from '../../mixins/DBSearchMixin';
import AddDialog from './AddMaterialsSetupDialog.vue';
import { sources } from '../../store';

/**
 * @typedef {typeof import('../../mixins/RouteUtilsMixin').default} RouteUtilsMixin
 * @typedef {{
 *   addDialog: V.Instance<typeof AddDialog>
 *   operYear: V.Instance<typeof OperYearSelector>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<options, Refs>> &
 *   V.Instance<typeof DBSearchMixin> &
 *   V.Instance<ReturnType<BaseVue.BaseKeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin>
 * } Instance
 */

const options = { source: 'materialsSetups' };

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'MaterialsSetups',
  components: { OperYearSelector, AddDialog },
  mixins: [ DBSearchMixin ],
  ...options,
  /**
   * @return {{ earNumber: number|string|null }}
   */
  data() {
    return { earNumber: null };
  },
  /** @this {Instance} */
  async mounted() {
    this.onKey('ctrl-s-keydown', this.doSearch);

    await this.fetch();
  },
  methods: {
    /** @this {Instance} */
    async loadRoute() {
      try {
        const query = this.$route.query;
        this.page = toNumber(get(query, 'page', 0));

        const year = /** @type string */(get(query, 'year', ''));
        await this.$refs.operYear.setEditValue(year).catch(noop);

        const earNumber = /** @type {string|number} */ (get(query, 'earNumber', null));
        this.earNumber = earNumber;
        set(this.$refs, [ 'earNumber', 'editValue' ],
          earNumber);
      }
      catch (e) {
        console.log(e);
      }
    },
    /** @this {Instance} */
    async fetch() {
      await this.loadRoute();
      /** @type {any} */
      const params = {
        max: this.max, offset: this.max * this.page
      };
      this.prepareQuery(params);
      get(sources, this.$options.source).fetch(params);
    },
    /**
     * @this {Instance}
     * @param  {any} params
     * @return {any}
     */
    prepareQuery(params) {
      params = assign(params, {
        query: {},
        sort: 'id:desc'
      });

      /** @type {any} */
      var value = get(this.$refs, [ 'operYear', 'editValue' ]);
      if (!isNil(value) && value !== '') {
        params.query.operYear = value;
      }
      value = get(this.$refs, [ 'earNumber', 'editValue' ]);
      if (!isEmpty(value)) {
        params.query.earNumber = value;
      }
      return params;
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    doSearch(event) {
      if (event) {
        event.preventDefault();
      }
      this.addQuery({
        page: "0",
        year: /** @type string */(get(this.$refs, [ 'operYear', 'editValue' ])),
        earNumber: get(this.$refs, [ 'earNumber', 'editValue' ])
      });
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doAdd(event) {
      if (event) { event.preventDefault(); }
      await this.$refs.addDialog.request().catch(noop);
    }
  }
});
export default component;
