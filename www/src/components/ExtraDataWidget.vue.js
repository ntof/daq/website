// @ts-check

import Vue from 'vue';
import { BaseLogger as logger } from '@cern/base-vue';
import { get, isNil } from 'lodash';
import { sources } from '../store';

/**
 * @typedef {V.Instance<typeof component, V.ExtVue<any, any>> } Instance
 */

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'ExtraDataWidget',
  props: {
    title: { type: String, default: null },
    extraDataKey: { type: String, default: null, required: true }
  },
  /**
   * @return {{
   *  inEdit: boolean,
   *  saving: boolean,
   *  loading: boolean,
   *  valueInDb: boolean,
   *  dbValue: string?
   * }}
   */
  data() {
    return {
      inEdit: false,
      saving: false,
      loading: true,
      valueInDb: false,
      dbValue: ''
    };
  },
  /** @this {Instance} */
  mounted() {
    this.fetch();
  },
  methods: {
    /**
     * @param {boolean} [nocache=false]
     */
    async fetch() {
      try {
        this.loading = true;
        this.dbValue = await sources.extraData.getById(this.extraDataKey)
        .then((res) => get(res, 'value'));
        this.valueInDb = true;
      }
      catch (err) { this.valueInDb = false; }
      finally {
        this.loading = false;
      }
    },
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value) {
      this.inEdit = value;
    },
    /**
     * @this {Instance}
     * @param {string} key
     */
    async doSave() {
      let refresh = false;
      try {
        this.saving = true;
        const newVal = get(this.$refs, [ 'mw', 'editValue' ]);
        if (!isNil(newVal) && newVal !== this.dbValue) {
          refresh = true;
          if (this.valueInDb) {
            await sources.extraData.put(this.extraDataKey, { value: newVal });
          }
          else {
            await sources.extraData.post({ key: this.extraDataKey, value: newVal });
            this.valueInDb = true;
          }
        }
      }
      catch (err) { logger.error(err); }
      finally {
        this.saving = false;
        if (refresh) { await this.fetch(); }
        this.setEdit(false);
      }
    }
  }
});

export default component;
