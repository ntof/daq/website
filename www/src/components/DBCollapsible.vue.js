// @ts-check

import { get, isEmpty, merge } from 'lodash';
import Vue from 'vue';
import { sources } from '../store';

/**
 * @typedef {V.Instance<typeof component>} Instance
 */

const component = Vue.extend({
  name: 'DBCollapsible',
  props: {
    title: { type: String, default: null },
    source: { type: String, default: null },
    parentItem: { type: Object, default: null },
    parentType: { type: String, default: null },
    max: { type: Number, default: 100 },
    query: { type: Object, default: null },
    expand: { type: Boolean, default: false },
    prefetch: { type: Boolean, default: true }
  },
  data() {
    return { offset: 0 };
  },
  computed: {
    /** @return {any[]} */
    rows() { return get(this.$store, [ 'state', this.source, 'rows' ]); },
    /** @return {boolean} */
    loading() { return get(this.$store, [ 'state', this.source, 'loading' ]); }
  },
  watch: {
    parentItem() { this.fetch(); },
    rows() { this.checkPastEnd(); },
    loading() { this.checkPastEnd(); }
  },
  beforeMount() {
    if (this.prefetch) {
      this.fetch();
    }
  },
  methods: {
    fetch(nocache = false) {
      if (this.parentType) {
        get(sources, this.source).fetchRelated(this.parentType, this.parentItem,
          merge({ max: this.max, offset: this.offset }, this.query), nocache);
      }
      else {
        get(sources, this.source).fetch(merge({ max: this.max, offset: this.offset }, this.query), nocache);
      }
    },
    /**
     * @param  {boolean} value
     */
    onExpanded(value) {
      if (value) { this.fetch(); }
    },
    next() {
      this.offset += this.max;
      return this.fetch();
    },
    previous() {
      if (this.offset > 0) {
        this.offset = Math.max(0, this.offset - this.max);
      }
      return this.fetch();
    },
    checkPastEnd() {
      if (!this.loading && this.offset && isEmpty(this.rows)) {
        this.previous();
      }
    }
  }
});
export default component;
