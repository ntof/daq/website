// @ts-check

import Vue from 'vue';
import { mapGetters, mapState } from 'vuex';
import { get, map, noop } from 'lodash';
import DBCollapsible from '../DBCollapsible.vue';
import EditCommentDialog from './EditCommentDialog.vue';
import AddCommentDialog from './AddCommentDialog.vue';
import UserLink from '../Users/UserLink.vue';

import { sources } from '../../store';
import RouteUtilsMixin from '../../mixins/RouteUtilsMixin';

/**
 * @typedef {{
 *   editDialog: V.Instance<EditCommentDialog>,
 *   addDialog: V.Instance<AddCommentDialog>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs> > &
 *  V.Instance<typeof RouteUtilsMixin>
 * } Instance
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'CommentsList',
  components: {
    DBCollapsible, EditCommentDialog,
    UserLink, AddCommentDialog
  },
  mixins: [ RouteUtilsMixin ],
  props: {
    inEdit: { type: Boolean, default: false },
    run: { type: Object, default: null },
    expand: { type: Boolean, default: false }
  },
  computed: {
    .../** @type {{ users(): any }} */(mapState('usersCache', { users: 'db' })),
    .../** @type {{ comments(): any }} */(mapState('comments', { comments: 'rows' })),
    .../** @type {{ username(): string, canEdit(): boolean }} */(mapGetters([ 'username', 'canEdit' ]))
  },
  watch: {
    comments(rows) {
      sources.usersCache.fetchMore(map(rows, 'userId'));
    }
  },
  methods: {
    /**
     * @this {Instance}
     * @param {number} id
     */
    getUser(id) {
      return get(this.users, [ id ]);
    },
    /**
     * @this {Instance}
     * @param {number} id
     */
    isCurrentUser(id) {
      return get(this.users, [ id, 'login' ]) === this.username;
    },
    /**
     * @this {Instance}
     * @param {AppStore.Comment} comment
     */
    async doEdit(comment) {
      await this.$refs.editDialog.edit(comment).catch(noop);
    },
    /** @this {Instance} */
    async doAdd() {
      await this.$refs.addDialog.add(this.run).catch(noop);
    }
  }
});
export default component;
