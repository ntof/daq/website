// @ts-check

import { assign, get, map, noop, toNumber, toString } from 'lodash';
import Vue from 'vue';
import { mapGetters, mapState } from 'vuex';
import EditCommentDialog from './EditCommentDialog.vue';
import UserLink from '../Users/UserLink.vue';
import { AreaByPrefix } from '../../Consts';
import DBSearchMixin from '../../mixins/DBSearchMixin';
import { sources } from '../../store';

/**
 * @typedef {typeof import('../../mixins/RouteUtilsMixin').default} RouteUtilsMixin
 * @typedef {import('@cern/base-vue').BaseKeyboardEventMixin} KeyboardEventMixin
 * @typedef {{
 *  editDialog: V.Instance<EditCommentDialog>
 * }} Refs
 * @typedef {V.Instance<component, V.ExtVue<options, Refs>> &
 *   V.Instance<typeof DBSearchMixin> &
 *   V.Instance<ReturnType<KeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin>
 * } Instance
 */

const options = { source: 'comments' };

const component = /** @type {V.Constructor<options, Refs> } */ (Vue).extend({
  name: 'Comments',
  components: { UserLink, EditCommentDialog },
  mixins: [ DBSearchMixin ],
  ...options,
  /**
   * @return {{ earNumber: number, inEdit: boolean }}
   */
  data() {
    return { earNumber: 1, inEdit: get(this.$route, [ 'meta', 'isAdmin' ]) };
  },
  computed: {
    /** @return {{ name: string, prefix:number, dns: string }|undefined} */
    area() {
      return get(AreaByPrefix, this.earNumber);
    },
    /** @return {number} */
    prefix() {
      return (this.area) ? (this.area.prefix * 100000) : 0;
    },
    .../** @type {{ users(): any }} */(mapState('usersCache', { users: 'db' })),
    .../** @type {{ username(): string, canEdit(): boolean }} */(mapGetters([ 'username', 'canEdit' ]))
  },
  watch: {
    rows(rows) {
      sources.usersCache.fetchMore(map(rows, 'userId'));
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
  },
  methods: {
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value) {
      if (this.canEdit) {
        this.inEdit = value;
      }
    },
    /**
     * @this {Instance}
     * @param  {string} type
     */
    setType(type) {
      this.addQuery({ type });
    },
    /**
     * @this {Instance}
     * @param {number} id
     */
    getUser(id) {
      return get(this.users, [ id ]);
    },
    /**
     * @this {Instance}
     * @param {number} id
     */
    isCurrentUser(id) {
      return get(this.users, [ id, 'login' ]) === this.username;
    },
    /** @this {Instance} */
    loadRoute() {
      try {
        const query = this.$route.query;
        this.page = toNumber(get(query, 'page', 0));
        this.earNumber = toNumber(get(query, 'earNumber', 1));
      }
      catch (e) {
        console.log(e);
      }
    },
    /**
     * @param  {any} params
     * @return {any}
     */
    prepareQuery(params) {
      params = assign(params, {
        query: { },
        sort: 'runNumber:desc,date:desc'
      });
      if (this.prefix !== 0) {
        params.query.runNumber = {
          '$gte': this.prefix, '$lte': this.prefix + 100000 - 1
        };
      }
      return params;
    },
    /**
     * @this {Instance}
     * @param  {number} num
     */
    setEarNumber(num) {
      this.addQuery({ earNumber: toString(num), page: "0" });
    },
    /**
     * @this {Instance}
     * @param {AppStore.Comment} comment
     */
    async doEdit(comment) {
      await this.$refs.editDialog.edit(comment).catch(noop);
    }
  }
});

export default component;
