// @ts-check

import { get, invoke } from 'lodash';
import { sources } from '../../store';
import Vue from 'vue';
import d from 'debug';
import $ from 'jquery';
import RouteUtilsMixin from '../../mixins/RouteUtilsMixin';

const debug = d('app:comp');

/**
 * @typedef {{
 *  dialog: V.Instance<typeof BaseVue.BaseDialog>
 *  md: V.Instance<typeof BaseVue.BaseMarkdownWidget>
 * }} Refs
 * @typedef {V.Instance<component, V.ExtVue<any, Refs>> &
 *  V.Instance<typeof RouteUtilsMixin>
 * } Instance
 */

const component = /** @type {V.Constructor<any, Refs> } */ (Vue).extend({
  name: 'AddCommentDialog',
  mixins: [ RouteUtilsMixin ],
  /**
   * @return {{ comment: any, visible: boolean }}
   */
  data() {
    return { comment: {}, visible: false };
  },
  mounted() {
    $(this.$el).on('shown.bs.modal', this.onShown);
  },
  beforeDestroy() {
    $(this.$el).off('shown.bs.modal', this.onShown);
  },
  methods: {
    /**
     * @this {Instance}
     * @param {any} run
     */
    async add(run) {
      this.visible = true;
      try {
        if (await this.$refs.dialog.request()) {
          await sources.comments.post({
            comments: get(this, [ '$refs', 'md', 'editValue' ]),
            runNumber: get(run, [ 'runNumber' ])
          }, this.isAdmin);
        }
        sources.comments.refresh();
      }
      catch (err) {
        debug('%s error:', this.$options.name, err);
      }
      this.visible = false;
    },
    /** @this {Instance} */
    onShown() {
      invoke(this, [ '$refs', 'md', 'refreshEditor' ]);
      invoke(this, [ '$refs', 'md', 'focus' ]);
    }
  }
});
export default component;
