// @ts-check
import { get, isNil } from 'lodash';
import Vue from 'vue';
import { mapGetters, mapState } from 'vuex';
import { BaseKeyboardEventMixin as KeyboardEventMixin } from '@cern/base-vue';
import { createPutParam, materialDesc } from '../../interfaces/database';
import DocumentsList from '../Documents/DocumentsList.vue';
import DBCollapsible from '../DBCollapsible.vue';
import { sources } from '../../store';
import RouteUtilsMixin from '../../mixins/RouteUtilsMixin';

/**
 * @typedef {{ deleteDialog: V.Instance<typeof BaseVue.BaseDialog> }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>> &
 *   V.Instance<ReturnType<KeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin>
 * } Instance
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'MaterialInfo',
  components: { DocumentsList, DBCollapsible },
  mixins: [ KeyboardEventMixin({ local: false }), RouteUtilsMixin ],
  /**
   * @return {{ inEdit: boolean }}
   */
  data() {
    return { inEdit: get(this.$route, [ 'meta', 'isAdmin' ]) };
  },
  computed: {
    /** @return {string} */
    id() { return get(this.$route, [ 'params', 'id' ]); },
    .../** @type {{ showKeyHints(): boolean }} */(mapState('ui', [ 'showKeyHints' ])),
    .../** @type {{ materialLoading(): boolean, material(): any }} */(mapState('materials', {
      materialLoading: (state) => state.loading,
      material(state) { return get(state, [ 'db', this.id ]); }
    })),
    .../** @type {{ docsLoading(): boolean, docs(): any[] }} */(mapState('documents',
      { docs: 'rows', docsLoading: 'loading' })),
    .../** @type {{ setupsLoading(): boolean, setups(): any[] }} */(mapState('materialsSetups',
      { setups: 'rows', setupsLoading: 'loading' })),
    .../** @type {{ canEdit(): boolean }} */(mapGetters([ 'canEdit' ])),
    /** @return {boolean} */
    loading() {
      return this.materialLoading || this.docsLoading || this.setupsLoading;
    }
  },
  watch: {
    id() {
      if (isNil(this.material)) {
        this.fetch();
      }
    }
  },
  /** @this {Instance} */
  beforeMount() {
    if (isNil(this.material)) {
      this.fetch();
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('ctrl-s-keydown', this.doSubmit);
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
  },
  methods: {
    /**
     * @param  {boolean} [nocache=false]
     */
    async fetch(nocache = false) {
      const id = get(this.$route, [ 'params', 'id' ]);
      const params = { max: 1, query: { id } };
      sources.materials.fetch(params, nocache);
      sources.materialsSetups.fetchRelated('materials', { id }, { max: 100 }, nocache);
    },
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value) {
      if (this.canEdit) {
        this.inEdit = value;
      }
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doSubmit(event) {
      if (event) {
        event.preventDefault();
      }
      try {
        const params = createPutParam(this, this.material, materialDesc);
        await sources.materials.put(this.material.id, params);
        this.inEdit = false;
        await this.fetch(true);
      }
      catch (e) { /* noop */ }
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doDelete(event) {
      if (event) {
        event.preventDefault();
      }
      try {
        if (!await this.$refs.deleteDialog.request()) { return; }
        await sources.materials.delete(this.material.id);
        sources.materials.refresh();
        this.$router.push('/admin/materials');
      }
      catch (e) { /* noop */ }
    }
  }
});
export default component;
