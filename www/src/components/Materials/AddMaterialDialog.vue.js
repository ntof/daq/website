// @ts-check

import { assign } from 'lodash';
import { sources } from '../../store';
import { createPostParam, materialDesc } from '../../interfaces/database';
import Vue from 'vue';
import d from 'debug';

const debug = d('app:comp');

const component = Vue.extend({
  name: 'AddMaterialDialog',
  props: { type: { type: String, default: 'filter' } },
  methods: {
    async request() {
      this.$refs.type.editValue = this.type;
      this.$refs.title.editValue = '';
      this.$refs.compound.editValue = '';
      this.$refs.massNumber.editValue = -1;
      this.$refs.atomicNumber.editValue = -1;
      try {
        if (!await this.$refs.dialog.request()) { return; }
        const id = await sources.materials.post(
          assign({ status: true, description: '' },
            createPostParam(this, materialDesc)));
        this.$router.push('/admin/materials/' + id);
      }
      catch (err) {
        debug('doAdd error:', err);
      }
    }
  }
});
export default component;
