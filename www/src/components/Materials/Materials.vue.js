// @ts-check

import { assign, get, isFinite, isNil, noop, set, toNumber, toString } from 'lodash';
import Vue from 'vue';
import AddMaterialDialog from './AddMaterialDialog.vue';
import DBSearchMixin from '../../mixins/DBSearchMixin';

/**
 * @typedef {typeof import('../../mixins/RouteUtilsMixin').default} RouteUtilsMixin
 * @typedef {BaseVue.BaseKeyboardEventMixin} KeyboardEventMixin
 * @typedef {{ addDialog: V.Instance<AddMaterialDialog> }} Refs
 * @typedef {V.Instance<component, V.ExtVue<options, Refs>> &
 *   V.Instance<typeof DBSearchMixin> &
 *   V.Instance<ReturnType<KeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin>
 * } Instance
 */

const options = { source: 'materials' };

const component = /** @type {V.Constructor<options, Refs> } */ (Vue).extend({
  name: 'Materials',
  components: { AddMaterialDialog },
  mixins: [ DBSearchMixin ],
  ...options,
  /**
   * @return {{ type: string }}
   */
  data() {
    return { type: 'filter' };
  },
  methods: {
    /**
     * @this {Instance}
     * @param {string} type
     */
    typeButtonClass(type) {
      return (this.type === type) ? 'btn-primary' : 'btn-light text-muted';
    },
    /**
     * @this {Instance}
     * @param  {string} type
     */
    setType(type) {
      this.addQuery({ type });
    },
    /** @this {Instance} */
    loadRoute() {
      try {
        const query = this.$route.query;
        this.page = toNumber(get(query, 'page', 0));
        this.type = toString(get(query, 'type', 'filter'));

        set(this.$refs, [ 'element', 'editValue' ],
          get(query, 'element'));
        set(this.$refs, [ 'atomicNumber', 'editValue' ],
          get(query, 'atomicNumber'));
        set(this.$refs, [ 'massNumber', 'editValue' ],
          get(query, 'massNumber'));
      }
      catch (e) {
        console.log(e);
      }
    },
    /**
     * @param  {any} params
     * @return {any}
     */
    prepareQuery(params) {
      params = assign(params, {
        query: { type: this.type },
        sort: 'id:desc'
      });

      if (!this.isAdmin) {
        params.query.status = true;
      }

      /** @type {any} */
      var value = get(this.$refs, [ 'element', 'editValue' ]);
      if (!isNil(value)) {
        params.query.compound = { '$like': this.globLike(value) };
      }
      value = get(this.$refs, [ 'massNumber', 'editValue' ]);
      if (isFinite(toNumber(value))) {
        params.query.massNumber = value;
      }
      value = get(this.$refs, [ 'atomicNumber', 'editValue' ]);
      if (isFinite(toNumber(value))) {
        params.query.atomicNumber = value;
      }
      return params;
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    doSearch(event) {
      if (event) {
        event.preventDefault();
      }
      this.addQuery({
        type: this.type,
        page: "0",
        element: get(this.$refs, [ 'element', 'editValue' ]),
        atomicNumber: get(this.$refs, [ 'atomicNumber', 'editValue' ]),
        massNumber: get(this.$refs, [ 'massNumber', 'editValue' ])
      });
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doAdd(event) {
      if (event) { event.preventDefault(); }
      await this.$refs.addDialog.request().catch(noop);
    }
  }
});

export default component;
