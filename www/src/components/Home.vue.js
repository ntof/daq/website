// @ts-check

import Vue from 'vue';
import ExtraDataWidget from './ExtraDataWidget.vue';

/**
 * @typedef {V.Instance<typeof component, V.ExtVue<any, any>> } Instance
 */

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'Home',
  components: { ExtraDataWidget }
});
export default component;
