// @ts-check

import { assign } from 'lodash';
import { sources } from '../../store';
import { createPostParam, detectorDesc } from '../../interfaces/database';
import Vue from 'vue';
import d from 'debug';

const debug = d('app:comp');

const component = Vue.extend({
  name: 'AddDetectorDialog',
  methods: {
    async request() {
      this.$refs.name.editValue = '';
      this.$refs.daqName.editValue = '';
      this.$refs.trecNumber.editValue = -1;
      try {
        if (!await this.$refs.dialog.request()) { return; }
        const id = await sources.detectors.post(
          assign({ description: '' },
            createPostParam(this, detectorDesc)));
        this.$router.push('/admin/detectors/' + id);
      }
      catch (err) {
        debug('doAdd error:', err);
      }
    }
  }
});
export default component;
