// @ts-check

import { assign, get, isFinite, isNil, noop, set, toNumber } from 'lodash';
import Vue from 'vue';
import AddDetectorDialog from './AddDetectorDialog.vue';
import DBSearchMixin from '../../mixins/DBSearchMixin';

/**
 * @typedef {typeof import('../../mixins/RouteUtilsMixin').default} RouteUtilsMixin
 * @typedef {import('@cern/base-vue').BaseKeyboardEventMixin} KeyboardEventMixin
 * @typedef {{ addDialog: V.Instance<AddDetectorDialog> }} Refs
 * @typedef {V.Instance<component, V.ExtVue<options, Refs>> &
 *   V.Instance<typeof DBSearchMixin> &
 *   V.Instance<ReturnType<KeyboardEventMixin>> &
 *   V.Instance<RouteUtilsMixin>
 * } Instance
 */

const options = { source: 'detectors' };

const component = /** @type {V.Constructor<options, Refs> } */ (Vue).extend({
  name: 'Detectors',
  components: { AddDetectorDialog },
  mixins: [ DBSearchMixin ],
  ...options,
  methods: {
    /** @this {Instance} */
    loadRoute() {
      try {
        const query = this.$route.query;
        this.page = toNumber(get(query, 'page', 0));

        set(this.$refs, [ 'name', 'editValue' ],
          get(query, 'name'));
        set(this.$refs, [ 'daqName', 'editValue' ],
          get(query, 'daqName'));
        set(this.$refs, [ 'trecNumber', 'editValue' ],
          get(query, 'trecNumber'));
      }
      catch (e) {
        console.log(e);
      }
    },
    /**
     * @param  {any} params
     * @return {any}
     */
    prepareQuery(params) {
      params = assign(params, {
        query: {},
        sort: 'id:desc'
      });

      /** @type {any} */
      var value = get(this.$refs, [ 'name', 'editValue' ]);
      if (!isNil(value)) {
        params.query.name = { '$like': this.globLike(value) };;
      }
      value = get(this.$refs, [ 'daqName', 'editValue' ]);
      if (!isNil(value)) {
        params.query.daqName = { '$like': this.globLike(value) };
      }
      value = get(this.$refs, [ 'trecNumber', 'editValue' ]);
      if (isFinite(toNumber(value))) {
        params.query.trecNumber = value;
      }
      return params;
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    doSearch(event) {
      if (event) {
        event.preventDefault();
      }
      this.addQuery({
        page: "0",
        name: get(this.$refs, [ 'name', 'editValue' ]),
        daqName: get(this.$refs, [ 'daqName', 'editValue' ]),
        trecNumber: get(this.$refs, [ 'trecNumber', 'editValue' ])
      });
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    async doAdd(event) {
      if (event) { event.preventDefault(); }
      await this.$refs.addDialog.request().catch(noop);
    }
  }
});

export default component;
