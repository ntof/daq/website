// @ts-check

import moment from 'moment';
import { get, has, isString, lowerFirst, replace } from 'lodash';
import Vue from 'vue';

/** @type {string} */
var _currentUrl = window.location.origin + window.location.pathname;

/**
 * @return {string}
 */
export function currentUrl() {
  return _currentUrl;
}

/**
 * @param {string} url
 */
export function setCurrentUrl(url) {
  _currentUrl = url;
}

/**
 * @param  {string} dns DIM dns hostname
 * @return {string} dns proxy url
 */
export function dnsUrl(dns/*: any */) {
  return _currentUrl + '/' + dns + '/dns/query';
}

let id = 0;
/**
* @return {string}
*/
export function genId() {
  return 'a-' + (++id);
}


export const UrlUtilitiesMixin = Vue.extend({
  methods: {
    getProxy() {
      return _currentUrl;
    },
    currentUrl() {
      return _currentUrl;
    },
    /**
     * @param  {string} dns
     */
    dnsUrl(dns/*: any */) {
      return _currentUrl + '/' + dns + '/dns/query';
    }
  }
});

/**
 * @param  {?number=} timestamp
 * @return {string}
 */
export const getHumanReadableDate = (timestamp /*: number*/) => {
  if (!timestamp || timestamp === 0) {
    return "Not set.";
  }
  var x = moment.unix(timestamp);
  var y = moment();
  // if (x < y) { return "Expired."; } // Should never happen?
  return moment.duration(x.diff(y)).humanize(true);
};

/**
 * @param  {string} prefix string to append on errors
 * @param  {string|Error} err
 * @return {string|Error}
 */
export function errorPrefix(prefix /*: string*/, err /*: any */) {
  if (isString(err)) {
    return prefix + err;
  }
  else if (has(err, 'message')) {
    err.message = prefix + lowerFirst(err.message);
  }
  throw err;
}

const templateRegex = /{([^\}]*)}/g;
/**
 * @brief compile a string template
 * @details can't use "template" from lodash
 * (with { interpolate: /{([^\}]*)}/g }), since it uses eval.
 * @details uses "{word}" for parts to be replaced
 * @param  {string} source format string
 * @param  {{ [key:string]: any }} repl replacement object
 * @param  {boolean} keep whereas to keep unmatched braces
 * @return {string}
 */
export function format(source, repl, keep = false) {
  return replace(source, templateRegex, function(match, key) {
    return get(repl, key, keep ? match : '');
  });
}
