// @ts-check
import { keyBy } from 'lodash';

/* this one is ordered on purpose */
export const AreaList = [
  { name: 'EAR1', prefix: 1, dns: 'ntofproxy-1.cern.ch' },
  { name: 'EAR2', prefix: 2, dns: 'ntofproxy-2.cern.ch' },
  { name: 'EAR3', prefix: 3, dns: 'ntofdaq-m5.cern.ch' },
  { name: 'LAB', prefix: 9, dns: 'ntofdaq-m12.cern.ch' }
];

/* for convenience access to are metadata */
export const Area = keyBy(AreaList, 'name');

export const AreaByPrefix = keyBy(AreaList, 'prefix');

export const ShiftKindType = {
  LEADER: 'shift-leader',
  NIGHT: 'night',
  MORNING: 'morning',
  AFTERNOON: 'afternoon'
};

export const ShiftKind = {
  'shift-leader': { alias: 'L', startHour: 0, period: '0-24h' },
  'night': { alias: 'N', startHour: 0, period: '0h-8h' },
  'morning': { alias: 'M', startHour: 8, period: '8h-16h' },
  'afternoon': { alias: 'A', startHour: 16, period: '16h-24h' }
};

export const SpecialUsers = {
  CANCELLED: "Cancelled",
  RESERVED: "Reserved",
  NTOFALIAS: "__nTOF"
};

export const ShiftStatsKind = {
  BYUSER: 'by-user',
  BYINSTITUTE: 'by-institute'
};

/** @type {string} */
// @ts-ignore
const VER = (typeof VERSION === 'undefined') ? 'unknown' : VERSION; // jshint ignore:line
export { VER as VERSION };
