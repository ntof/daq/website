// @ts-check

export = Iface
export as namespace Iface

declare namespace Iface {
  declare namespace DB {
    interface TableDesc {
      id: string,
      rows: ColDesc[]
    }

    interface ColDesc<T = any> {
      name: string,
      conv?: (value: any, col: ColDesc) => T,
      nullable?: boolean
    }
  }
}
