// @ts-check
import { createPostParam, createPutParam,
  numberConv, stringConv } from './database-utils';

export { createPostParam, createPutParam };

/** @type {Iface.DB.TableDesc} */
export const materialDesc = {
  id: 'id',
  rows: [
    { name: 'type', nullable: false, conv: stringConv },
    { name: 'title', nullable: false, conv: stringConv },
    { name: 'compound', nullable: false, conv: stringConv },
    { name: 'massNumber', nullable: false, conv: numberConv },
    { name: 'mass', conv: numberConv },
    { name: 'atomicNumber', conv: numberConv },
    { name: 'thickness', conv: numberConv },
    { name: 'diameter', conv: numberConv },
    { name: 'status', conv: (v) => ((v) ? 1 : 0) },
    { name: 'areaDensity', conv: numberConv },
    { name: 'description', nullable: false, conv: stringConv }
  ]
};

/** @type {Iface.DB.TableDesc} */
export const detectorDesc = {
  id: 'id',
  rows: [
    { name: 'name', nullable: false, conv: stringConv },
    { name: 'daqName', nullable: false, conv: stringConv },
    { name: 'trecNumber', nullable: false, conv: stringConv },
    { name: 'description', nullable: false, conv: stringConv }
  ]
};

/** @type {Iface.DB.TableDesc} */
export const containerDesc = {
  id: 'id',
  rows: [
    { name: 'name', nullable: false, conv: stringConv },
    { name: 'trecNumber', nullable: false, conv: stringConv },
    { name: 'description', nullable: false, conv: stringConv }
  ]
};

/** @type {Iface.DB.TableDesc} */
export const setupDesc = {
  id: 'id',
  rows: [
    { name: 'name', nullable: false, conv: stringConv },
    { name: 'earNumber', nullable: false, conv: numberConv },
    { name: 'operYear', nullable: false, conv: numberConv },
    { name: 'description', nullable: false, conv: stringConv }
  ]
};

/** @type {Iface.DB.TableDesc} */
export const runDesc = {
  id: 'runNumber',
  rows: [
    { name: 'title', nullable: false, conv: stringConv },
    { name: 'description', nullable: false, conv: stringConv }
  ]
};

/** @type {Iface.DB.TableDesc} */
export const userDesc = {
  id: 'id',
  rows: [
    { name: 'firstName', nullable: false, conv: stringConv },
    { name: 'lastName', nullable: false, conv: stringConv },
    { name: 'email', nullable: false, conv: stringConv },
    { name: 'involvments', nullable: false, conv: stringConv },
    { name: 'status', nullable: false, conv: (v) => ((v) ? 1 : 0) }
  ]
};

/** @type {Iface.DB.TableDesc} */
export const instituteDesc = {
  id: 'id',
  rows: [
    { name: 'name', nullable: false, conv: stringConv },
    { name: 'city', nullable: false, conv: stringConv },
    { name: 'country', nullable: false, conv: stringConv },
    { name: 'alias', nullable: false, conv: stringConv },
    { name: 'status', nullable: false, conv: (v) => ((v) ? 1 : 0) }
  ]
};
