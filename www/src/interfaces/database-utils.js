// @ts-check
import { defaultTo, get, isEmpty, isFinite, isNil, toNumber,
  toString, transform } from 'lodash';
import d from 'debug';

const debug = d('app:db:conv');

/**
 * @param  {string|number|null|undefined} value
 * @param  {Iface.DB.ColDesc} desc
 * @return {number|null|undefined}
 */
export function numberConv(value, desc) {
  if (!isFinite(value) && isEmpty(value)) {
    return get(desc, 'nullable', true) ? null : 0;
  }
  return toNumber(value);
}

/**
 * @param  {string|number|null|undefined} value
 * @param  {Iface.DB.ColDesc} desc
 * @return {string|null|undefined}
 */
export function stringConv(value, desc) {
  if (isNil(value)) {
    return get(desc, 'nullable', true) ? null : '';
  }
  return toString(value);
}

/**
 * @brief create an update parameter
 * @param  {V.Instance<any>} component
 * @param  {any} current current values for item
 * @param  {Iface.DB.TableDesc} description
 * @return {any}
 */
export function createPutParam(component, current, description) {
  /** @type {any} */
  const ret = transform(description.rows, (ret, col) => {
    const input = get(component, [ '$refs', col.name ]);
    if (!isNil(input)) {
      var value = get(input, 'editValue');
      if (col.conv) {
        value = col.conv(value, col);
      }
      if (value !== current[col.name]) {
        ret[col.name] = value;
      }
    }
  }, /** @type {any} */ ({}));
  debug('PUT param: %o', ret);
  return isEmpty(ret) ? null : ret;
}

/**
 * @brief create an update parameter
 * @param  {V.Instance<any>} component
 * @param  {Iface.DB.TableDesc} description
 * @return {any}
 */
export function createPostParam(component, description) {
  /** @type {any} */
  const ret = transform(description.rows, (ret, col) => {
    const input = get(component, [ '$refs', col.name ]);
    if (!isNil(input)) {
      let value = get(input, 'editValue');
      if (col.conv) {
        value = col.conv(value, col);
      }
      ret[col.name] = value;
    }
    else if (!defaultTo(col.nullable, true) && col.conv) {
      const value = col.conv(null, col);
      if (!isNil(value)) {
        ret[col.name] = value;
      }
    }
  }, /** @type {any} */ ({}));
  debug('POST param: %o', ret);
  return ret;
}
