// @ts-check

import { get, isEqual, size, toString } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';
import {
  BaseAnimationBlock as AnimBlock,
  BaseCard as Card,
  BaseKeyboardEventMixin as KeyboardEventMixin } from '@cern/base-vue';
import RouteUtilsMixin from './RouteUtilsMixin';
import { sources } from '../store';
import d from 'debug';

const debug = d('app:compo');

const s_globLikeRe = /[%_?*]/g;
const s_globLikeMap = {
  '%': '\%',
  '_': '\_',
  '?': '_',
  '*': '%'
};

/**
 * @typedef {{ }} Refs
 * @typedef {{ source: string }} Options
 * @typedef {V.Instance<typeof component, V.ExtVue<Options, Refs>> &
 *   V.Instance<typeof RouteUtilsMixin> &
 *   V.Instance<ReturnType<KeyboardEventMixin>>
 * } Instance
 */

const component = /** @type {V.Constructor<Options, Refs>} */ (Vue).extend({
  name: 'DBSearchMixin',
  components: { Card, AnimBlock },
  mixins: [ KeyboardEventMixin({ local: false }), RouteUtilsMixin ],
  /**
   * @return {{
   *   max: number,
   *   page: number,
   * }}
   */
  data() {
    return {
      max: get(this.$store, [ 'state', 'ui', 'max' + this.$route.name ], 40), page: 0
    };
  },
  computed: {
    .../** @type {{ showKeyHints(): boolean }} */mapState('ui', {
      showKeyHints: (state) => state.showKeyHints
    }),
    /**
     * @this {Instance}
     * @return {any[]}
     */
    rows() { return get(this.$store.state, this.$options.source, {}).rows; },
    /**
     * @this {Instance}
     * @return {boolean}
     */
    loading() { return get(this.$store.state, this.$options.source, {}).loading; }
  },
  watch: {
    '$route.query': function(query, old) {
      if (!isEqual(query, old)) {
        this.fetch();
      }
    },
    /** @this {Instance} */
    max(m, old) {
      if (m === old) { return; }
      this.$store.commit('ui/update', { ['max' + this.$route.name]: m });
      if (!this.addQuery({ page: "0" })) {
        this.fetch(); /* forced update */
      }
    },
    rows(r) {
      if (!this.loading && size(r) === 0 && this.page > 0) {
        this.goToPage(this.page - 1);
      }
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('ctrl-s-keydown', this.doSearch);
    this.fetch();
  },
  methods: {
    loadRoute() {},
    fetch() {
      this.loadRoute();
      /** @type {any} */
      const params = {
        max: this.max, offset: this.max * this.page
      };
      this.prepareQuery(params);
      get(sources, this.$options.source).fetch(params);
    },
    /**
     * @param  {any} params
     */
    prepareQuery(params) {
      debug('prepareQuery not implemented: %o', params);
    },
    /**
     * @this {Instance}
     * @param  {KeyboardEvent=} event
     */
    doSearch(event) {
      if (event) { event.preventDefault(); }

      debug('doSearch not implemented');
      this.addQuery({ page: "0" });
    },
    /**
     * @this {Instance}
     * @param  {number} num
     */
    goToPage(num) {
      if (num < 0) { return; }
      this.addQuery({ page: toString(num) });
    },
    /**
     * @param  {string} value
     * @return {string}
     */
    globLike(value) {
      return (value ?? '')
      .replace(s_globLikeRe, (x) => get(s_globLikeMap, [ x ], ''));
    }
  }
});

export default component;
