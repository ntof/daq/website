// @ts-check

import { clone, forEach, get, isEmpty, isNumber } from 'lodash';
import Vue from 'vue';

const component = Vue.extend({
  computed: {
    /** @return {boolean} */
    isAdmin() { return get(this.$route, [ 'meta', 'isAdmin' ], false); }
  },
  methods: {
    /**
     * @this {V.Instance<component>}
     * @brief add parameters to the route
     * @param {{ [key:string]: string }} values
     * @return {boolean} true if route was modified
     */
    addQuery(values) {
      let mod = false;
      const query = clone(this.getQuery());
      forEach(values, (value, name) => {
        if (isEmpty(value) && !isNumber(value)) {
          if (name in query) {
            mod = true;
            delete query[name];
          }
        }
        else if (query[name] !== value) {
          mod = true;
          query[name] = value;
        }
      });
      if (mod) {
        this.$router.push({ query });
        return true;
      }
      return false;
    },
    /**
     * @this {V.Instance<component>}
     */
    getQuery() {
      return this.$route.query || {};
    },
    /**
     * @this {V.Instance<component>}
     * @param {string} path
     * @brief get path adding admin prefix when needed
     */
    getRoute(path) {
      if (this.isAdmin) {
        return '/admin' + path;
      }
      return path;
    }
  }
});

export default component;
