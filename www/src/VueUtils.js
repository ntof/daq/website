// @ts-check
import { size, startCase, toString } from 'lodash';
import './public-path';
import { currentUrl } from './utilities';
import { butils } from '@cern/base-vue';

const utils = {
  /**
   * @param  {string} url
   * @return {string}
   */
  publicPath: function(url) {
    /* eslint-disable-next-line camelcase */ /* @ts-ignore */
    if (typeof __webpack_public_path__ !== 'undefined') {
      /* eslint-disable-next-line camelcase */ /* @ts-ignore */
      return __webpack_public_path__ + url;
    }
    return url;
  },

  currentUrl: currentUrl
};

/**
 * @param  {any[]} value
 * @param  {number} max
 * @return {string}
 */
function sizeLimit(value, max) {
  const sz = size(value);
  return (sz >= max) ? ('>' + max) : toString(sz);
}

/**
 * @brief display date in Europe/Zurich timezone
 * @param {number} timestamp
 * @param {string} [format] format to use
 * @return {string}
 */
function dateTime(timestamp, format) {
  if (!Number.isFinite(timestamp)) {
    return '';
  }
  return butils.getDateTime(timestamp, { zone: 'Europe/Zurich' })
  .toFormat(format ?? 'yyyy-LL-dd HH:mm:ss');
}

export default {
  /** @param {InstanceType<Vue>} Vue */
  install(Vue) {
    Vue.helpers = utils;
    Vue.prototype.$utils = utils;
    Vue.filter('sizeLimit', sizeLimit);
    Vue.filter('startCase', startCase);
    Vue.filter('dateTime', dateTime);
  }
};
