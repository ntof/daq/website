// @ts-check
import Vue from 'vue';
import Router from 'vue-router';
import { get } from 'lodash';

import Home from './components/Home.vue';
import Runs from './components/Runs/Runs.vue';
import RunInfo from './components/Runs/RunInfo.vue';
import Materials from './components/Materials/Materials.vue';
import MaterialInfo from './components/Materials/MaterialInfo.vue';
import MaterialsSetupInfo from './components/MaterialsSetups/MaterialsSetupInfo.vue';
import MaterialsSetups from './components/MaterialsSetups/MaterialsSetups.vue';
import Detectors from './components/Detectors/Detectors.vue';
import DetectorInfo from './components/Detectors/DetectorInfo.vue';
import DetectorsSetupInfo from './components/DetectorsSetups/DetectorsSetupInfo.vue';
import DetectorsSetups from './components/DetectorsSetups/DetectorsSetups.vue';
import Containers from './components/Containers/Containers.vue';
import ContainerInfo from './components/Containers/ContainerInfo.vue';
import Shifts from './components/Shifts/Shifts.vue';
import ShiftsStatistics from './components/Shifts/ShiftsStatistics.vue';
import Admin from './components/Admin/Admin.vue';
import Comments from './components/Comments/Comments.vue';
import OperationPeriods from './components/OperationPeriods/OperationPeriods.vue';
import OperationPeriod from './components/OperationPeriods/OperationPeriod.vue';
import Users from './components/Users/Users.vue';
import UserInfo from './components/Users/UserInfo.vue';
import Institutes from './components/Institutes/Institutes.vue';
import InstituteInfo from './components/Institutes/InstituteInfo.vue';
import { sources, default as store } from './store';

// FIXME
// const Todo = Vue.extend({ template: '<div>TODO</div>' });

Vue.use(Router);

/**
 * @param  {V.Instance<any>}  v
 * @return {boolean}
 */
function isAdmin(v) {
  return get(v, [ '$store', 'getters', 'canAdmin' ]);
}

const router = new Router({
  routes: [
    { name: 'Home', path: '/', component: Home },
    // Materials
    { name: 'Materials', path: '/materials', component: Materials },
    { name: 'MaterialsSetups', path: '/materials/setups', component: MaterialsSetups,
      meta: { navbar: false } },
    { name: 'MaterialInfo', path: '/materials/:id', component: MaterialInfo,
      meta: { navbar: false } },
    { name: 'MaterialsSetupInfo', path: '/materials/setups/:id', component: MaterialsSetupInfo,
      meta: { navbar: false } },
    // Detectors
    { name: 'Detectors', path: '/detectors', component: Detectors,
      meta: { navbar: false } },
    { name: 'DetectorsSetups', path: '/detectors/setups', component: DetectorsSetups,
      meta: { navbar: false } },
    { name: 'DetectorInfo', path: '/detectors/:id', component: DetectorInfo,
      meta: { navbar: false } },
    { name: 'DetectorsSetupInfo', path: '/detectors/setups/:id', component: DetectorsSetupInfo,
      meta: { navbar: false } },
    // Containers
    { name: 'Containers', path: '/containers', component: Containers,
      meta: { navbar: false } },
    { name: 'ContainerInfo', path: '/containers/:id', component: ContainerInfo,
      meta: { navbar: false } },

    { name: 'EAR1', path: '/EAR1', component: Runs, props: { area: 'EAR1' } },
    { name: 'EAR2', path: '/EAR2', component: Runs, props: { area: 'EAR2' }  },
    { name: 'EAR3', path: '/EAR3', component: Runs, props: { area: 'EAR3' }  },
    { name: 'EAR1RunInfo', path: '/EAR1/:id', component: RunInfo,
      meta: { navbar: false }  },
    { name: 'EAR2RunInfo', path: '/EAR2/:id', component: RunInfo,
      meta: { navbar: false }  },
    { name: 'EAR3RunInfo', path: '/EAR3/:id', component: RunInfo,
      meta: { navbar: false }  },
    { name: 'RunInfo', path: '/runs/:id', component: RunInfo,
      meta: { navbar: false }  },

    { name: 'LogBook', path: '/logbook', component: Comments },
    // { name: 'Statistics', path: '/stats', component: Todo },
    // { name: 'Slowinfo', path: '/slowinfo', component: Todo },
    // { name: 'Documentation', path: '/doc', component: Todo },
    { name: 'Users', path: '/users', component: Users },
    { name: 'UserInfo', path: '/users/:id', component: UserInfo,
      meta: { navbar: false } },
    { name: 'Institutes', path: '/institutes', component: Institutes,
      meta: { navbar: false } },
    { name: 'InstituteInfo', path: '/institutes/:id', component: InstituteInfo,
      meta: { navbar: false } },
    { name: 'Shifts', path: '/shifts', component: Shifts },
    { name: 'ShiftsStatistics', path: '/shifts/stats', component: ShiftsStatistics,
      meta: { navbar: false } },

    { name: 'me', path: '/me',
      beforeEnter: (to, from, next) => {
        sources.users.get({
          query: { login: store.getters.username }, max: 1 })
        .then(
          (ret) => next(`/users/${ret[0].id}`),
          () => next('/'));
      }, meta: { navbar: false }
    },

    { name: 'Admin', path: '/admin', component: Admin,
      meta: { navbar: isAdmin, isAdmin: true } },
    // Materials
    { name: 'AdminMaterials', path: '/admin/materials', component: Materials,
      meta: { navbar: false, isAdmin: true } },
    { name: 'AdminMaterialsSetups', path: '/admin/materials/setups', component: MaterialsSetups,
      meta: { navbar: false, isAdmin: true } },
    { name: 'AdminMaterialInfo', path: '/admin/materials/:id', component: MaterialInfo,
      meta: { navbar: false, isAdmin: true } },
    { name: 'AdminMaterialsSetupInfo', path: '/admin/materials/setups/:id', component: MaterialsSetupInfo,
      meta: { navbar: false, isAdmin: true } },
    // Detectors
    { name: 'AdminDetectors', path: '/admin/detectors', component: Detectors,
      meta: { navbar: false, isAdmin: true } },
    { name: 'AdminDetectorsSetups', path: '/admin/detectors/setups', component: DetectorsSetups,
      meta: { navbar: false, isAdmin: true } },
    { name: 'AdminDetectorInfo', path: '/admin/detectors/:id', component: DetectorInfo,
      meta: { navbar: false, isAdmin: true } },
    { name: 'AdminDetectorsSetupInfo', path: '/admin/detectors/setups/:id', component: DetectorsSetupInfo,
      meta: { navbar: false, isAdmin: true } },
    // Containers
    { name: 'AdminContainers', path: '/admin/containers', component: Containers,
      meta: { navbar: false, isAdmin: true } },
    { name: 'AdminContainerInfo', path: '/admin/containers/:id', component: ContainerInfo,
      meta: { navbar: false, isAdmin: true } },

    { name: 'AdminRuns', path: '/admin/runs', component: Runs,
      meta: { navbar: false, isAdmin: true } },
    { name: 'AdminRunInfo', path: '/admin/runs/:id', component: RunInfo,
      meta: { navbar: false, isAdmin: true } },
    { name: 'AdminLogBook', path: '/admin/logbook', component: Comments,
      meta: { navbar: false, isAdmin: true } },
    { name: 'AdminOperatioPeriods', path: '/admin/operationPeriods', component: OperationPeriods,
      meta: { navbar: false, isAdmin: true } },
    { name: 'AdminOperatioPeriodInfo', path: '/admin/operationPeriods/:id', component: OperationPeriod,
      meta: { navbar: false, isAdmin: true } },
    { name: 'AdminShifts', path: '/admin/shifts', component: Shifts,
      meta: { navbar: false, isAdmin: true } },
    { name: 'AdminUsers', path: '/admin/users', component: Users,
      meta: { navbar: false, isAdmin: true } },
    { name: 'AdminUserInfo', path: '/admin/users/:id', component: UserInfo,
      meta: { navbar: false, isAdmin: true } },
    { name: 'AdminInstitutes', path: '/admin/institutes', component: Institutes,
      meta: { navbar: false, isAdmin: true } },
    { name: 'AdminInstituteInfo', path: '/admin/institutes/:id', component: InstituteInfo,
      meta: { navbar: false, isAdmin: true } },

    { path: '/index.html', redirect: '/', meta: { navbar: false } },
    { path: '/Layout', redirect: '/', meta: { navbar: false } },
    { path: '/overview', redirect: '/', meta: { navbar: false } }
  ]
});

export default router;
