// @ts-check
import { get, includes, merge, some } from 'lodash';
import Vuex from 'vuex';
import Vue from 'vue';

import {
  sources as baseSources,
  createStore,
  storeOptions } from '@cern/base-vue';

import nOperationWeeks from './modules/nOperationWeeks';
import dbQuery from './modules/dbQuery';

import DBQuerySource from './sources/DBQuerySource';
import DBCacheSource from './sources/DBCacheSource';
import DBRunsSource from './sources/DBRunsSource';

Vue.use(Vuex);

merge(storeOptions, /** @type {V.StoreOptions<AppStore.State>} */ ({
  state: {
    user: null
  },
  getters: {
    canBookShift: (state) => {
      const userRoles = get(state, [ 'user', 'cern_roles' ]);
      return some([ 'user', 'editor', 'admin' ], (r) => includes(userRoles, r));
    }
  },
  mutations: {
    user(state, user) {
      state.user = user;
    }
  },
  modules: {
    nOperationWeeks,
    runs: dbQuery('runNumber'),
    triggers: dbQuery('id'),
    comments: dbQuery('id'),
    materials: dbQuery('id'),
    detectors: dbQuery('id'),
    containers: dbQuery('id'),
    documents: dbQuery('id'),
    materialsSetups: dbQuery('id'),
    materialsSetupInfo: dbQuery(),
    detectorsSetups: dbQuery('id'),
    detectorsSetupInfo: dbQuery(),
    users: dbQuery('id'),
    configurations: dbQuery('id'),
    daqInstruments: dbQuery('id'),
    hvParameters: dbQuery('id'),
    operationPeriods: dbQuery('year'),
    pointsShifts: dbQuery('id'),
    duePoints: dbQuery('id'),
    shifts: dbQuery('id'),
    associations: dbQuery('id'),
    institutes: dbQuery('id'),
    extraData: dbQuery('key'),

    associationsCache: dbQuery('id'),
    daqParametersCache: dbQuery('id'),
    usersCache: dbQuery('id'),
    institutesCache: dbQuery('id')
  }
}));

const store = /** @type {V.Store<AppStore.State>} */ (createStore());
export default store;

export const sources = merge(baseSources, {
  runs: new DBRunsSource(store, [ 'runs' ], '/db/runs',
    { related: {
      materialsSetups: '/db/materialsSetups/item/{id}/related/runs',
      detectorsSetups: '/db/detectorsSetups/item/{id}/related/runs' } }),
  triggers: new DBQuerySource(store, [ 'triggers' ], '/db/triggers',
    { related: {
      runs: '/db/runs/item/{runNumber}/related/triggers' }
    }),
  comments: new DBQuerySource(store, [ 'comments' ], '/db/comments',
    { related: {
      runs: '/db/runs/item/{runNumber}/related/comments' }
    }),
  materials: new DBQuerySource(store, [ 'materials' ], '/db/materials',
    { related: {
      materialsSetups: '/db/materialsSetups/item/{id}/related/materials' } }),
  detectors: new DBQuerySource(store, [ 'detectors' ], '/db/detectors',
    { related: {
      detectorsSetups: '/db/detectorsSetups/item/{id}/related/detectors' } }),
  containers: new DBQuerySource(store, [ 'containers' ], '/db/containers',
    { related: {
      detectorsSetups: '/db/detectorsSetups/item/{id}/related/containers' } }),
  documents: new DBQuerySource(store, [ 'documents' ], '/db/documents',
    { related: {
      materials: '/db/materials/item/{id}/related/documents',
      detectors: '/db/detectors/item/{id}/related/documents',
      detectorsSetups: '/db/detectorsSetups/item/{id}/related/documents',
      runs: '/db/runs/item/{runNumber}/related/documents' }
    }),
  materialsSetups: new DBQuerySource(store, [ 'materialsSetups' ], '/db/materialsSetups',
    { related: {
      materials: '/db/materials/item/{id}/related/materialsSetups' } }),
  detectorsSetups: new DBQuerySource(store, [ 'detectorsSetups' ], '/db/detectorsSetups',
    { related: {
      detectors: '/db/detectors/item/{id}/related/detectorsSetups',
      containers: '/db/containers/item/{id}/related/detectorsSetups' } }),
  materialsSetupInfo: new DBQuerySource(store, [ 'materialsSetupInfo' ], '/db/materialsSetupInfo'),
  detectorsSetupInfo: new DBQuerySource(store, [ 'detectorsSetupInfo' ], '/db/detectorsSetupInfo'),
  users: new DBQuerySource(store, [ 'users' ], '/db/users'),
  configurations: new DBQuerySource(store, [ 'configurations' ], '/db/configurations',
    { related: {
      runs: '/db/runs/item/{runNumber}/related/configurations' }
    }),
  daqInstruments: new DBQuerySource(store, [ 'daqInstruments' ], '/db/daqInstruments'),
  relConfParams: new DBQuerySource(store, null, '/db/relConfParams'),
  hvParameters: new DBQuerySource(store, [ 'hvParameters' ], '/db/hvParameters',
    { related: {
      runs: '/db/runs/item/{runNumber}/related/hvParameters' }
    }),
  operationPeriods: new DBQuerySource(store, [ 'operationPeriods' ], '/db/operationPeriods'),
  pointsShifts: new DBQuerySource(store, [ 'pointsShifts' ], '/db/pointsShifts',
    { related: {
      operationPeriods: '/db/operationPeriods/item/{operYear}/related/pointsShifts' }
    }),
  duePoints: new DBQuerySource(store, [ 'duePoints' ], '/db/duePoints',
    { related: {
      operationPeriods: '/db/operationPeriods/item/{operYear}/related/duePoints',
      institutes: '/db/institutes/item/{id}/related/duePoints' }
    }),
  shifts: new DBQuerySource(store, [ 'shifts' ], '/db/shifts'),
  institutes: new DBQuerySource(store, [ 'institutes' ], '/db/institutes'),
  associations: new DBQuerySource(store, [ 'associations' ], '/db/associations',
    { related: {
      shifts: '/db/shifts/item/{assoId}/related/associations',
      institutes: '/db/institutes/item/{id}/related/associations',
      users: '/db/users/item/{id}/related/associations' }
    }),
  extraData: new DBQuerySource(store, [ 'extraData' ], '/db/extraData'),

  associationsCache: new DBCacheSource(store, 'id', [ 'associationsCache' ], '/db/associations'),
  daqParametersCache: new DBCacheSource(store, 'id', [ 'daqParametersCache' ], '/db/daqParameters'),
  usersCache: new DBCacheSource(store, 'id', [ 'usersCache' ], '/db/users'),
  institutesCache: new DBCacheSource(store, 'id', [ 'institutesCache' ], '/db/institutes')
});

/**
 * @param {string} name
 * @param {any} defaultValue
 * @returns {function(any): any}
 */
export function getUiState(name, defaultValue = null) {
  return /** @this {Vue} */ function(state) {
    if (state[name] === undefined) {
      Vue.set(state, name, defaultValue);
      this.$store.commit('ui/update', { [name]: defaultValue });
    }
    return state[name];
  };
}
