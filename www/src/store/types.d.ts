
export = AppStore
export as namespace AppStore

declare namespace AppStore {
  interface State {
    user: { username: string, [key: string]: any } | null
  }
  interface OperYearInfoState {
    [year: number]: number
  }

  interface DBQueryState {
    query: { [key: string]: any }|null,
    // raw result, respecting requested order
    rows: any[]|null,
    loading: boolean,
    // id keyed result
    db: { [key: string]: any }
  }

  interface User {
    id: string,
    login: string,
    firstName: string,
    lastName: string,
    email: string,
    status: boolean,
    privileges: string[]
  }

  interface Comment {
    id: number,
    date: number,
    comments: string,
    runNumber: number,
    userId: number
  }

  interface OperationPeriods {
    year: number,
    start: number,
    stop: number,
  }

  interface Institute {
    id: number,
    name: string,
    city: string,
    country: string,
    alias: string,
    status: number
  }

  interface PointsShifts {
    id: number,
    day: number,
    operYear: number,
    shiftKind: string,
    value: number
  }

  interface DuePoints {
    id: number,
    operYear: number,
    institute: id,
    nUsers: number,
    shiftPoints: number,
    shiftLeaderPoints: number
  }
}
