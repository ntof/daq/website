// @ts-check
import { get, isArray, isEqual, isNil, keyBy } from 'lodash';
import d from 'debug';
import Vue from 'vue';

const debug = d('store:db');

/**
 * @param  {string?=} key
 */
function dbQuery(key) {
  /** @type {V.Module<AppStore.DBQueryState>} */
  const m = {
    namespaced: true,
    state: () => ({
      query: null,
      rows: null,
      loading: false,
      db: {}
    }),
    mutations: {
      /**
       * @param  {AppStore.DBQueryState} state
       * @param  {any} query
       */
      start(state, query) {
        if (!isEqual(query, state.query)) {
          // @ts-ignore it is defined
          m.mutations.restart(state, query);
        }
        else {
          debug('not updating query:', query);
        }
      },
      /**
       * @param  {AppStore.DBQueryState} state
       * @param  {any} query
       */
      restart(state, query) {
        debug('updating query:', query);
        state.query = query;
        state.loading = (isNil(query)) ? false : true;
        state.rows = null;
      },
      /**
       * @param  {AppStore.DBQueryState} state
       * @param  {any[]} rows
       */
      end(state, rows) {
        state.rows = rows;
        state.loading = false;
        if (key) {
          state.db = keyBy(rows, key);
        }
      },
      /**
       * @param  {AppStore.DBQueryState} state
       * @param  {any[]} items
       */
      update(state, items) {
        if (isNil(items) || isNil(key)) {
          return;
        }
        else if (isArray(items)) {
          items.forEach((u) => Vue.set(state.db, u[key], u));
        }
        else {
          Vue.set(state.db, get(items, key), items);
        }
      }
    }
  };
  return m;
}
export default dbQuery;
