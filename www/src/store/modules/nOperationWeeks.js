// @ts-check
import Vue from 'vue';
import { assign } from 'lodash';

/** @type {V.Module<AppStore.OperYearInfoState>} */
const m = {
  namespaced: true,
  state: () => ({}),
  mutations: {
    /**
     * @param  {AppStore.OperYearInfoState} state
     * @param  {{ year: number, value: number}} data
     */
    setWeeks(state, data) {
      Vue.set(state, data.year, data.value);
    },
    update: assign
  }
};
export default m;
