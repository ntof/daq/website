// @ts-check
import axios from 'axios';
import { get, isObject, startsWith, transform } from 'lodash';
import { currentUrl, format } from '../../utilities';
import { BaseLogger as logger } from '@cern/base-vue';
import d from 'debug';
import DBQuerySource from './DBQuerySource';

const debug = d('source:db');

export default class DBRunsSource extends DBQuerySource {
  /**
   * @param {V.Store<any>} store
   * @param {string[]?} storePath
   * @param {string} endpoint
   * @param {{ related: { [name: string]: string } }} [options]
   */
   constructor(store, storePath, endpoint, options) {
    super(store, storePath, endpoint, options)
  }

  /**
   * @param  {string|number} id
   * @return {Promise<any>}
   */
   async updateTriggers(id) {
    const endpoint = '/admin' + this.endpoint;
    debug('put %s/item/%s/updateTriggers', endpoint, id);
    await axios.put(currentUrl() + `${endpoint}/item/${id}/updateTriggers`, {})
    .catch((err) => { logger.error(err); throw err; });
  }

}
