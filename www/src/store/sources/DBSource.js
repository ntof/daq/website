// @ts-check
import axios from 'axios';
import { get, isObject, startsWith, transform } from 'lodash';
import { currentUrl, format } from '../../utilities';
import { BaseLogger as logger } from '@cern/base-vue';
import d from 'debug';

const debug = d('source:db');

export default class DBSource {
  /**
   * @param {string} endpoint
   * @param {{ related: { [name: string]: string } }} [options]
   */
  constructor(endpoint, options) {
    this.endpoint = endpoint;
    /** @type {{ [name: string]: string }} */
    this.related = get(options, [ 'related' ], {});
  }

  /**
   * @param  {{ [key:string]: any}} params
   * @return {Promise<any[]>}
   */
  async get(params) {
    /** @type {string} */
    const endpoint = get(params, '_ep', this.endpoint);

    /* remove parameters starting with '_', stringify objects */
    params = transform(params, (ret, value, key) => {
      if (!startsWith(key, '_')) {
        ret[key] = isObject(value) ? JSON.stringify(value) : value;
      }
    }, /** @type {any} */ ({}));

    debug('get new values on %s', endpoint);
    try {
      const ret = await axios.get(currentUrl() + endpoint, { params });
      return ret.data;
    }
    catch (err) {
      logger.error(err);
      throw err;
    }
  }

  /**
   * @param  {string|number} id
   * @return {Promise<any>}
   */
  async getById(id) {
    if (!id) { return; }
    debug('get %s/item/%s', this.endpoint, id);
    try {
      const ret = await axios.get(currentUrl() + `${this.endpoint}/item/${id}`);
      return ret.data;
    }
    catch (err) {
      logger.error(err);
      throw err;
    }
  }

  /**
   * @param  {string|number} id
   * @param  {any}  params parameters to push
   * @param  {boolean} isAdmin
   * @return {Promise<any>}
   */
  async put(id, params, isAdmin = true) {
    if (!params) { return; }
    const endpoint = (isAdmin ? '/admin' : '') + this.endpoint;
    debug('put %s/item/%s %o', endpoint, id, params);
    await axios.put(currentUrl() + `${endpoint}/item/${id}`, params)
    .catch((err) => { logger.error(err); throw err; });
  }

  /**
   * @param {string|number} id
   * @param {string} stream
   * @param {File}  file file to stream
   * @return {Promise<any>}
   */
  async putData(id, stream, file) {
    if (!file) { return; }
    debug('put %s/item/%s/stream/%s file:%s', this.endpoint, id,
      stream, get(file, 'name'));
    await axios.put(currentUrl() + `/admin${this.endpoint}/item/${id}/streams/${stream}`, file,
      { headers: { 'content-type': 'application/octet-stream' } })
    .catch((err) => { logger.error(err); throw err; });
  }

  /**
   * @param  {any}  params parameters to post
   * @param  {boolean} isAdmin
   * @return {Promise<number|string>}
   */
  async post(params, isAdmin = true) {
    const endpoint = (isAdmin ? '/admin' : '') + this.endpoint;
    debug('post %s %o', endpoint, params);
    return await axios.post(currentUrl() + `${endpoint}`, params)
    .then(
      (ret) => ret.data,
      (err) => { logger.error(err); throw err; });
  }

  /**
   * @param  {string|number} id
   * @param  {boolean} isAdmin
   */
  async delete(id, isAdmin = true) {
    const endpoint = (isAdmin ? '/admin' : '') + this.endpoint;
    debug('delete %s/item/%s', endpoint, id);
    await axios.delete(currentUrl() + `${endpoint}/item/${id}`)
    .catch((err) => { logger.error(err); throw err; });
  }

  /**
   * @param  {string}  name relation name
   * @param  {any}     item related item
   * @param  {{ [key:string]: any}} params
   * @return {Promise<any[]>}
   */
  async getRelations(name, item, params) {
    const tpl = get(this.related, name);
    if (!tpl) {
      debug('invalid relation request: %s', name);
      return [];
    }
    debug('get relation:%s %s %s', name, this.endpoint, params);
    try {
      const ret = await axios.get(currentUrl() + format(tpl, item), { params });
      return ret.data;
    }
    catch (err) {
      logger.error(err);
      throw err;
    }
  }


  /**
   * @param  {string}  name relation name
   * @param  {any}     item related item
   * @param  {string|number}  id relation to remove
   * @return {Promise<void>}
   */
  async postRelation(name, item, id) {
    const tpl = get(this.related, name);
    if (!tpl) {
      debug('invalid relation request: %s', name);
      return;
    }
    debug('post relation:%s %s %s', name, this.endpoint, id);
    await axios.post(currentUrl() + '/admin' + format(tpl, item), id,
      { headers: { 'content-type': 'application/json' } })
    .catch((err) => { logger.error(err); throw err; });
  }

  /**
   * @param  {string}  name relation name
   * @param  {any}     item related item
   * @param  {string|number}  id relation to remove
   * @return {Promise<void>}
   */
  async deleteRelation(name, item, id) {
    const tpl = get(this.related, name);
    if (!tpl) {
      debug('invalid relation request: %s', name);
      return;
    }
    debug('delete relation:%s %s %s', name, this.endpoint, id);
    await axios.delete(currentUrl() + '/admin' + format(tpl, item),
      { data: id, headers: { 'content-type': 'application/json' } })
    .catch((err) => { logger.error(err); throw err; });
  }
}
