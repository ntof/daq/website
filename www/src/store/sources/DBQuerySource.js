// @ts-check
import { get, isEqual, isNil, noop } from 'lodash';
import { format } from '../../utilities';
import DBSource from './DBSource';
import d from 'debug';

const debug = d('source:db');

export default class DBQuerySource extends DBSource {
  /**
   * @param {V.Store<any>} store
   * @param {string[]?} storePath
   * @param {string} endpoint
   * @param {{ related: { [name: string]: string } }} [options]
   */
  constructor(store, storePath, endpoint, options) {
    super(endpoint, options);
    this.store = store;
    this.req = 0;

    if (storePath) {
      this.storePath = storePath;
      this.commitPath = storePath.join('/');
      this.storePath.push('query');
      this.unwatch = store.watch((state) => get(state, storePath),
        this.onQueryChanged.bind(this));
    }
  }

  destroy() {
    if (this.unwatch) {
      this.unwatch();
    }
  }

  /**
   * @param  {any} params
   * @param  {boolean} [nocache=false]
   */
  fetch(params, nocache = false) {
    if (!this.storePath) {
      return;
    }
    else if (nocache && isEqual(params, get(this.store.state, this.storePath))) {
      this.store.commit(this.commitPath + '/start', null);
    }
    this.store.commit(this.commitPath + '/start', params);
  }

  refresh() {
    if (!this.storePath) {
      return;
    }
    this.fetch(get(this.store.state, this.storePath), true);
  }

  clear() {
    if (!this.storePath) {
      return;
    }
    this.store.commit(this.commitPath + '/start', null);
  }

  /**
   * @param  {string} name relation name
   * @param  {any} item related item
   * @param  {any} params query parameters
   * @param  {boolean} [nocache=false]
   */
  fetchRelated(name, item, params, nocache = false) {
    const tpl = get(this.related, name);
    if (!tpl) {
      debug('invalid relation request: %s', name);
      return;
    }
    params._ep = format(tpl, item);
    return this.fetch(params, nocache);
  }

  onQueryChanged() {
    if (!this.storePath) {
      return;
    }
    const req = ++this.req;
    const query = get(this.store.state, this.storePath);
    if (isNil(query)) { return; }

    debug('fetching new values on %s', this.endpoint);
    return this.get(query)
    .then(
      (data) => {
        if (req !== this.req) { return; } /* not latest request */
        this.store.commit(this.commitPath + '/end', data);
      },
      () => noop);
  }
}
