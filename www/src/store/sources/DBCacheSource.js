// @ts-check
import { concat, get, isEmpty, isNil, set, transform } from 'lodash';
import DBSource from './DBSource';
import d from 'debug';

const debug = d('source:db');

/**
 * @brief grew a cache of objects
 * @details does not use a global query, retrieve items either from the store
 * or from database use ids
 */
export default class DBCacheSource extends DBSource {
  /**
   * @param {V.Store<any>} store
   * @param {string} key
   * @param {string[]} storePath
   * @param {string} endpoint
   * @param {{ related: { [name: string]: string } }} [options]
   */
  constructor(store, key, storePath, endpoint, options) {
    const dbPath = concat(storePath, 'db');
    super(endpoint, options);
    this.store = store;
    this.key = key;

    this.commitPath = storePath.join('/');
    this.dbPath = dbPath;
  }

  /**
   * @brief retrieve all items
   * @param {Number} [max]
   * @return {Promise<{ [key:string]: any }>}
   */
  async fetchAll(max) {
    debug('filling cache on %s', this.endpoint);
    /** @type {{ [key:string]: any }} */
    var ret = {};
    const fetchedItems = await this.get(max ? { max } : {});
    transform(fetchedItems, (ret, item) => {
      ret[item[this.key]] = item;
    }, ret);
    this.store.commit(this.commitPath + '/update', fetchedItems);
    return ret;
  }

  /**
   * @brief retrieve more items
   * @details does not change the query
   * @param  {(number|string)[]}  itemIds
   * @param  {boolean} [nocache=false]
   * @return {Promise<{ [key:string]: any }>}
   */
  async fetchMore(itemIds, nocache = false) {
    /** @type {{ [key:string]: any }} */
    var ret = {};
    if (!nocache) {
      const db = get(this.store.state, this.dbPath);
      itemIds = transform(itemIds, (missing, id) => {
        const item = get(db, id);
        if (!isNil(item)) {
          ret[id] = item;
        }
        else {
          missing.push(id);
        }
      }, /** @type {(number|string)[]} */ ([]));
      if (isEmpty(itemIds)) { return ret; }
    }
    debug('caching new values on %s', this.endpoint);
    const fetchedItems = await this.get(
      set({ max: itemIds.length }, [ 'query', this.key, '$in' ], itemIds));
    transform(fetchedItems, (ret, item) => {
      ret[item[this.key]] = item;
    }, ret);
    this.store.commit(this.commitPath + '/update', fetchedItems);
    return ret;
  }
}
