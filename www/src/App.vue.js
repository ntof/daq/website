// @ts-check

import { VERSION } from './Consts';
import Vue from 'vue';

const component = Vue.extend({
  name: 'App',
  data() { return { VERSION }; }
});

export default component;
