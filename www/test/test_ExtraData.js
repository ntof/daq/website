// @ts-check

import './karma_index';
import {
  createLocalVue, createRouter,
  waitForValue } from './utils';
import * as utilities from "../src/utilities";

import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import Home from '../src/components/Home.vue';
import ExtraDataWidget from '../src/components/ExtraDataWidget.vue';

// import sources, code may be elided otherwise
/* eslint-disable-next-line @typescript-eslint/no-unused-vars */
import { sources, default as store } from '../src/store';  // jshint ignore:line
import { clone, get, set } from 'lodash';
import { expect } from 'chai';

/*::
declare var serverRequire: (string) => any
*/

describe('ExtraData', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */ ({});
      return utils.createApp(this.env)
      .then(() => this.env.server.db.createStub())
      .then(() => {
        Object.defineProperty(this.env.server.app.request, 'user', {
          configurable: true,
          enumerable: true,
          get() { return { sub: 'ntofdev' }; }
        });
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
    });
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can modify an extra data field', async function() {
    store.commit('user', {
      sub: 'ntofdev', cern_roles: [ 'user', 'editor', 'admin' ] }); // eslint-disable-line camelcase
    wrapper = mount(Home, { router: createRouter('/'), localVue: createLocalVue() });

    const extraDataWidget = wrapper.findComponent({ name: 'ExtraDataWidget' });

    await waitForValue(() => get(extraDataWidget.vm, 'loading'), false);
    await waitForValue(() => get(extraDataWidget.vm, 'valueInDb'), true);
    const originalValue = clone(get(extraDataWidget.vm, 'dbValue'));

    // Modify the content of the widget
    const markdownViewer = extraDataWidget.findComponent({ name: 'BaseMarkdownWidget' });
    set(markdownViewer.vm, 'editValue', originalValue + "!");

    // Press Save button
    const saveButton = extraDataWidget.find('.x-extra-save');
    saveButton.trigger('click');

    await waitForValue(() => get(extraDataWidget.vm, 'saving'), false);
    await waitForValue(() => get(extraDataWidget.vm, 'loading'), false);

    const newValue = clone(get(extraDataWidget.vm, 'dbValue'));

    expect(newValue).to.be.equal(originalValue + "!");
  });

  it('can create an extra data field if missing in db', async function() {
    store.commit('user', {
      sub: 'ntofdev', cern_roles: [ 'user', 'editor', 'admin' ] }); // eslint-disable-line camelcase
    wrapper = mount(ExtraDataWidget, {
      propsData: { title: 'test', extraDataKey: 'testKey' }, localVue: createLocalVue() });

    // check there is no testKey on extraData
    let res = await sources.extraData.get({ max: 1,  query: { key: 'testKey' } });
    expect(res.length).to.be.equal(0);

    await waitForValue(() => get(wrapper.vm, 'loading'), false);
    await waitForValue(() => get(wrapper.vm, 'valueInDb'), false);

    await wrapper.setProps({ inEdit: true });

    // Modify the content of the widget
    const markdownViewer = wrapper.findComponent({ name: 'BaseMarkdownWidget' });
    set(markdownViewer.vm, 'editValue', "## test");

    // Press Save button
    const saveButton = wrapper.find('.x-extra-save');
    saveButton.trigger('click');

    await waitForValue(() => get(wrapper.vm, 'saving'), false);
    await waitForValue(() => get(wrapper.vm, 'loading'), false);
    await waitForValue(() => get(wrapper.vm, 'valueInDb'), true);

    // check there is now a testKey item on extraData
    res = await sources.extraData.get({ max: 1,  query: { key: 'testKey' } });
    expect(res.length).to.be.equal(1);
  });

});
