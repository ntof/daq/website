// @ts-check

import './karma_index';
import {
  createLocalVue, createRouter,
  waitFor,
  waitForValue, waitForWrapper } from './utils';
import * as utilities from "../src/utilities";

import { get, size, toNumber, noop } from 'lodash';
import { expect } from 'chai';
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import DetectorInfo from '../src/components/Detectors/DetectorInfo.vue';
import App from '../src/App.vue';
import appRouter from '../src/router';

// import sources, code may be elided otherwise
import { default as store } from '../src/store';
import { delay } from '@cern/prom';

/*::
declare var serverRequire: (string) => any
*/

describe('DetectorInfo', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => this.env.server.db.createStub())
      .then(() => {
        var addr = this.env.server.address();
        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
    });
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can display DetectorInfo', async function() {
    wrapper = mount(DetectorInfo, {
      router: createRouter('/detectors/1'), localVue: createLocalVue() });

    await waitForValue(
      () => get(wrapper, [ 'vm', 'detector', 'id' ]), 1);

    await waitForValue(() => wrapper.find('.x-desc').find('.b-markdown-viewer').text(),
      'Sample detector with some description');
  });

  it('can navigate to DetectorInfo', async function() {
    wrapper = mount(App, {
      router: createRouter('/'), localVue: createLocalVue() });
    wrapper.vm.$router.push('/detectors');

    const row = await waitForWrapper(() => wrapper.find('tbody tr'));
    row.trigger('click');

    const info = await waitForWrapper(
      () => wrapper.findComponent({ name: 'DetectorInfo' }));
    expect(toNumber(info.vm.$route.params.id)).to.be.gte(0);
  });

  it('can edit', async function() {
    // eslint-disable-next-line camelcase
    store.commit('user', { cern_roles: [ 'user', 'editor', 'admin' ] });

    wrapper = mount(DetectorInfo, {
      router: createRouter('/detectors/1'), localVue: createLocalVue({ auth: true }) });

    wrapper.vm.setEdit(true);

    (await waitForWrapper(
      () => wrapper.findComponent({ ref: 'name' })))
    .setData({ editValue: 'test' });

    (await waitForWrapper(() => wrapper.find('.x-submit')))
    .trigger('click');

    // out of edit mode
    await waitForValue(() => wrapper.find('.fa-cog').exists(), true);
    await waitForValue(
      () => get(wrapper, [ 'vm', 'detector', 'name' ]), 'test');
    // this must be visible only to editors/admin
    // eslint-disable-next-line camelcase
    store.commit('user', { cern_roles: [ 'user' ] });
    await waitForValue(() => wrapper.find('.fa-cog').exists(), false);
  });

  it('can load related info', async function() {
    wrapper = mount(DetectorInfo, {
      router: createRouter('/detectors/1'), localVue: createLocalVue() });

    await waitForValue(() => size(wrapper.vm.setups), 1);
    await waitForValue(() => size(wrapper.vm.docs), 1);
  });
});
