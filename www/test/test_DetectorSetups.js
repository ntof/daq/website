// @ts-check

import './karma_index';
import {
  createLocalVue, createRouter,
  waitFor, waitForValue, waitForWrapper } from './utils';
import * as utilities from "../src/utilities";

import { get, last, set, toNumber } from 'lodash';
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import { expect } from 'chai';

// import sources, code may be elided otherwise
/* eslint-disable-next-line @typescript-eslint/no-unused-vars */
import { sources, default as store } from '../src/store'; // jshint ignore:line

import DetectorsSetups from '../src/components/DetectorsSetups/DetectorsSetups.vue';
import App from '../src/App.vue';

/*::
declare var serverRequire: (string) => any
*/

describe('DetectorsSetups', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => this.env.server.db.createStub())
      .then(() => {
        var addr = this.env.server.address();
        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
    });
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can list DetectorsSetups', async function() {
    wrapper = mount(DetectorsSetups, {
      router: createRouter(), localVue: createLocalVue() });

    await waitFor(() => wrapper.findAll('tbody tr').length > 0);
  });

  it('can create DetectorsSetups', async function() {
    wrapper = mount(App, {
      router: createRouter('/admin/detectors/setups'), localVue: createLocalVue() });

    (await waitForWrapper(() => wrapper.find('.x-add')))
    .trigger('click');

    const dialog = await waitForWrapper(() => wrapper.find('.x-add-dialog'));
    set(dialog.findComponent({ ref: 'name' }), [ 'vm', 'editValue' ], 'new setup');
    set(dialog.findComponent({ ref: 'earNumber' }), [ 'vm', 'editValue' ], 1);

    dialog.findAll('.btn-primary').filter((b) => (b.text() === 'Ok'))
    .trigger('click');

    const info = (await waitForWrapper(() => wrapper.findComponent({ name: 'DetectorsSetupInfo' })));
    await waitForValue(() => get(info, [ 'vm', 'setup', 'name' ]),
      'new setup');
    const id = get(info, [ 'vm', 'setup', 'id' ]);

    // item is in DB
    expect(await sources.detectorsSetups.get({ query: { id } })).to.have.length(1);

    wrapper.find('.x-delete').trigger('click');
    // confirm
    (await waitForWrapper(() => wrapper.findAll('button.btn-danger').filter((b) => (b.text() === 'Ok'))))
    .trigger('click');

    // redirected to detectors
    await waitForWrapper(() => wrapper.find('.x-detectors-setups'));
    // item is not in DB
    expect(await sources.detectorsSetups.get({ query: { id } })).to.have.length(0);
    await waitFor(() => wrapper.findAll('tbody tr').length > 0);
  });

  it('can duplicate a DetectorsSetups', async function() {
    wrapper = mount(App, {
      router: createRouter('/admin/detectors/setups/22'), localVue: createLocalVue() });

    const info = (await waitForWrapper(() => wrapper.findComponent({ name: 'DetectorsSetupInfo' })));
    await waitForValue(() => get(info, [ 'vm', 'loading' ]), false);
    await waitForValue(() => get(info, [ 'vm', '$refs', 'detectors', 'loading' ]), false);

    /* modify it by removing the container */
    wrapper.findAll('button').filter((b) => b.text() === 'Remove Container').trigger('click');

    wrapper.find('.fa-copy').trigger('click');
    const titleEdit = await waitForWrapper(() => info.findComponent({ ref: 'titleDialogEdit' }));
    const year = new Date().getFullYear();
    await waitForValue(() => get(titleEdit.vm, 'editValue'),
      `${year} EAR2 D: test detector`, 'edit value not modified');
    (info.findAll('button').filter((b) => b.text() === 'Yes')).trigger('click');

    await waitForValue(() => get(info, [ 'vm', 'setup', 'name' ]),
      `${year} EAR2 D: test detector`, 'db value not modified');
    expect(toNumber(get(info, [ 'vm', 'id' ]))).to.be.gt(22);
  });

  it('can edit a DetectorsSetups', async function() {
    wrapper = mount(App, {
      router: createRouter('/admin/detectors/setups/22'), localVue: createLocalVue() });

    const info = (await waitForWrapper(() => wrapper.findComponent({ name: 'DetectorsSetupInfo' })));
    await waitForValue(() => get(info, [ 'vm', 'loading' ]), false);
    await waitForValue(() => get(info, [ 'vm', '$refs', 'detectors', 'loading' ]), false);

    /* modify it by removing the container */
    wrapper.findAll('button').filter((b) => b.text() === 'Remove Container').trigger('click');
    const dets = info.findComponent({ name: 'DetectorsSetupDetectors' });

    /* add a filter going through the search sequence */
    dets.find('.fa-plus-square').trigger('click');
    const addDialog = dets.find('.x-add-dialog');
    set(addDialog.findComponent({ ref: 'trecNumber' }), [ 'vm', 'editValue' ], '123456');
    addDialog.findAll('button').filter((b) => b.text() === 'Search').trigger('click');
    await waitForValue(() => addDialog.find('tbody td').text(), '2'); // Detector Id
    addDialog.find('tbody tr').trigger('click');
    await waitForValue(
      () => last(get(info, [ 'vm', '$refs', 'detectors', 'infoData' ])).detectorId, 2);

    /* modify some params */
    set(info.findComponent({ ref: 'operYear' }), [ 'vm', 'editValue' ], 2020);
    set(info.findComponent({ ref: 'description' }), [ 'vm', 'editValue' ], 'pwet pwet');

    wrapper.find('.fa-upload').trigger('click');
    const titleEdit = await waitForWrapper(() => info.findComponent({ ref: 'titleDialogEdit' }));
    await waitForValue(() => get(titleEdit.vm, 'editValue'),
      '2020 EAR2 D: test detector, D: test detector 2');
    (info.findAll('button').filter((b) => b.text() === 'Yes')).trigger('click');

    await waitForValue(() => get(info, [ 'vm', 'setup', 'name' ]),
      '2020 EAR2 D: test detector, D: test detector 2');

    // quick DB check
    const setup = await sources.detectorsSetups.get({ query: { id: 22 } });
    expect(setup[0]).to.have.property('description', 'pwet pwet');
    expect(setup[0]).to.have.property('operYear', 2020);
  });

});
