// @ts-check

import './karma_index';
import {
  createLocalVue, createRouter,
  waitFor, waitForValue, waitForWrapper } from './utils';
import * as utilities from "../src/utilities";

import { find, get, last, map, set, toNumber } from 'lodash';
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import { expect } from 'chai';

// import sources, code may be elided otherwise
/* eslint-disable-next-line @typescript-eslint/no-unused-vars */
import { sources, default as store } from '../src/store'; // jshint ignore:line

import MaterialsSetups from '../src/components/MaterialsSetups/MaterialsSetups.vue';
import App from '../src/App.vue';

/*::
declare var serverRequire: (string) => any
*/

describe('MaterialsSetups', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => this.env.server.db.createStub())
      .then(() => {
        var addr = this.env.server.address();
        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
    });
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can list MaterialsSetups', async function() {
    wrapper = mount(MaterialsSetups, {
      router: createRouter(), localVue: createLocalVue() });

    await waitFor(() => wrapper.findAll('tbody tr').length > 0);
  });

  it('can create MaterialsSetup', async function() {
    wrapper = mount(App, {
      router: createRouter('/admin/materials/setups'), localVue: createLocalVue() });

    (await waitForWrapper(() => wrapper.find('.x-add')))
    .trigger('click');

    const dialog = await waitForWrapper(() => wrapper.find('.x-add-dialog'));
    set(dialog.findComponent({ ref: 'name' }), [ 'vm', 'editValue' ], 'new setup');
    set(dialog.findComponent({ ref: 'earNumber' }), [ 'vm', 'editValue' ], 1);

    dialog.findAll('.btn-primary').filter((b) => (b.text() === 'Ok'))
    .trigger('click');

    const info = (await waitForWrapper(() => wrapper.findComponent({ name: 'MaterialsSetupInfo' })));
    await waitForValue(() => get(info, [ 'vm', 'setup', 'name' ]),
      'new setup');
    const id = get(info, [ 'vm', 'setup', 'id' ]);

    // item is in DB
    expect(await sources.materialsSetups.get({ query: { id } })).to.have.length(1);

    wrapper.find('.x-delete').trigger('click');
    // confirm
    (await waitForWrapper(() => wrapper.findAll('button.btn-danger').filter((b) => (b.text() === 'Ok'))))
    .trigger('click');

    // redirected to materials
    await waitForWrapper(() => wrapper.find('.x-materials-setups'));
    // item is not in DB
    expect(await sources.materialsSetups.get({ query: { id } })).to.have.length(0);
    await waitFor(() => wrapper.findAll('tbody tr').length > 0);
  });

  it('can duplicate a MaterialsSetup', async function() {
    wrapper = mount(App, {
      router: createRouter('/admin/materials/setups/460'), localVue: createLocalVue() });

    const info = (await waitForWrapper(() => wrapper.findComponent({ name: 'MaterialsSetupInfo' })));
    await waitForValue(() => get(info, [ 'vm', 'loading' ]), false);
    await waitForValue(() => get(info, [ 'vm', '$refs', 'materials', 'loading' ]), false);

    /* modify something */
    const filter = get(info, [ 'vm', '$refs', 'materials', 'filter', 0 ]);
    expect(filter).to.exist();
    filter.position = 8;
    filter.status = 0;

    wrapper.find('.fa-copy').trigger('click');
    const titleEdit = await waitForWrapper(() => info.findComponent({ ref: 'titleDialogEdit' }));
    await waitForValue(() => get(titleEdit.vm, 'editValue'),
      '2017 EAR2 Filters: W,Co,Bi Sources: Y');
    (info.findAll('button').filter((b) => b.text() === 'Yes')).trigger('click');

    await waitForValue(() => get(info, [ 'vm', 'setup', 'name' ]),
      '2017 EAR2 Filters: W,Co,Bi Sources: Y');
    expect(toNumber(get(info, [ 'vm', 'id' ]))).to.be.gt(460);
  });

  it('can edit a MaterialsSetup', async function() {
    wrapper = mount(App, {
      router: createRouter('/admin/materials/setups/460'), localVue: createLocalVue() });

    const info = (await waitForWrapper(() => wrapper.findComponent({ name: 'MaterialsSetupInfo' })));
    await waitForValue(() => get(info, [ 'vm', 'loading' ]), false);
    await waitForValue(() => get(info, [ 'vm', '$refs', 'materials', 'loading' ]), false);

    /* modify something */
    var filter = get(info, [ 'vm', '$refs', 'materials', 'filter', 0 ]);
    expect(filter).to.exist();
    filter.position = 8;
    filter.status = 0;
    const mats = info.findComponent({ name: 'MaterialsSetupMaterials' });
    mats.findAll('.fa-trash').at(1).trigger('click');

    /* add a filter going through the search sequence */
    mats.find('.fa-plus-square').trigger('click');
    const addDialog = mats.find('.x-add-dialog');
    set(addDialog.findComponent({ ref: 'element' }), [ 'vm', 'editValue' ], 'Pb');
    addDialog.findAll('button').filter((b) => b.text() === 'Search').trigger('click');
    await waitForValue(() => addDialog.find('tbody td').text(), '284');
    addDialog.find('tbody tr').trigger('click');
    await waitForValue(
      () => last(get(info, [ 'vm', '$refs', 'materials', 'filter' ])).id, 284);
    filter = last(get(info, [ 'vm', '$refs', 'materials', 'filter' ]));
    filter.position = 1;
    filter.status = 1;

    /* modify some params */
    set(info.findComponent({ ref: 'operYear' }), [ 'vm', 'editValue' ], 2020);
    set(info.findComponent({ ref: 'description' }), [ 'vm', 'editValue' ], 'pwet pwet');

    wrapper.find('.fa-upload').trigger('click');
    const titleEdit = await waitForWrapper(() => info.findComponent({ ref: 'titleDialogEdit' }));
    await waitForValue(() => get(titleEdit.vm, 'editValue'),
      '2020 EAR2 Filters: Pb,Co,Bi Sources: Y');
    (info.findAll('button').filter((b) => b.text() === 'Yes')).trigger('click');

    await waitForValue(() => get(info, [ 'vm', 'setup', 'name' ]),
      '2020 EAR2 Filters: Pb,Co,Bi Sources: Y');

    // quick DB check
    const setup = await sources.materialsSetups.get({ query: { id: 460 } });
    expect(setup[0]).to.have.property('description', 'pwet pwet');
    expect(setup[0]).to.have.property('operYear', 2020);
  });


  it('can edit samples', async function() {
    wrapper = mount(App, {
      router: createRouter('/admin/materials/setups/689'), localVue: createLocalVue() });

    const info = (await waitForWrapper(() => wrapper.findComponent({ name: 'MaterialsSetupInfo' })));
    await waitForValue(() => get(info, [ 'vm', 'loading' ]), false);
    await waitForValue(() => get(info, [ 'vm', '$refs', 'materials', 'loading' ]), false);
    const setupMaterials = wrapper.findComponent({ name: 'MaterialsSetupMaterials' });

    expect(get(setupMaterials.vm, [ 'sample', 0 ])).to.have.property('status', 1);
    expect(get(setupMaterials.vm, [ 'sample', 1 ])).to.have.property('status', 0);
    const materialIds = map(get(setupMaterials.vm, [ 'sample' ]), 'id');
    expect(materialIds).to.have.length(2);

    setupMaterials.find('.x-samples').findAll('.x-position').at(0).setValue(1);
    setupMaterials.find('.x-samples').findAll('.x-position').at(1).setValue(2);
    setupMaterials.find('.x-samples').findAll('.x-status') // @ts-ignore
    .at(1).vm.toggle();

    await waitForValue(() => get(setupMaterials.vm, [ 'sample', 1, 'status' ]), 1);
    expect(get(setupMaterials.vm, [ 'sample', 0 ])).to.have.property('status', 0);

    // submit and update name
    wrapper.find('.fa-upload').trigger('click');
    await waitForWrapper(() => info.findComponent({ ref: 'titleDialogEdit' }));
    (info.findAll('button').filter((b) => b.text() === 'Yes')).trigger('click');

    await waitForValue(() => get(setupMaterials.vm, 'inEdit'), false);
    await waitForValue(() => get(setupMaterials.vm, [ 'sample', 1, 'status' ]), 1);

    // quick DB check
    const setup = await sources.materialsSetupInfo.get(
      { query: { materialsSetupId: 689 } });
    let mat = find(setup, { materialId: materialIds[0] });
    expect(mat).to.have.property('status', 0);
    mat = find(setup, { materialId: materialIds[1] });
    expect(mat).to.have.property('status', 1);
  });
});
