// @ts-check

import './karma_index';
import {
  createLocalVue, createRouter,
  isVisible, waitFor, waitForValue } from './utils';
import * as utilities from "../src/utilities";

import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import Shifts from '../src/components/Shifts/Shifts.vue';

// import sources, code may be elided otherwise
/* eslint-disable-next-line @typescript-eslint/no-unused-vars */
import { sources, default as store } from '../src/store';  // jshint ignore:line
import { get } from 'lodash';

/*::
declare var serverRequire: (string) => any
*/

const createUserAssociation = async () => {
  const ntofdevId = await sources.users.get({ max: 1, query: { login: 'ntofdev' } })
  .then((res) => get(res, [ 0, 'id' ]));
  // Create association for current and next year
  const thisYear = new Date().getFullYear();
  const asso = {
    operYear: thisYear,
    userId: ntofdevId,
    institute: 92 // ntof
  };
  await sources.associations.post(asso);
  asso.operYear++;
  await sources.associations.post(asso);
};

describe('Shifts', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */ ({});
      return utils.createApp(this.env)
      .then(() => this.env.server.db.createStub())
      .then(() => {
        Object.defineProperty(this.env.server.app.request, 'user', {
          configurable: true,
          enumerable: true,
          get() { return { sub: 'ntofdev' }; }
        });
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
    });
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can list all Shifts', async function() {
    wrapper = mount(Shifts, { router: createRouter('/shifts'), localVue: createLocalVue() });

    await waitForValue(() => wrapper.vm.loading, false);
    await waitForValue(() => wrapper.findAll('.cv-item').length > 0, true);
    // Click on previous month
    wrapper.find('.previousPeriod').trigger('click');
    // check there are 0 slots
    await waitForValue(() => wrapper.findAll('.cv-item').length > 0, false);
  });

  it('can take and free a slot', async function() {
    store.commit('user', {
      sub: 'ntofdev', cern_roles: [ 'user' ] }); // eslint-disable-line camelcase
    await createUserAssociation();

    wrapper = mount(Shifts, { router: createRouter('/shifts'), localVue: createLocalVue() });

    await waitForValue(() => wrapper.vm.loading, false);

    // Click on next month
    wrapper.find('.x-shifts-cal .nextPeriod').trigger('click');
    await waitForValue(() => wrapper.vm.loading, false);
    // Next month has no slot taken
    await waitForValue(() => wrapper.findAll('.x-shifts-cal .text-user').length > 0, false);
    // Enter edit mode
    wrapper.setData({ inEdit: true });
    // Check take the slot icon are present;
    await waitForValue(() => wrapper.findAll('.x-shifts-cal .fa-user-plus').length > 0, true);
    // take the slot
    wrapper.find('.x-shifts-cal .fa-user-plus').trigger('click');
    await waitForValue(() => wrapper.vm.loading, false);
    // now one slot is taken
    await waitForValue(() => wrapper.findAll('.text-user').length === 1, true);

    // Now free the slot. Find for .fa-times icon
    await waitForValue(() => wrapper.findAll('.x-shifts-cal .fa-times').length === 1, true);
    wrapper.find('.x-shifts-cal .fa-times').trigger('click');
    await waitForValue(() => wrapper.vm.loading, false);
    // Now should be free with no slot taken
    await waitForValue(() => wrapper.findAll('.x-shifts-cal .text-user').length > 0, false);
  });

  it('can free, reserve and cancel as admin', async function() {
    store.commit('user', {
      sub: 'ntofdev', cern_roles: [ 'user', 'editor', 'admin' ] }); // eslint-disable-line camelcase
    await createUserAssociation();

    wrapper = mount(Shifts, { router: createRouter('/admin/shifts'),
      localVue: createLocalVue(), attachTo: document.body });

    await waitForValue(() => wrapper.vm.loading, false, 'Component failed to load');

    // Click on next month
    wrapper.find('.x-shifts-cal .nextPeriod').trigger('click');
    await waitForValue(() => wrapper.vm.loading, false, 'Failed to load next period');
    // Next month has no slot taken
    await waitForValue(() => wrapper.findAll('.x-shifts-cal .text-user').length > 0, false,
      'there should be no slots taken');

    // Reserve a Slot
    await waitForValue(() => wrapper.findAll('.x-shifts-cal .fa-user-lock').length > 0, true,
      'could not find lock icon');
    wrapper.find('.x-shifts-cal .fa-user-lock').trigger('click');
    await waitForValue(() => wrapper.vm.loading, false,
      'Failed to assign user');
    // now one slot is taken (Reserved)
    await waitForValue(() => wrapper.findAll('.text-user').length === 1, true,
      'shift should be locked by user');

    // Now free the slot. Find for .fa-times icon
    await waitForValue(() => wrapper.findAll('.x-shifts-cal .fa-times').length === 1, true,
      'could not find user removal icon');
    wrapper.find('.x-shifts-cal .fa-times').trigger('click');
    // Press OK on the Dialog
    await waitForValue(() => wrapper.findAll('.x-shifts-cal .btn-danger').length === 1, true,
      'could not find ok button');
    await waitFor(() => isVisible(wrapper.find('.x-shifts-cal .btn-danger')),
      'confirmation modal did not load');
    wrapper.find('.x-shifts-cal .btn-danger').trigger('click');

    await waitForValue(() => wrapper.vm.loading, false,
      'failed to cancel slot');
    // Now should be free with no slot taken
    await waitForValue(() => wrapper.findAll('.x-shifts-cal .text-user').length > 0, false,
      'slot still assigned');

    // Now Cancel the slot
    await waitForValue(() => wrapper.findAll('.x-shifts-cal .fa-ban').length > 0, true,
      'failed to find cancel icon');
    wrapper.find('.x-shifts-cal .fa-ban').trigger('click');
    await waitForValue(() => wrapper.vm.loading, false,
      'fail to assign slot to cancel user');
    // now one slot is taken (Cancelled)
    await waitForValue(() => wrapper.findAll('.text-user').length === 1, true,
      'slot is not assigned to cancel user');
  });

});
