// @ts-check

import './karma_index';
import {
  createLocalVue, createRouter,
  waitForValue, waitForWrapper } from './utils';
import * as utilities from "../src/utilities";

import { get, set } from 'lodash';
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import { expect } from 'chai';

import App from '../src/App.vue';

// import sources, code may be elided otherwise
import { sources, default as store } from '../src/store';

/*::
declare var serverRequire: (string) => any
*/

describe('Institutes', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => this.env.server.db.createStub())
      .then(() => {
        var addr = this.env.server.address();
        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
    });
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can admin institute', async function() {
    store.commit('user', {
      sub: 'ntofdev', cern_roles: [ 'user', 'editor', 'admin' ] }); // eslint-disable-line camelcase
    wrapper = mount(App, {
      router: createRouter('/institutes'), localVue: createLocalVue() });

    (await waitForWrapper(() => wrapper.find('.fa-key'), 'admin button not found'))
    .trigger('click');

    const list = await waitForWrapper(() => wrapper.find('.x-institutes'),
      'institutes list not found');
    await waitForValue(() => get(list, [ 'vm', 'isAdmin' ]), true);

    (await waitForWrapper(() => wrapper.findComponent({ name: 'InstituteLink' }),
      'institute link not found'))
    .trigger('click');

    const info = await waitForWrapper(() => wrapper.find('.x-instituteinfo'),
      'institute info not found');
    await waitForValue(() => get(info, [ 'vm', 'inEdit' ]), true);

    (await waitForWrapper(() => info.findComponent({ ref: 'name' }),
      'name input not found'))
    .setData({ editValue: 'name test' });

    (await waitForWrapper(() => info.find('.x-submit'), 'submit button not found'))
    .trigger('click');

    await waitForValue(() => get(info, [ 'vm', 'inEdit' ]), false);
    // item updated in DB
    const items = await sources.institutes.get({ query: { id: get(info, [ 'vm', 'id' ]) }, max: 1 });
    expect(items).to.have.length(1);
    expect(items[0]).to.have.property('name', 'name test');
  });

  it('can admin create a new institute', async function() {
    store.commit('user', {
      sub: 'ntofdev', cern_roles: [ 'user', 'editor', 'admin' ] }); // eslint-disable-line camelcase
    wrapper = mount(App, {
      router: createRouter('/admin/institutes'), localVue: createLocalVue() });

    (await waitForWrapper(() => wrapper.find('.x-add')))
    .trigger('click');

    const dialog = await waitForWrapper(() => wrapper.find('.x-add-dialog'));
    set(dialog.findComponent({ ref: 'name' }), [ 'vm', 'editValue' ], 'test name');
    set(dialog.findComponent({ ref: 'city' }), [ 'vm', 'editValue' ], 'test city');
    set(dialog.findComponent({ ref: 'country' }), [ 'vm', 'editValue' ], 'test country');
    set(dialog.findComponent({ ref: 'alias' }), [ 'vm', 'editValue' ], 'test alias');

    dialog.findAll('.btn-primary').filter((b) => (b.text() === 'Ok'))
    .trigger('click');

    const info = (await waitForWrapper(() => wrapper.findComponent({ name: 'InstituteInfo' })));
    await waitForValue(() => get(info, [ 'vm', 'institute', 'name' ]),
      'test name');
    const id = get(info, [ 'vm', 'institute', 'id' ]);

    // item is in DB
    expect(await sources.institutes.get({ query: { id } })).to.have.length(1);
  });
});
