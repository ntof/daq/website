// @ts-check

import './karma_index';
import {
  createLocalVue, createRouter,
  waitForValue, waitForWrapper } from './utils';
import * as utilities from "../src/utilities";

import { set } from 'lodash';
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import Runs from '../src/components/Runs/Runs.vue';

// import sources, code may be elided otherwise
/* eslint-disable-next-line @typescript-eslint/no-unused-vars */
import { default as store } from '../src/store';  // jshint ignore:line

/*::
declare var serverRequire: (string) => any
*/

describe('Runs', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */ ({});
      return utils.createApp(this.env)
      .then(() => this.env.server.db.createStub())
      .then(() => {
        var addr = this.env.server.address();
        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
    });
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can list Runs', async function() {
    wrapper = mount(Runs, { propsData: { area: 'EAR2' },
      router: createRouter(), localVue: createLocalVue() });

    var options = wrapper.find('.x-entries').findAll('option');
    options.at(1).setSelected();
    await waitForValue(() => wrapper.findAll('tbody tr').length, 40);

    options.at(2).setSelected();
    await waitForValue(() => wrapper.findAll('tbody tr').length, 100);
  });

  it('can find a specific Run', async function() {
    wrapper = mount(Runs, { propsData: { area: 'EAR2' },
      router: createRouter(), localVue: createLocalVue() });

    const input = await waitForWrapper(
      () => wrapper.findComponent({ ref: 'runNumber' }));
    set(input, [ 'vm', 'editValue' ], "211507");

    wrapper.find('button.x-search').trigger('click');

    await waitForValue(
      () => wrapper.find('tbody tr td:nth-child(2)').text(), "211507");
  });
});
