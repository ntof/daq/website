// @ts-check

import './karma_index';
import {
  createLocalVue, createRouter,
  waitForValue, waitForWrapper } from './utils';
import * as utilities from "../src/utilities";

import { get } from 'lodash';
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import { expect } from 'chai';

import App from '../src/App.vue';

// import sources, code may be elided otherwise
import { sources, default as store } from '../src/store';

/*::
declare var serverRequire: (string) => any
*/

describe('Users', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => this.env.server.db.createStub())
      .then(() => {
        Object.defineProperty(this.env.server.app.request, 'user', {
          configurable: true,
          enumerable: true,
          get() { return { sub: 'ntofdev' }; }
        });
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
    });
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can edit current user', async function() {
    store.commit('user', {
      sub: 'ntofdev', cern_roles: [ 'user' ] }); // eslint-disable-line camelcase
    wrapper = mount(App, {
      router: createRouter('/me'), localVue: createLocalVue() });

    const info = await waitForWrapper(() => wrapper.find('.x-userinfo'));
    (await waitForWrapper(() => info.find('.fa-cog')))
    .trigger('click');
    await waitForValue(() => get(info, [ 'vm', 'inEdit' ]), true);

    (await waitForWrapper(() => info.findComponent({ ref: 'firstName' })))
    .setData({ editValue: 'name test' });

    (await waitForWrapper(() => info.find('.x-submit')))
    .trigger('click');

    await waitForValue(() => get(info, [ 'vm', 'inEdit' ]), false);
    // item updated in DB
    const items = await sources.users.get({ query: { login: 'ntofdev' }, max: 1 });
    expect(items).to.have.length(1);
    expect(items[0]).to.have.property('firstName', 'name test');
  });

  it('can admin another user', async function() {
    store.commit('user', {
      sub: 'ntofdev', cern_roles: [ 'user', 'editor', 'admin' ] }); // eslint-disable-line camelcase
    wrapper = mount(App, {
      router: createRouter('/users'), localVue: createLocalVue() });

    (await waitForWrapper(() => wrapper.find('.fa-key')))
    .trigger('click');

    const usersList = await waitForWrapper(() => wrapper.find('.x-users'));
    await waitForValue(() => get(usersList, [ 'vm', 'isAdmin' ]), true);

    const user = await waitForWrapper(() => wrapper.findComponent({ name: 'UserLink' }));
    user.trigger('click');

    const info = await waitForWrapper(() => wrapper.find('.x-userinfo'));
    await waitForValue(() => get(info, [ 'vm', 'inEdit' ]), true);

    (await waitForWrapper(() => info.findComponent({ ref: 'firstName' })))
    .setData({ editValue: 'name test' });

    (await waitForWrapper(() => info.find('.x-submit')))
    .trigger('click');

    await waitForValue(() => get(info, [ 'vm', 'inEdit' ]), false);
    // item updated in DB
    const items = await sources.users.get({ query: { login: 'ntofdev' }, max: 1 });
    expect(items).to.have.length(1);
    expect(items[0]).to.have.property('firstName', 'name test');
  });

  it('can set an association', async function() {
    await sources.operationPeriods.post({ year: 4242, start: 0, stop: 0 });
    /** @type {AppStore.Institute[]} */
    const institutes = await sources.institutes.get({ max: 2 });
    expect(institutes).to.have.length(2);

    store.commit('user', {
      sub: 'ntofdev', cern_roles: [ 'user' ] }); // eslint-disable-line camelcase
    wrapper = mount(App, {
      router: createRouter('/me'), localVue: createLocalVue() });

    const info = await waitForWrapper(() => wrapper.find('.x-userinfo'),
      'failed to find userinfo');
    (await waitForWrapper(() => info.find('.fa-cog'), 'failed to find cog'))
    .trigger('click');
    await waitForValue(() => get(info, [ 'vm', 'inEdit' ]), true);
    await waitForValue(() => get(info, [ 'vm', 'operYear' ]), 4242);

    (await waitForWrapper(() => info.findComponent({ ref: 'institute' }), 'failed to find institute'))
    .setData({ editValue: get(institutes, [ 0, 'id' ]) });

    (await waitForWrapper(() => info.find('.x-submit'), 'failed to find submit'))
    .trigger('click');

    await waitForValue(() => get(info, [ 'vm', 'inEdit' ]), false);
    // item updated in DB
    const items = await sources.associations.get({ query: {
      operYear: 4242, institute: get(institutes, [ 0, 'id' ]) }, max: 1 });
    expect(items).to.have.length(1);
    expect(items[0]).to.deep.contains(
      { operYear: 4242, institute: get(institutes, [ 0, 'id' ]), userId: 4242 });
  });
});
