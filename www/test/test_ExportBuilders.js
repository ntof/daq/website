// @ts-check

import './karma_index';
import { map, omit } from 'lodash';
import { describe, it } from 'mocha';
import { createWrapper } from '@vue/test-utils';

import { CSVBuilder, HTMLTableBuilder, JSONBuilder } from '../src/components/ExportBuilders';

import { expect } from 'chai';

const testData = [
  { field1: "value1", field2: 2, field3: 3 },
  { field1: "value11", field2: 22, field3: 33 }
];

/**
 * @param {string} str
 * @returns {Element}
 */
const stringToHTML = (str) => {
  var dom = document.createElement('div');
  dom.innerHTML = str;
  return /** @type {Element} */(dom.firstChild);
};

/*::
declare var serverRequire: (string) => any
*/

describe('ExportBuilders', function() {

  it('JSONBuilder', async function() {
    const builder = new JSONBuilder();
    builder.append(testData);
    const jsonData = JSON.parse(builder.getStringData());
    expect(jsonData).to.be.deep.equal(testData);
  });

  it('JSONBuilder with ignored fields', async function() {
    const builder = new JSONBuilder();
    builder.setIgnoredFields([ 'field3' ]);
    builder.append(testData);
    const jsonData = JSON.parse(builder.getStringData());
    const expectedTestData = map(testData, (i) => omit(i, [ 'field3' ]));
    expect(jsonData).to.be.deep.equal(expectedTestData);
  });

  it('CSVBuilder', async function() {
    const builder = new CSVBuilder();
    builder.append(testData);
    const csvData = builder.getStringData();
    const expectedData = 'field1,field2,field3\n' +
                         '"value1",2,3\n' +
                         '"value11",22,33';
    expect(csvData).equal(expectedData);
  });

  it('CSVBuilder with ignored fields', async function() {
    const builder = new CSVBuilder();
    builder.setIgnoredFields([ 'field3' ]);
    builder.append(testData);
    const csvData = builder.getStringData();
    const expectedData = 'field1,field2\n' +
                         '"value1",2\n' +
                         '"value11",22';
    expect(csvData).equal(expectedData);
  });

  it('HTMLTableBuilder', async function() {
    const builder = new HTMLTableBuilder();
    builder.append(testData);
    const htmlDataStr = builder.getStringData();
    const tableDom = /** @type {HTMLElement} */(stringToHTML(htmlDataStr));
    const wrapperTable = createWrapper(tableDom);
    const ths = wrapperTable.findAll('th');
    expect(ths.length).equal(3);
    expect(ths.at(0).element.innerHTML).equal('field1');
    expect(ths.at(1).element.innerHTML).equal('field2');
    expect(ths.at(2).element.innerHTML).equal('field3');
    const trs = wrapperTable.findAll('tr');
    expect(trs.length).equal(3); // header + 2 data
  });

  it('HTMLTableBuilder with ignored fields', async function() {
    const builder = new HTMLTableBuilder();
    builder.setIgnoredFields([ 'field3' ]);
    builder.append(testData);
    const htmlDataStr = builder.getStringData();
    const tableDom = /** @type {HTMLElement} */(stringToHTML(htmlDataStr));
    const wrapperTable = createWrapper(tableDom);
    const ths = wrapperTable.findAll('th');
    expect(ths.length).equal(2);
    expect(ths.at(0).element.innerHTML).equal('field1');
    expect(ths.at(1).element.innerHTML).equal('field2');
    const trs = wrapperTable.findAll('tr');
    expect(trs.length).equal(3); // header + 2 data
  });
});
