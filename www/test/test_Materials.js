// @ts-check

import './karma_index';
import {
  createLocalVue, createRouter,
  waitFor, waitForValue, waitForWrapper } from './utils';
import * as utilities from "../src/utilities";

import { get, set } from 'lodash';
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import { expect } from 'chai';

import Materials from '../src/components/Materials/Materials.vue';
import App from '../src/App.vue';

// import sources, code may be elided otherwise
import { sources, default as store } from '../src/store';

/*::
declare var serverRequire: (string) => any
*/

describe('Materials', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => this.env.server.db.createStub())
      .then(() => {
        var addr = this.env.server.address();
        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
    });
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  /**
   * @param  {Tests.Wrapper} wrapper
   * @param  {string} type
   */
  function checkMaterialType(wrapper, type) {
    const firstRow = wrapper.find('tbody td');
    return firstRow.exists() && (get(store.state,
      [ 'materials', 'db', firstRow.text(), 'type' ]) === type);
  }

  it('can list Materials', async function() {
    wrapper = mount(Materials, {
      router: createRouter(), localVue: createLocalVue() });

    var options = wrapper.find('.x-entries').findAll('option');
    options.at(0).setSelected();
    await waitFor(() => wrapper.findAll('tbody tr').length > 0);

    for (const type of [ 'sample', 'source', 'filter' ]) {
      const button = await waitForWrapper(
        () => wrapper.find('button.x-' + type));
      button.trigger('click');
      await waitFor(checkMaterialType.bind(null, wrapper, type));
    }
  });

  it('can search Materials', async function() {
    wrapper = mount(Materials, {
      router: createRouter(), localVue: createLocalVue() });

    wrapper.find('button.x-filter').trigger('click');
    await waitFor(checkMaterialType.bind(null, wrapper, 'filter'));

    const input = await waitForWrapper(
      () => wrapper.findComponent({ ref: 'element' }));
    set(input, [ 'vm', 'editValue' ], 'Pb');

    wrapper.find('button.x-search').trigger('click');

    await waitForValue(
      () => get(wrapper, [ 'vm', 'rows', 0, 'compound' ]), "Pb");
  });

  it('can create Materials', async function() {
    wrapper = mount(App, {
      router: createRouter('/admin/materials'), localVue: createLocalVue() });

    (await waitForWrapper(() => wrapper.find('.x-add')))
    .trigger('click');

    const dialog = await waitForWrapper(() => wrapper.find('.x-add-dialog'));
    set(dialog.findComponent({ ref: 'title' }), [ 'vm', 'editValue' ], 'new element title');

    dialog.findAll('.btn-primary').filter((b) => (b.text() === 'Ok'))
    .trigger('click');

    const info = (await waitForWrapper(() => wrapper.findComponent({ name: 'MaterialInfo' })));
    await waitForValue(() => get(info, [ 'vm', 'material', 'title' ]),
      'new element title');
    const id = get(info, [ 'vm', 'material', 'id' ]);

    // item is in DB
    expect(await sources.materials.get({ query: { id } })).to.have.length(1);

    wrapper.find('.x-delete').trigger('click');
    // confirm
    wrapper.findAll('button.btn-danger').filter((b) => (b.text() === 'Ok'))
    .trigger('click');

    // redirected to materials
    await waitForWrapper(() => wrapper.find('.x-materials'));
    // item is not in DB
    expect(await sources.materials.get({ query: { id } })).to.have.length(0);
  });
});
