// @ts-check

import './karma_index';
import {
  createLocalVue, createRouter,
  waitFor, waitForValue, waitForWrapper } from './utils';
import * as utilities from "../src/utilities";

import { get, set } from 'lodash';
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import { expect } from 'chai';

import Detectors from '../src/components/Detectors/Detectors.vue';
import App from '../src/App.vue';

// import sources, code may be elided otherwise
import { sources } from '../src/store';

/*::
declare var serverRequire: (string) => any
*/

describe('Detectors', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => this.env.server.db.createStub())
      .then(() => {
        var addr = this.env.server.address();
        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
    });
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can list Detectors', async function() {
    wrapper = mount(Detectors, {
      router: createRouter(), localVue: createLocalVue() });

    var options = wrapper.find('.x-entries').findAll('option');
    options.at(0).setSelected();
    await waitFor(() => wrapper.findAll('tbody tr').length > 0);
  });

  it('can search Detectors', async function() {
    wrapper = mount(Detectors, {
      router: createRouter(), localVue: createLocalVue() });

    const input = await waitForWrapper(
      () => wrapper.findComponent({ ref: 'trecNumber' }));
    set(input, [ 'vm', 'editValue' ], "12345");

    wrapper.find('button.x-search').trigger('click');

    await waitForValue(
      () => get(wrapper, [ 'vm', 'rows', 0, 'trecNumber' ]), "12345");
  });

  it('can create Detectors', async function() {
    wrapper = mount(App, {
      router: createRouter('/admin/detectors'), localVue: createLocalVue() });

    (await waitForWrapper(() => wrapper.find('.x-add')))
    .trigger('click');

    const dialog = await waitForWrapper(() => wrapper.find('.x-add-dialog'));
    set(dialog.findComponent({ ref: 'name' }), [ 'vm', 'editValue' ], 'new detector name');

    dialog.findAll('.btn-primary').filter((b) => (b.text() === 'Ok'))
    .trigger('click');

    const info = (await waitForWrapper(() => wrapper.findComponent({ name: 'DetectorInfo' })));
    await waitForValue(() => get(info, [ 'vm', 'detector', 'name' ]),
      'new detector name');
    const id = get(info, [ 'vm', 'detector', 'id' ]);

    // item is in DB
    expect(await sources.detectors.get({ query: { id } })).to.have.length(1);

    wrapper.find('.x-delete').trigger('click');
    // confirm
    wrapper.findAll('button.btn-danger').filter((b) => (b.text() === 'Ok'))
    .trigger('click');

    // redirected to materials
    await waitForWrapper(() => wrapper.find('.x-detectors'));
    // item is not in DB
    expect(await sources.detectors.get({ query: { id } })).to.have.length(0);
  });
});
