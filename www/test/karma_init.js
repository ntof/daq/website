
import Vue from 'vue';
import { config as tuConfig } from '@vue/test-utils';
import { afterEach, before } from 'mocha';

import {
  default as BaseVue,
  BaseLogger as logger } from "@cern/base-vue";

import VueUtils from '../src/VueUtils';
import { Area } from '../src/Consts';
import store from '../src/store';

import d from 'debug';
const debug = d('test:error');

before(function() {
  Vue.use(VueUtils);
  Vue.use(BaseVue);

  /* "provide" doesn't seem to work properly, using mocks */
  tuConfig.mocks['$store'] = store;

  // Override default DNS values
  Area.EAR1.dns = 'ear1-fake-url.cern.ch';
  Area.EAR2.dns = 'ear2-fake-url.cern.ch';
  Area.EAR3.dns = 'ear3-fake-url.cern.ch';
  Area.LAB.dns = 'lab-fake-url.cern.ch';

  logger.autoReload = false; // do not reload browser on network error
  logger.on('error', (error) => debug('error:', error));
});

afterEach(function() {
  logger.clear();
  // several tests are using the router
  // it takes time to mount, and mounting Layout may first display previous test
  // page then refresh
  window.location.hash = '';
});
