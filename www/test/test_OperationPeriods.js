// @ts-check

import './karma_index';
import {
  createLocalVue, createRouter, isVisible, waitFor,
  waitForValue, waitForWrapper } from './utils';
import * as utilities from "../src/utilities";

import { forEach, get, map, omit, set } from 'lodash';
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import { expect } from 'chai';
import App from '../src/App.vue';
import moment from 'moment';

// import sources, code may be elided otherwise
import { sources, default as store } from '../src/store';

async function prepareDB() {
  await sources.operationPeriods.post({
    year: 2039,
    start: moment.utc([ 2039 ]).startOf('year').valueOf(),
    stop: moment.utc([ 2039 ]).endOf('year').valueOf()
  });

  await sources.operationPeriods.post({
    year: 2040,
    start: moment.utc([ 2040 ]).startOf('year').valueOf(),
    stop: moment.utc([ 2040 ]).endOf('year').valueOf()
  });
  const instituteId = await sources.institutes.post({
    name: 'test', city: 'nowhere', country: 'none', alias: 'test', status: 1
  });

  const users = await sources.users.get({ });
  await sources.associations.post(map(users, (u) => ({
    operYear: 2039, userId: u.id, institute: instituteId })));

  // Create shiftpoints for 2039
  const shiftKinds = [ 'shift-leader', 'night', 'morning', 'afternoon' ];
  const weekdays = [ 0, 1, 2, 3, 4, 5, 6 ];
  /** @type Array<Partial<AppStore.PointsShifts>> */
  const pointShifts = [];
  forEach(shiftKinds, (shiftKind) => {
    forEach(weekdays, (day) => {
      pointShifts.push({ day, shiftKind, operYear: 2039, value: 10 });
    });
  });
  await sources.pointsShifts.post(pointShifts);

  // Create due points for 2039
  const institutes = await sources.institutes.get({ max: 200, query: {} });
  /** @type Array<Partial<AppStore.DuePoints>> */
  const duePoints = [];
  forEach(institutes, (inst) => {
    duePoints.push({ operYear: 2039, institute: inst.id, nUsers: 1, shiftPoints: 2 });
  });
  await sources.duePoints.post(duePoints);

  sources.operationPeriods.clear();
  sources.institutes.clear();
  sources.users.clear();
  sources.associations.clear();
  sources.pointsShifts.clear();
  sources.duePoints.clear();
}

describe('OperationPeriods', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => this.env.server.db.createStub())
      .then(() => {
        var addr = this.env.server.address();
        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      store.commit('user', {
        sub: 'ntofdev', cern_roles: [ 'user', 'editor', 'admin' ] }); // eslint-disable-line camelcase
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
  });

  it('can create OperationPeriod', async function() {
    wrapper = mount(App, {
      router: createRouter('/admin/operationPeriods'), localVue: createLocalVue() });
    (await waitForWrapper(() => wrapper.find('.x-add')))
    .trigger('click');

    const dialog = await waitForWrapper(() => wrapper.find('.x-add-dialog'));
    set(dialog.findComponent({ ref: 'year' }), [ 'vm', 'editValue' ], '2040');
    dialog.findAll('.btn-primary').filter((b) => (b.text() === 'Ok'))
    .trigger('click');

    const info = await waitForWrapper(() => wrapper.findComponent({ name: 'OperationPeriodInfo' }));
    await waitForValue(() => get(info, [ 'vm', 'operationPeriod', 'year' ]), 2040);
    // item is in DB
    expect(await sources.operationPeriods.get({ query: { year: 2040 } })).to.have.length(1);

    // go edit mode
    (await waitForWrapper(() => info.find('.fa-cog'))).trigger('click');
    await waitFor(() => isVisible(wrapper.find('.x-submit')));
    // edit
    const startField = info.findComponent({ ref: 'start' });
    const startInputElem = /** @type HTMLInputElement */(startField.find('input').element);
    const oldStartValue = startInputElem.value;
    set(startField, [ 'vm', 'editTimestamp' ],
      moment([ 2040, 1, 12 ]).unix());
    await waitFor(() => startInputElem.value !== oldStartValue);
    // submit edit
    info.find('.x-submit').trigger('click');
    // check store is updated
    await waitForValue(
      () => get(store, [ 'state', 'operationPeriods', 'rows', 0, 'start' ]),
      moment([ 2040, 1, 12 ]).valueOf(), 'value not updated in UI');
    // check DB
    const period = await sources.operationPeriods.get({ query: { year: 2040 } });
    expect(get(period, [ 0, 'start' ]))
    .to.equal(moment([ 2040, 1, 12 ]).valueOf(), 'value not updated in DB');
  });

  it('can generate shifts slots and copy associations', async function() {
    await prepareDB();

    wrapper = mount(App, {
      router: createRouter('/admin/operationPeriods/2040'), localVue: createLocalVue() });

    const info = await waitForWrapper(
      () => wrapper.findComponent({ name: 'OperationPeriodInfo' }),
      'failed to find OperationPeriodInfo');

    (await waitForWrapper(() => info.find('.fa-cog'))).trigger('click');

    (await waitForWrapper(
      () => info.find('button.x-asso'))).trigger('click');
    await waitForValue(() => get(info.vm, 'processing'), false,
      'failed to process asso', 8000);

    (await waitForWrapper(
      () => info.find('button.x-shifts'))).trigger('click');
    await waitForValue(() => get(info.vm, 'processing'), false,
      'failed to process shifts');

    // item is in DB
    const users = await sources.users.get({ });
    expect(await sources.associations.get({ query: { operYear: 2040 } }))
    .to.have.length(users.length);

    expect(await sources.shifts.get(
      { query: { date: {
        '$gte': moment.utc([ 2040 ]).valueOf(),
        '$lte': moment.utc([ 2040 ]).endOf('year').valueOf()
      } } }))
    .to.not.be.empty();
  });

  it('create points and duePoints automatically from previous year', async function() {
    await prepareDB();

    wrapper = mount(App, {
      router: createRouter('/admin/operationPeriods/2040'), localVue: createLocalVue() });

    const shiftPointsInfo = await waitForWrapper(
      () => wrapper.findComponent({ name: 'ShiftPoints' }));
    await waitForValue(() => get(shiftPointsInfo.vm, 'loading'), false);

    // points
    const points2039 = await sources.pointsShifts.get({ query: { operYear: 2039 } })
    .then((res) => map(res, (p) => omit(p, [ 'id', 'operYear' ])));
    const points2040 = await sources.pointsShifts.get({ query: { operYear: 2040 } })
    .then((res) => map(res, (p) => omit(p, [ 'id', 'operYear' ])));
    expect(points2039).to.deep.equal(points2040);

    // duePoints
    const duePoints2039 = await sources.duePoints.get({ query: { operYear: 2039 } })
    .then((res) => map(res, (p) => omit(p, [ 'id', 'operYear', 'shiftPoints' ])));
    const duePoints2040 = await sources.duePoints.get({ query: { operYear: 2040 } })
    .then((res) => map(res, (p) => omit(p, [ 'id', 'operYear', 'shiftPoints' ])));
    expect(duePoints2039).to.deep.equal(duePoints2040);
  });
});
