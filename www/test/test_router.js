// @ts-check

import './karma_index';
import { createLocalVue, waitForValue, createRouter } from './utils';
import * as utilities from "../src/utilities";

import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import App from '../src/App.vue';
import appRouter from '../src/router';

// import sources, code may be elided otherwise
import { default as store } from '../src/store';

/*::
declare var serverRequire: (string) => any
*/

describe('Router', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => this.env.server.db.createStub())
      .then(() => {
        var addr = this.env.server.address();
        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
    });
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('is granted admin access', async function() {
    // eslint-disable-next-line camelcase
    store.commit('user', { cern_roles: [ 'user', 'editor', 'admin' ] });

    wrapper = mount(App, {
      router: createRouter('/'), localVue: createLocalVue() });
    wrapper.vm.$router.push('/admin/materials');

    await waitForValue(() => wrapper.find('button.x-add').exists(), true);
  });

  it('is denied admin access', async function() {
    // eslint-disable-next-line camelcase
    store.commit('user', { cern_roles: [ 'user' ] });
    await appRouter.push('/').catch(() => undefined);

    wrapper = mount(App, {
      router: appRouter, localVue: createLocalVue() });
    wrapper.vm.$router.push('/admin/materials');

    // home redirect
    await waitForValue(() => wrapper.find('.x-home').exists(), true);
  });
});
