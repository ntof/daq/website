// @ts-check

import './karma_index';
import {
  createLocalVue, createRouter,
  waitForValue, waitForWrapper } from './utils';
import * as utilities from "../src/utilities";

import { cloneDeep, get, pick, set } from 'lodash';
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import { expect } from 'chai';

import RunInfo from '../src/components/Runs/RunInfo.vue';

// import sources, code may be elided otherwise
/* eslint-disable-next-line @typescript-eslint/no-unused-vars */
import { sources, default as store } from '../src/store'; // jshint ignore:line

/**
 * @typedef {() =>any} serverRequire
 */

describe('RunInfo', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(/** @this {any} */ function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */ ({});
      return utils.createApp(this.env)
      .then(() => this.env.server.db.createStub())
      .then(() => {
        var addr = this.env.server.address();
        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
    });
  });

  afterEach(async function() {
    await server.run(/** @this {any} */ function() {
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can display Run DAQ configuration', async function() {
    wrapper = mount(RunInfo, {
      router: createRouter('/EAR2/211522'),
      localVue: createLocalVue()
    });

    await waitForValue(() => get(wrapper.vm, 'runLoading'), false);
    (await waitForWrapper(() => wrapper.find('.x-daq-config .b-clickable')))
    .trigger('click');

    const daqConfig = await waitForWrapper(() => wrapper.findComponent({ name: 'DAQConfig' }));
    await waitForValue(
      () => daqConfig.find('tbody tr td:nth-child(2)').text(), "NAID");
  });

  it('can display Run HV configuration', async function() {
    wrapper = mount(RunInfo, {
      router: createRouter('/EAR2/211522'),
      localVue: createLocalVue()
    });

    await waitForValue(() => get(wrapper.vm, 'runLoading'), false);
    (await waitForWrapper(() => wrapper.find('.x-hv-config .b-clickable')))
    .trigger('click');

    const daqConfig = await waitForWrapper(() => wrapper.findComponent({ name: 'HVConfig' }));
    await waitForValue(() => {
      const elt = daqConfig.find('tbody tr td:nth-child(3)');
      return elt.exists() ? elt.text() : undefined;
    }, "LaBr2");
  });

  it('can modify run DataStatus', async function() {
    // eslint-disable-next-line camelcase
    store.commit('user', { cern_roles: [ 'user', 'editor' ] });
    
    wrapper = mount(RunInfo, {
      router: createRouter('/EAR2/211522'),
      localVue: createLocalVue()
    });

    await waitForValue(() => get(wrapper.vm, 'runLoading'), false);

    (await waitForWrapper(
      () => wrapper.find('.fa-cog'))).trigger('click');

    (await waitForWrapper(
      () => wrapper.find('.btn.x-edit-data-status'))).trigger('click');

    const dialog = await waitForWrapper(
      () => wrapper.findComponent({ ref: 'editDataStatusDialog' }));

    set(dialog.vm, [ '$refs', 'status', 'editValue' ], 'bad');
    dialog.find('.x-submit').trigger('click');

    await waitForValue(() => get(wrapper.vm, [ 'run', 'dataStatus' ]), 'bad');
    expect(get(store, [ 'state', 'comments', 'rows', 0, 'comments' ])).contains(' -> bad');
  });

  it('can add a Comment', async function() {
    wrapper = mount(RunInfo, {
      router: createRouter('/EAR2/211522'),
      localVue: createLocalVue()
    });

    await waitForValue(() => get(wrapper.vm, 'runLoading'), false);

    // eslint-disable-next-line camelcase
    store.commit('user', { cern_roles: [ 'user', 'editor' ] });
    (await waitForWrapper(
      () => wrapper.find('.fa-cog'))).trigger('click');

    const comments = await waitForWrapper(
      () => wrapper.findComponent({ name: 'CommentsList' }));
    comments.find('.x-add').trigger('click');

    const dialog = comments.findComponent({ name: 'AddCommentDialog' });
    set(await waitForWrapper(() => dialog.findComponent({ ref: 'md' })),
      [ 'vm', 'editValue' ], 'this is a test !');

    comments.find('.x-submit').trigger('click');

    await waitForValue(
      () => get(store, [ 'state', 'comments', 'rows', 0, 'comments' ]),
      'this is a test !');
  });

  it('can recalculate trigger counters', async function() {
    // Get this run from db (most recent run with triggers data!)
    const run211523 = await sources.runs.getById(211523);
    // clone and update run with wrong counters
    const clonedRun211523 = cloneDeep(run211523);
    clonedRun211523.triggersCalibration = 1111;
    clonedRun211523.triggersPrimary = 1111;
    clonedRun211523.triggersParasitic = 1111;

    wrapper = mount(RunInfo, {
      router: createRouter('/EAR2/211523'),
      localVue: createLocalVue()
    });

    await waitForValue(() => get(wrapper.vm, 'runLoading'), false);

    // eslint-disable-next-line camelcase
    store.commit('user', { cern_roles: [ 'user', 'editor', 'admin' ] });

    // Check that the value are the same as the db
    await waitForValue(() => get(wrapper.vm, [ 'run', 'triggersCalibration' ]), run211523.triggersCalibration);
    await waitForValue(() => get(wrapper.vm, [ 'run', 'triggersPrimary' ]), run211523.triggersPrimary);
    await waitForValue(() => get(wrapper.vm, [ 'run', 'triggersParasitic' ]), run211523.triggersParasitic);

    // Modify the run
    await sources.runs.put(clonedRun211523.runNumber,
        pick(clonedRun211523, ['triggersCalibration', 'triggersPrimary', 'triggersParasitic']))

    // reload the run by invalidating the cache
    await wrapper.vm.fetch(true);
    await waitForValue(() => get(wrapper.vm, 'runLoading'), false);

    // Check that the value are now the modified one
    await waitForValue(() => get(wrapper.vm, [ 'run', 'triggersCalibration' ]), 1111);
    await waitForValue(() => get(wrapper.vm, [ 'run', 'triggersPrimary' ]), 1111);
    await waitForValue(() => get(wrapper.vm, [ 'run', 'triggersParasitic' ]), 1111);

    // Go on edit mode anc click on Update Triggers button
    (await waitForWrapper(
      () => wrapper.find('.fa-cog'))).trigger('click');
    (await waitForWrapper(
      () => wrapper.find('.btn.x-update-triggers'))).trigger('click');

    // wait the proces to be done
    await waitForValue(() => get(wrapper.vm, 'updateTriggersOngoing'), false);
    await waitForValue(() => get(wrapper.vm, 'inEdit'), false);

    // when update completes, it will automatically refetch the run
    // check that values are back to originals
    await waitForValue(() => get(wrapper.vm, [ 'run', 'triggersCalibration' ]), run211523.triggersCalibration);
    await waitForValue(() => get(wrapper.vm, [ 'run', 'triggersPrimary' ]), run211523.triggersPrimary);
    await waitForValue(() => get(wrapper.vm, [ 'run', 'triggersParasitic' ]), run211523.triggersParasitic);
  });
});
