// @ts-check

import './karma_index';
import {
  createLocalVue, createRouter,
  waitForValue, waitForWrapper } from './utils';
import * as utilities from "../src/utilities";

import { get, size, toNumber, noop } from 'lodash';
import { expect } from 'chai';
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import MaterialInfo from '../src/components/Materials/MaterialInfo.vue';
import App from '../src/App.vue';
import appRouter from '../src/router';

// import sources, code may be elided otherwise
import { default as store } from '../src/store';

/*::
declare var serverRequire: (string) => any
*/

describe('MaterialInfo', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => this.env.server.db.createStub())
      .then(() => {
        var addr = this.env.server.address();
        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
    });
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can display MaterialInfo', async function() {
    wrapper = mount(MaterialInfo, {
      router: createRouter('/materials/298'), localVue: createLocalVue() });

    await waitForValue(
      () => get(wrapper, [ 'vm', 'material', 'id' ]), 298);

    await waitForValue(() => wrapper.find('.x-desc').find('.b-markdown-viewer').text(),
      'Co 0.25 mm EAR2 pos#1');
  });

  it('can navigate to MaterialInfo', async function() {
    wrapper = mount(App, {
      router: createRouter('/'), localVue: createLocalVue() });
    wrapper.vm.$router.push('/materials');

    const row = await waitForWrapper(() => wrapper.find('tbody tr'));
    row.trigger('click');

    const info = await waitForWrapper(
      () => wrapper.findComponent({ name: 'MaterialInfo' }));
    expect(toNumber(info.vm.$route.params.id)).to.be.gte(0);
  });

  it('can edit', async function() {
    // eslint-disable-next-line camelcase
    store.commit('user', { cern_roles: [ 'user', 'editor', 'admin' ] });

    wrapper = mount(MaterialInfo, {
      router: createRouter('/materials/298'), localVue: createLocalVue() });

    wrapper.vm.setEdit(true);

    (await waitForWrapper(
      () => wrapper.findComponent({ ref: 'title' })))
    .setData({ editValue: 'test' });

    (await waitForWrapper(() => wrapper.find('.x-submit')))
    .trigger('click');

    // out of edit mode
    await waitForValue(() => wrapper.find('.fa-cog').exists(), true);
    await waitForValue(
      () => get(wrapper, [ 'vm', 'material', 'title' ]), 'test');

    // this must be visible only to editors/admin
    // eslint-disable-next-line camelcase
    store.commit('user', { cern_roles: [ 'user' ] });
    await waitForValue(() => wrapper.find('.fa-cog').exists(), false);
  });

  it('can load related info', async function() {
    wrapper = mount(MaterialInfo, {
      router: createRouter('/materials/215'), localVue: createLocalVue() });

    await waitForValue(() => size(wrapper.vm.setups), 4);
    await waitForValue(() => size(wrapper.vm.docs), 1);
  });
});
