// @ts-check
/* eslint-disable max-lines */

/** @type {mrest.DatabaseDescription} */
module.exports = {
  runs: {
    table: "RUNS",
    key: "RUN_NUMBER",
    mapping: {
      RUN_NUMBER: "runNumber",
      RUN_START: "start",
      RUN_STOP: "stop",
      RUN_TITLE: "title",
      RUN_DESCRIPTION: "description",
      RUN_TITLE_ORIGINAL: "titleOriginal",
      RUN_DESCRIPTION_ORIGINAL: "descOriginal",
      RUN_CASTOR_FOLDER: "castorFolder",
      RUN_TOTAL_PROTONS: "totalProtons",
      RUN_RUN_STATUS: "runStatus",
      RUN_DATA_STATUS: "dataStatus",
      RUN_TRIGGERS_CALIBRATION: "triggersCalibration",
      RUN_TRIGGERS_PRIMARY: "triggersPrimary",
      RUN_TRIGGERS_PARASITIC: "triggersParasitic",
      RUN_DET_SETUP_ID: "detSetupId",
      RUN_MAT_SETUP_ID: "matSetupId"
    },
    strict: true,
    related: {
      triggers: {
        path: "triggers",
        key: "runNumber"
      },
      castorFiles: {
        path: "castorFiles",
        key: "runNumber"
      },
      documents: {
        path: "documents",
        join: {
          table: "REL_DOCS_RUNS",
          key: "RUN_NUMBER",
          relKey: "DOC_ID"
        }
      },
      comments: {
        path: "comments",
        key: "runNumber"
      },
      hvParameters: {
        path: "hvParameters",
        key: "runNumber"
      },
      configurations: {
        path: "configurations",
        key: "runNumber"
      }
    }
  },
  triggers: {
    table: "TRIGGERS",
    key: "TRIG_ID",
    mapping: {
      TRIG_ID: "id",
      TRIG_SOURCE: "source",
      TRIG_CYCLESTAMP: "cycleStamp",
      TRIG_TRIGGER_NUMBER: "triggerNumber",
      TRIG_INTENSITY: "intensity",
      TRIG_RUN_NUMBER: "runNumber"
    },
    strict: true
  },
  operationPeriods: {
    table: "OPERATION_PERIODS",
    key: "OPER_YEAR",
    mapping: {
      OPER_YEAR: "year",
      OPER_START: "start",
      OPER_STOP: "stop"
    },
    strict: true,
    related: {
      pointsShifts: {
        path: "pointsShifts",
        key: "operYear"
      },
      duePoints: {
        path: "duePoints",
        key: "operYear"
      },
      associations: {
        path: "associations",
        key: "operYear"
      }
    }
  },
  materials: {
    table: "MATERIALS",
    key: "MAT_ID",
    mapping: {
      MAT_ID: "id",
      MAT_TYPE: "type",
      MAT_TITLE: "title",
      MAT_COMPOUND: "compound",
      MAT_MASS_NUMBER: "massNumber",
      MAT_MASS_MG: "mass",
      MAT_ATOMIC_NUMBER: "atomicNumber",
      MAT_THICKNESS_UM: "thickness",
      MAT_DIAMETER_MM: "diameter",
      MAT_STATUS: "status",
      MAT_AREA_DENSITY_UM_CM2: "areaDensity",
      MAT_DESCRIPTION: "description"
    },
    strict: true,
    related: {
      materialsSetups: {
        path: "materialsSetups",
        join: {
          table: "REL_MATERIALS_SETUPS",
          key: "MAT_ID",
          relKey: "MAT_SETUP_ID"
        }
      },
      documents: {
        path: "documents",
        join: {
          table: "REL_DOCS_MATERIALS",
          key: "MAT_ID",
          relKey: "DOC_ID"
        }
      }
    }
  },
  materialsType: {
    table: "MATERIALS_TYPE",
    key: "MAT_TYPE",
    mapping: {
      MAT_TYPE: "matType"
    },
    strict: true,
    related: {
      materials: {
        path: "materials",
        key: "matType"
      }
    }
  },
  materialsSetups: {
    table: "MATERIALS_SETUPS",
    key: "MAT_SETUP_ID",
    mapping: {
      MAT_SETUP_ID: "id",
      MAT_SETUP_NAME: "name",
      MAT_SETUP_DESCRIPTION: "description",
      MAT_SETUP_EAR_NUMBER: "earNumber",
      MAT_SETUP_OPER_YEAR: "operYear"
    },
    strict: true,
    related: {
      materials: {
        path: "materials",
        join: {
          table: "REL_MATERIALS_SETUPS",
          key: "MAT_SETUP_ID",
          relKey: "MAT_ID"
        }
      },
      materialsSetupInfo: {
        path: "materialsSetupInfo",
        key: "materialsSetupId"
      },
      runs: {
        path: "runs",
        key: "matSetupId"
      }
    }
  },
  materialsSetupInfo: {
    table: "REL_MATERIALS_SETUPS",
    mapping: {
      MAT_ID: "materialId",
      MAT_SETUP_ID: "materialsSetupId",
      MAT_POSITION: "position",
      MAT_STATUS: "status"
    },
    strict: true
  },
  detectorsSetups: {
    table: "DETECTORS_SETUPS",
    key: "DET_SETUP_ID",
    mapping: {
      DET_SETUP_ID: "id",
      DET_SETUP_NAME: "name",
      DET_SETUP_DESCRIPTION: "description",
      DET_SETUP_EAR_NUMBER: "earNumber",
      DET_SETUP_OPER_YEAR: "operYear"
    },
    strict: true,
    related: {
      detectorsSetupInfo: {
        path: "detectorsSetupInfo",
        key: 'detectorSetupId'
      },
      detectors: {
        path: "detectors",
        join: {
          table: "REL_DETECTORS_SETUPS",
          key: "DET_SETUP_ID",
          relKey: "DET_ID"
        }
      },
      containers: {
        path: "containers",
        join: {
          table: "REL_DETECTORS_SETUPS",
          key: "DET_SETUP_ID",
          relKey: "DET_CONT_ID"
        }
      },
      documents: {
        path: "documents",
        join: {
          table: "REL_DOCS_DET_SETUPS",
          key: "DET_SETUP_ID",
          relKey: "DOC_ID"
        }
      },
      runs: {
        path: "runs",
        key: "detSetupId"
      }
    }
  },
  detectorsSetupInfo: {
    table: "REL_DETECTORS_SETUPS",
    mapping: {
      DET_ID: "detectorId",
      DET_SETUP_ID: "detectorSetupId",
      DET_CONT_ID: "containerId"
    },
    strict: true
  },
  comments: {
    table: "COMMENTS",
    key: "COM_ID",
    mapping: {
      COM_ID: "id",
      COM_DATE: "date",
      COM_COMMENTS: "comments",
      COM_RUN_NUMBER: "runNumber",
      COM_USER_ID: "userId"
    },
    strict: true
  },
  users: {
    table: "USERS",
    key: "USER_ID",
    mapping: {
      USER_ID: "id",
      USER_LOGIN: "login",
      USER_FIRST_NAME: "firstName",
      USER_LAST_NAME: "lastName",
      USER_EMAIL: "email",
      USER_STATUS: "status",
      USER_PRIVILEGES: "privileges",
      USER_INVOLVMENTS: "involvments"
    },
    strict: true,
    related: {
      comments: {
        path: "comments",
        key: "userId"
      },
      associations: {
        path: "associations",
        key: "userId"
      }
    }
  },
  institutes: {
    table: "INSTITUTES",
    key: "INST_ID",
    mapping: {
      INST_ID: "id",
      INST_NAME: "name",
      INST_CITY: "city",
      INST_COUNTRY: "country",
      INST_ALIAS: "alias",
      INST_STATUS: "status"
    },
    strict: true,
    related: {
      associations: {
        path: "associations",
        key: "institute"
      },
      duePoints: {
        path: "duePoints",
        key: "institute"
      }
    }
  },
  shifts: {
    table: "SHIFTS",
    key: "SHIFT_ID",
    mapping: {
      SHIFT_ID: "id",
      SHIFT_DATE: "date",
      SHIFT_ASSO_ID: "assoId",
      SHIFT_SHK_KIND: "shiftKind"
    },
    strict: true
  },
  associations: {
    table: "ASSOCIATIONS",
    key: "ASSO_ID",
    mapping: {
      ASSO_ID: "id",
      ASSO_OPER_YEAR: "operYear",
      ASSO_USER_ID: "userId",
      ASSO_INST_ID: "institute"
    },
    strict: true,
    related: {
      shifts: {
        path: "shifts",
        key: "assoId"
      }
    }
  },
  pointsShifts: {
    table: "POINTS_SHIFTS",
    key: "POINT_ID",
    mapping: {
      POINT_ID: "id",
      POINT_DAY_OF_WEEK: "day",
      POINT_VALUE: "value",
      POINT_OPER_YEAR: "operYear",
      POINT_SHK_KIND: "shiftKind"
    },
    strict: true
  },
  shiftKinds: {
    table: "SHIFT_KINDS",
    key: "SHK_KIND",
    mapping: {
      SHK_KIND: "shiftKind"
    },
    strict: true,
    related: {
      shifts: {
        path: "shifts",
        key: "shiftKind"
      },
      pointsShifts: {
        path: "pointsShifts",
        key: "shiftKind"
      }
    }
  },
  duePoints: {
    table: "DUE_POINTS",
    key: "DUE_ID",
    mapping: {
      DUE_ID: "id",
      DUE_OPER_YEAR: "operYear",
      DUE_INST_ID: "institute",
      DUE_NUMBER_USERS: "nUsers",
      DUE_SHIFT_POINTS: "shiftPoints"
    },
    strict: true
  },
  containers: {
    table: "CONTAINERS",
    key: "CONT_ID",
    mapping: {
      CONT_ID: "id",
      CONT_NAME: "name",
      CONT_DESCRIPTION: "description",
      CONT_TRECNUMBER: "trecNumber"
    },
    related: {
      detectorsSetups: {
        path: "detectorsSetups",
        join: {
          table: "REL_DETECTORS_SETUPS",
          key: "DET_CONT_ID",
          relKey: "DET_SETUP_ID"
        }
      }
    },
    strict: true
  },
  detectors: {
    table: "DETECTORS",
    key: "DET_ID",
    mapping: {
      DET_ID: "id",
      DET_NAME: "name",
      DET_DESCRIPTION: "description",
      DET_DAQNAME: "daqName",
      DET_TRECNUMBER: "trecNumber"
    },
    strict: true,
    related: {
      detectorsSetups: {
        path: "detectorsSetups",
        join: {
          table: "REL_DETECTORS_SETUPS",
          key: "DET_ID",
          relKey: "DET_SETUP_ID"
        }
      },
      documents: {
        path: "documents",
        join: {
          table: "REL_DOCS_DETECTORS",
          key: "DET_ID",
          relKey: "DOC_ID"
        }
      }
    }
  },
  configurations: {
    table: "CONFIGURATIONS",
    key: "CONF_ID",
    mapping: {
      CONF_ID: "id",
      CONF_STATUS: "status",
      CONF_RUN_NUMBER: "runNumber",
      CONF_DAQI_ID: "daqiId"
    },
    strict: true,
    related: {
      daqParameters: {
        path: "daqParameters",
        join: {
          table: "REL_CONF_PARAMS",
          key: "CONF_ID",
          relKey: "PARAM_ID"
        }
      }
    }
  },
  daqParameters: {
    table: "DAQ_PARAMETERS",
    key: "PARAM_ID",
    mapping: {
      PARAM_ID: "id",
      PARAM_INDEX: "index",
      PARAM_NAME: "name",
      PARAM_VALUE: "value"
    },
    strict: true,
    related: {
      configurations: {
        path: "configurations",
        join: {
          table: "REL_CONF_PARAMS",
          key: "PARAM_ID",
          relKey: "CONF_ID"
        }
      }
    }
  },
  relConfParams: {
    table: 'REL_CONF_PARAMS',
    mapping: {
      PARAM_ID: 'paramId',
      CONF_ID: 'confId'
    }
  },
  daqInstruments: {
    table: "DAQ_INSTRUMENTS",
    key: "DAQI_ID",
    mapping: {
      DAQI_ID: "id",
      DAQI_CHANNEL: "channel",
      DAQI_MODULE: "module",
      DAQI_CRATE: "crate",
      DAQI_STREAM: "stream"
    },
    strict: true,
    related: {
      configurations: {
        path: "configurations",
        key: "daqiId"
      }
    }
  },
  hvParameters: {
    table: "HV_PARAMETERS",
    key: "HV_ID",
    mapping: {
      HV_ID: "id",
      HV_SLOT_ID: "slotId",
      HV_SERIAL_NUMBER: "serialNumber",
      HV_CHANNEL_ID: "channelId",
      HV_NAME: "name",
      HV_VO_SET_VOLT: "setVolt",
      HV_IO_SET_AMP: "setAmp",
      HV_TRIP: "trip",
      HV_RAMP_UP: "rampUp",
      HV_RAMP_DOWN: "rampDown",
      HV_V_MAX: "max",
      HV_RUN_NUMBER: "runNumber"
    },
    strict: true
  },
  castorFiles: {
    table: "CASTOR_FILES",
    key: "CF_ID",
    mapping: {
      CF_ID: "id",
      CF_FILE_NAME: "filename",
      CF_FILE_PATH: "path",
      CF_FILE_SIZE: "size",
      CF_CREATED: "created",
      CF_STATUS: "status",
      CF_STREAM: "stream",
      CF_EXTENSION: "extension",
      CF_RUN_NUMBER: "runNumber"
    },
    strict: true,
    related: {
      castorFileEvents: {
        path: "castorFileEvents",
        key: "castorFileID"
      }
    }
  },
  castorFileEvents: {
    table: "CASTOR_FILE_EVENTS",
    key: "CFE_ID",
    mapping: {
      CFE_ID: "id",
      CFE_EVENT: "event",
      CFE_OFFSET: "offset",
      CFE_SEQUENCE: "sequence",
      CFE_CF_ID: "castorFileID"
    },
    strict: true
  },
  documents: {
    table: "DOCUMENTS",
    key: "DOC_ID",
    mapping: {
      DOC_ID: "id",
      DOC_TITLE: "title",
      DOC_FILE_NAME: "name",
      DOC_SIZE: "size"
    },
    streams: {
      DOC_DOCUMENT: 'document'
    },
    strict: true,
    related: {
      runs: {
        path: "runs",
        join: {
          table: "REL_DOCS_RUNS",
          key: "DOC_ID",
          relKey: "RUN_NUMBER"
        }
      },
      detectorsSetups: {
        path: "detectorsSetups",
        join: {
          table: "REL_DOCS_DET_SETUPS",
          key: "DOC_ID",
          relKey: "DET_SETUP_ID"
        }
      },
      detectors: {
        path: "detectors",
        join: {
          table: "REL_DOCS_DETECTORS",
          key: "DOC_ID",
          relKey: "DET_ID"
        }
      },
      materials: {
        path: "materials",
        join: {
          table: "REL_DOCS_MATERIALS",
          key: "DOC_ID",
          relKey: "MAT_ID"
        }
      }
    }
  },
  extraData: {
    table: "EXTRA_DATA",
    key: "EXTRA_KEY",
    mapping: {
      EXTRA_KEY: "key",
      EXTRA_VALUE: "value"
    },
    strict: true
  }
};
