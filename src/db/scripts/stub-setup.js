#!/usr/bin/env node
// @ts-check
/* eslint-disable max-lines */

const
  debug = require('debug')('app:stub'),
  { createGunzip } = require('zlib'),
  readline = require('readline'),
  fs = require('fs'),
  path = require('path'),
  knex = require('knex'),
  { first, omit, map, transform } = require('lodash');

/**
 * @typedef {knex.Knex.TableBuilder} TableBuilder
 */

/**
 * @param {knex.Knex} db
 * @param {string} table
 * @param {(table: TableBuilder) => any} fun
 */
async function createTable(db, table, fun) {
  if (!await db.schema.hasTable(table)) {
    await db.schema.createTable(table, fun);
  }
}

/**
 * @param {knex.Knex} db
 */
async function createSchema(db) {
  debug('Building schema');
  /* tables are kept in the same order than database-miugration scripts */
  await createTable(db, 'OPERATION_PERIODS', function(table) {
    table.integer('OPER_YEAR').primary();
    table.integer('OPER_START').notNullable();
    table.integer('OPER_STOP').notNullable();
  });

  await createTable(db, 'MATERIALS', function(table) {
    table.increments('MAT_ID');
    table.string('MAT_TYPE', 20).notNullable();
    table.string('MAT_TITLE', 100).notNullable();
    table.string('MAT_COMPOUND', 25).notNullable();
    table.integer('MAT_MASS_NUMBER').notNullable();
    table.float('MAT_MASS_MG', 126);
    table.integer('MAT_ATOMIC_NUMBER');
    table.float('MAT_THICKNESS_UM', 126);
    table.float('MAT_DIAMETER_MM', 126);
    table.integer('MAT_STATUS').notNullable();
    table.float('MAT_AREA_DENSITY_UM_CM2', 126);
    table.string('MAT_DESCRIPTION', 4000).notNullable();
    table.index([ 'MAT_TYPE', 'MAT_COMPOUND' ], 'MAT_TYPE_COMPOUND_IDX');
  });

  await createTable(db, 'MATERIALS_SETUPS', function(table) {
    table.increments('MAT_SETUP_ID');
    table.string('MAT_SETUP_NAME', 255).unique();
    table.string('MAT_SETUP_DESCRIPTION', 255);
    table.integer('MAT_SETUP_EAR_NUMBER').notNullable();
    table.integer('MAT_SETUP_OPER_YEAR').notNullable();
    table.index([ 'MAT_SETUP_EAR_NUMBER', 'MAT_SETUP_OPER_YEAR' ],
      'MAT_EAR_YEAR_IDX');
  });

  await createTable(db, 'DOCUMENTS', function(table) {
    table.increments('DOC_ID');
    table.string('DOC_TITLE', 255).notNullable();
    table.string('DOC_FILE_NAME', 100).notNullable();
    table.integer('DOC_SIZE').notNullable();
    table.binary('DOC_DOCUMENT');
  });

  await createTable(db, 'DETECTORS', function(table) {
    table.increments('DET_ID');
    table.string('DET_NAME', 50).notNullable().unique();
    table.string('DET_DESCRIPTION', 255).notNullable();
    table.string('DET_DAQNAME', 255);
    table.string('DET_TRECNUMBER', 255);

    table.index([ 'DET_DAQNAME' ], 'DET_DAQNAME_IDX');
  });

  await createTable(db, 'DETECTORS_SETUPS', function(table) {
    table.increments('DET_SETUP_ID');
    table.string('DET_SETUP_NAME', 255).unique();
    table.string('DET_SETUP_DESCRIPTION', 255);
    table.integer('DET_SETUP_EAR_NUMBER').notNullable();
    table.integer('DET_SETUP_OPER_YEAR').notNullable();
    table.index(
      [ 'DET_SETUP_EAR_NUMBER', 'DET_SETUP_OPER_YEAR', 'DET_SETUP_NAME' ],
      'DET_SETUP_EAR_YEAR_NAME_IDX');
  });

  await createTable(db, 'CONTAINERS', function(table) {
    table.increments('CONT_ID');
    table.string('CONT_NAME', 255).notNullable();
    table.string('CONT_DESCRIPTION', 255);
    table.string('CONT_TRECNUMBER', 255);

    table.index([ 'CONT_NAME' ], 'CONT_NAME_IDX');
  });

  await createTable(db, 'POINTS_SHIFTS', function(table) {
    table.increments('POINT_ID');
    table.integer('POINT_DAY_OF_WEEK').notNullable();
    table.float('POINT_VALUE', 126).notNullable();
    table.integer('POINT_OPER_YEAR').notNullable();
    table.string('POINT_SHK_KIND', 35).notNullable();

    table.unique([ 'POINT_OPER_YEAR', 'POINT_DAY_OF_WEEK', 'POINT_SHK_KIND' ],
      'POINT_YEAR_WEEK_KIND_UNIQ');
  });

  await createTable(db, 'USERS', function(table) {
    table.increments('USER_ID');
    table.string('USER_LOGIN', 20).unique()
    .comment('may be null for expired users');
    table.string('USER_FIRST_NAME', 50).notNullable();
    table.string('USER_LAST_NAME', 50).notNullable();
    table.string('USER_EMAIL', 50).notNullable();
    table.integer('USER_STATUS').notNullable();
    table.integer('USER_PRIVILEGES').notNullable();
    table.string('USER_INVOLVMENTS', 4000);

    table.index([ 'USER_LAST_NAME', 'USER_FIRST_NAME' ], 'USER_NAME_IDX');
    table.index([ 'USER_LOGIN' ], 'USER_LOGIN_IDX');
  });

  await createTable(db, 'INSTITUTES', function(table) {
    table.increments('INST_ID');
    table.string('INST_NAME', 255).notNullable();
    table.string('INST_CITY', 50);
    table.string('INST_COUNTRY', 50).notNullable();
    table.string('INST_ALIAS', 50).unique().notNullable();
    table.integer('INST_STATUS').notNullable();

    table.index([ 'INST_STATUS', 'INST_NAME' ], 'INST_STATUS_NAME_IDX');
  });

  await createTable(db, 'DUE_POINTS', function(table) {
    table.increments('DUE_ID');
    table.integer('DUE_OPER_YEAR').notNullable()
    .references('OPERATION_PERIODS.OPER_YEAR').withKeyName('DUE_OPER_YEAR_FK');
    table.integer('DUE_INST_ID').notNullable()
    .references('INSTITUTES.INST_ID').withKeyName('DUE_INST_ID_FK');
    table.integer('DUE_NUMBER_USERS').notNullable();
    table.float('DUE_SHIFT_POINTS', 126).notNullable();

    table.unique([ 'DUE_OPER_YEAR', 'DUE_INST_ID' ],
      'DUE_OPER_YEAR_INST_ID_UNIQ');
  });

  await createTable(db, 'ASSOCIATIONS', function(table) {
    table.increments('ASSO_ID');
    table.integer('ASSO_OPER_YEAR').notNullable()
    .references('OPERATION_PERIODS.OPER_YEAR').withKeyName('ASSO_OPER_YEAR_FK');
    table.integer('ASSO_USER_ID').notNullable()
    .references('USERS.USER_ID').withKeyName('ASSO_USER_ID_FK');
    table.integer('ASSO_INST_ID').notNullable()
    .references('INSTITUTES.INST_ID').withKeyName('ASSO_INST_ID_FK');

    table.unique([ 'ASSO_OPER_YEAR', 'ASSO_USER_ID' ], 'ASSO_OPER_USER_UNIQ');
    table.index('ASSO_USER_ID', 'ASSO_USER_IDX');
    table.index([ 'ASSO_OPER_YEAR', 'ASSO_INST_ID' ], 'ASSO_YEAR_INST_IDX');
  });

  await createTable(db, 'SHIFTS', function(table) {
    table.increments('SHIFT_ID');
    table.integer('SHIFT_DATE').notNullable();
    table.integer('SHIFT_ASSO_ID')
    .references('ASSOCIATIONS.ASSO_ID').withKeyName('SHIFT_ASSO_ID_FK');
    table.string('SHIFT_SHK_KIND', 35).notNullable()
    .references('SHIFT_KINDS.SHK_KIND').withKeyName('SHIFT_SHK_KIND_FK');

    table.unique([ 'SHIFT_DATE', 'SHIFT_SHK_KIND' ], 'SHIFT_DATE_KIND_UNIQ');
    table.index([ 'SHIFT_DATE', 'SHIFT_ASSO_ID' ], 'SHIFT_DATE_ASSO_IDX');
  });

  await createTable(db, 'RUNS', function(table) {
    table.integer('RUN_NUMBER').primary();
    table.integer('RUN_START').notNullable();
    table.integer('RUN_STOP');
    table.string('RUN_TITLE', 255).notNullable();
    table.string('RUN_DESCRIPTION', 4000);
    table.string('RUN_TITLE_ORIGINAL', 255);
    table.string('RUN_DESCRIPTION_ORIGINAL', 4000);
    table.string('RUN_CASTOR_FOLDER', 255).notNullable();
    table.integer('RUN_TOTAL_PROTONS').defaultTo(0);
    table.string('RUN_RUN_STATUS', 12).notNullable();
    table.string('RUN_DATA_STATUS', 12).notNullable();
    table.integer('RUN_TRIGGERS_CALIBRATION').defaultTo(0);
    table.integer('RUN_TRIGGERS_PRIMARY').defaultTo(0);
    table.integer('RUN_TRIGGERS_PARASITIC').defaultTo(0);

    table.integer('RUN_DET_SETUP_ID');

    table.integer('RUN_MAT_SETUP_ID');

    table.index([ 'RUN_START' ], 'RUN_START_IDX');
    table.index([ 'RUN_TITLE' ], 'RUN_TITLE_IDX');
  });

  await createTable(db, 'TRIGGERS', function(table) {
    table.integer('TRIG_ID').primary();
    table.string('TRIG_SOURCE', 55).notNullable();
    table.integer('TRIG_CYCLESTAMP');
    table.integer('TRIG_TRIGGER_NUMBER').notNullable();
    table.float('TRIG_INTENSITY');
    table.integer('TRIG_RUN_NUMBER').notNullable()
    .references('RUNS.RUN_NUMBER').withKeyName('TRIGGERS_RUN_NUMBER_FK');
  });

  await createTable(db, 'COMMENTS', function(table) {
    table.increments('COM_ID');
    table.integer('COM_DATE').notNullable();
    table.string('COM_COMMENTS', 4000);
    table.integer('COM_RUN_NUMBER').notNullable();
    table.integer('COM_USER_ID').notNullable();

    table.index([ 'COM_RUN_NUMBER', 'COM_DATE' ], 'COM_RUN_DATE_IDX');
  });

  await createTable(db, 'HV_PARAMETERS', function(table) {
    table.increments('HV_ID');
    table.integer('HV_SLOT_ID').notNullable();
    table.integer('HV_SERIAL_NUMBER').comment('null on old entries');
    table.integer('HV_CHANNEL_ID').notNullable();
    table.string('HV_NAME', 255).notNullable();
    table.float('HV_VO_SET_VOLT', 126).notNullable();
    table.float('HV_IO_SET_AMP', 126).notNullable();
    table.float('HV_TRIP', 126).notNullable();
    table.float('HV_RAMP_UP', 126).notNullable();
    table.float('HV_RAMP_DOWN', 126).notNullable();
    table.integer('HV_V_MAX').notNullable();
    table.integer('HV_RUN_NUMBER').notNullable();
    table.unique([ 'HV_RUN_NUMBER', 'HV_CHANNEL_ID', 'HV_SLOT_ID' ],
      'HV_RUN_CHAN_SLOT_UNIQ');

    table.index([ 'HV_RUN_NUMBER' ], 'HV_RUN_NUMBER_IDX');
  });

  await createTable(db, 'DAQ_INSTRUMENTS', function(table) {
    table.increments('DAQI_ID');
    table.integer('DAQI_CHANNEL').notNullable();
    table.integer('DAQI_MODULE').notNullable();
    table.integer('DAQI_CRATE').notNullable();
    table.integer('DAQI_STREAM').notNullable();
    table.unique([ 'DAQI_STREAM', 'DAQI_CRATE', 'DAQI_MODULE', 'DAQI_CHANNEL' ],
      'DAQ_INSTRUMENTS_UNIQ');
  });

  await createTable(db, 'DAQ_PARAMETERS', function(table) {
    table.increments('PARAM_ID');
    table.integer('PARAM_INDEX').notNullable();
    table.string('PARAM_NAME', 50).notNullable();
    table.string('PARAM_VALUE', 50);
    table.unique([ 'PARAM_INDEX', 'PARAM_NAME', 'PARAM_VALUE' ],
      'DAQ_PARAMETERS_UNIQ');
  });

  await createTable(db, 'CONFIGURATIONS', function(table) {
    table.increments('CONF_ID');
    table.integer('CONF_STATUS').notNullable();
    table.integer('CONF_RUN_NUMBER').notNullable();
    table.integer('CONF_DAQI_ID').notNullable();
    table.unique([ 'CONF_RUN_NUMBER', 'CONF_DAQI_ID' ],
      'CONFIGURATIONS_RUN_DAQI_UNIQ');

    table.index([ 'CONF_RUN_NUMBER' ], 'CONF_RUN_NUMBER_IDX');
  });

  await createTable(db, 'REL_DOCS_MATERIALS', function(table) {
    table.integer('MAT_ID').notNullable()
    .references('MATERIALS.MAT_ID').withKeyName('DOC_MAT_ID_FK');
    table.integer('DOC_ID').notNullable()
    .references('DOCUMENTS.DOC_ID').withKeyName('MAT_DOC_ID_FK');

    table.primary([ 'MAT_ID', 'DOC_ID' ]);
    table.index([ 'DOC_ID' ], 'REL_DM_DOC_IDX');
  });

  await createTable(db, 'REL_DOCS_DET_SETUPS', function(table) {
    table.integer('DET_SETUP_ID').notNullable()
    .references('DETECTORS_SETUPS.DET_SETUP_ID')
    .withKeyName('DOC_DET_SETUP_ID_FK');
    table.integer('DOC_ID').notNullable()
    .references('DOCUMENTS.DOC_ID').withKeyName('DET_SETUP_DOC_ID_FK');

    table.primary([ 'DET_SETUP_ID', 'DOC_ID' ]);
    table.index([ 'DOC_ID' ], 'REL_DDS_DOC_IDX');
  });

  await createTable(db, 'REL_DOCS_DETECTORS', function(table) {
    table.integer('DET_ID').notNullable()
    .references('DETECTORS.DET_ID').withKeyName('DOC_DET_ID_FK');
    table.integer('DOC_ID').notNullable()
    .references('DOCUMENTS.DOC_ID').withKeyName('DET_DOC_ID_FK');

    table.primary([ 'DET_ID', 'DOC_ID' ]);
    table.index([ 'DOC_ID' ], 'REL_DD_DOC_IDX');
  });

  await createTable(db, 'REL_DETECTORS_SETUPS', function(table) {
    table.integer('DET_ID').notNullable()
    .references('DETECTORS.DET_ID').withKeyName('SETUP_DET_ID_FK');
    table.integer('DET_SETUP_ID').notNullable()
    .references('DETECTORS_SETUPS.DET_SETUP_ID').withKeyName('DET_SETUP_ID_FK');
    table.integer('DET_CONT_ID')
    .references('CONTAINERS.CONT_ID').withKeyName('DET_CONT_ID_FK');

    table.primary([ 'DET_SETUP_ID', 'DET_ID' ]);
  });

  await createTable(db, 'REL_MATERIALS_SETUPS', function(table) {
    table.integer('MAT_ID').notNullable()
    .references('MATERIALS.MAT_ID').withKeyName('SETUP_MAT_ID_FK');
    table.integer('MAT_SETUP_ID').notNullable()
    .references('MATERIALS_SETUPS.MAT_SETUP_ID').withKeyName('MAT_SETUP_ID_FK');
    table.integer('MAT_STATUS').notNullable();
    table.integer('MAT_POSITION');

    table.primary([ 'MAT_SETUP_ID', 'MAT_ID' ]);
  });

  await createTable(db, 'REL_DOCS_RUNS', function(table) {
    table.integer('RUN_NUMBER').notNullable()
    .references('RUNS.RUN_NUMBER').withKeyName('DOC_RUN_NUMBER_FK');
    table.integer('DOC_ID').notNullable()
    .references('DOCUMENTS.DOC_ID').withKeyName('RUN_DOC_ID_FK');

    table.primary([ 'RUN_NUMBER', 'DOC_ID' ]);
    table.index([ 'DOC_ID' ], 'REL_DR_DOC_IDX');
  });

  await createTable(db, 'REL_CONF_PARAMS', function(table) {
    table.integer('PARAM_ID').notNullable()
    .references('DAQ_PARAMETERS.PARAM_ID').withKeyName('CON_PARAM_ID_FK');
    table.integer('CONF_ID').notNullable()
    .references('CONFIGURATIONS.CONF_ID').withKeyName('PARAM_CONF_ID_FK');

    table.primary([ 'CONF_ID', 'PARAM_ID' ]);
  });

  await createTable(db, 'EXTRA_DATA', function(table) {
    table.string('EXTRA_KEY', 64).primary();
    table.text('EXTRA_VALUE');
  });
}

/**
 * @param {knex.Knex} db
 * @param {string} file
 */
async function insertData(db, file) {
  const gz = createGunzip();
  fs.createReadStream(file).pipe(gz);

  var trx = await db.transaction();
  const reader = readline.createInterface({
    input: gz, terminal: false
  });
  var i = 0;
  var stmt = '';

  for await (const line of reader) {
    if (line.length <= 0) {
      continue;
    }
    else if (line[line.length - 1] === ';') {
      stmt = stmt + line.slice(0, -1);
    }
    else {
      stmt += line.replace(/; $/g, ';') + '\n';
      continue;
    }
    await trx.raw(stmt);
    stmt = '';

    if ((++i % 5000) === 0) {
      debug('rows inserted:', i);
      await trx.commit();
      trx = await db.transaction();
    }
  }
  debug('rows inserted:', i);
  await trx.commit();
}

/**
 * @param {knex.Knex} db
 */
async function updateData(db) {
  /* update stubs to always work properly :) */
  const today = new Date();
  const thisYear = today.getFullYear();
  const thisMonth = today.getMonth();

  await db('MATERIALS_SETUPS')
  .update({ MAT_SETUP_OPER_YEAR: thisYear })
  .where('MAT_SETUP_OPER_YEAR', 2018);

  await db('DETECTORS_SETUPS')
  .update({ DET_SETUP_OPER_YEAR: thisYear })
  .where('DET_SETUP_OPER_YEAR', 2018);

  await db.raw(db('USERS').insert({
    USER_EMAIL: 'nowhere@nowhere.com', USER_FIRST_NAME: 'ntofdev',
    USER_ID: 4242, USER_LAST_NAME: 'APC', USER_LOGIN: 'ntofdev',
    USER_PRIVILEGES: 0, USER_STATUS: 1
  }).toString().replace(/^insert/, 'insert or replace'));

  // Add some containers + detectors
  await db.raw(db('CONTAINERS').insert({
    CONT_ID: 1, CONT_NAME: 'test container',
    CONT_DESCRIPTION: 'Sample container with some description',
    CONT_TRECNUMBER: 1234
  }).toString().replace(/^insert/, 'insert or replace'));

  await db.raw(db('DETECTORS').insert({
    DET_ID: 1, DET_NAME: 'test detector',
    DET_DESCRIPTION: 'Sample detector with some description',
    DET_DAQNAME: 'DAQNAME', DET_TRECNUMBER: 12345
  }).toString().replace(/^insert/, 'insert or replace'));

  await db.raw(db('DETECTORS').insert({
    DET_ID: 2, DET_NAME: 'test detector 2',
    DET_DESCRIPTION: 'Sample detector with some description 2',
    DET_DAQNAME: 'DAQNAME 2', DET_TRECNUMBER: 123456
  }).toString().replace(/^insert/, 'insert or replace'));

  await db.raw(db('REL_DETECTORS_SETUPS').insert({
    DET_ID: 1, DET_CONT_ID: 1, DET_SETUP_ID: 22
  }).toString().replace(/^insert/, 'insert or replace'));

  await db.raw(db('REL_DOCS_DETECTORS').insert({
    DOC_ID: 55, DET_ID: 1
  }).toString().replace(/^insert/, 'insert or replace'));

  // Create OperationYear for 2017 - Used in tests for materials
  await db.raw(db('OPERATION_PERIODS').insert({
    OPER_YEAR: 2017,
    OPER_START: new Date(2017, 1).getTime(),
    OPER_STOP: new Date(2017, 8).getTime()
  }).toString().replace(/^insert/, 'insert or replace'));

  // Import hompage_sidebar markdown
  const homepageSidebar = fs.readFileSync(
    path.join(__dirname, 'homepage_sidebar.md'), 'utf8');
  await db.raw(db('EXTRA_DATA').insert({
    EXTRA_KEY: 'homepage_sidebar',
    EXTRA_VALUE: homepageSidebar
  }).toString().replace(/^insert/, 'insert or replace'));

  // For Shifts: Copy October 2018 to current month.
  // Delete SHIFTS starting from Jan 2021
  await db('SHIFTS').select('*')
  .where('SHIFT_DATE', '>=', new Date(2021, 0))
  .del();
  // Copy SHIFTS of October 2018 to current month and create new empty one also
  const dateDiff = new Date().setDate(1) - Date.parse('01 Oct 2018');
  await db('SHIFTS').select('*')
  .where('SHIFT_DATE', '>=', Date.parse('01 Oct 2018'))
  .where('SHIFT_DATE', '<', Date.parse('01 Nov 2018'))
  .then(async (shifts) => {
    const newShifts = map(shifts, (s) => {
      s['SHIFT_ID'] += 1000; // Avoid duplicates
      s['SHIFT_DATE'] += dateDiff;
      return s;
    });
    await db('SHIFTS').insert(newShifts);
    // Remove all extra slot of next month (it happen for 30th months and feb)
    await db('SHIFTS').select('*')
    .where('SHIFT_DATE', '>=',
      new Date(today.getFullYear(), today.getMonth() + 1)).del();
    // New empty slots
    const nextMonthDiff = new Date().setMonth(thisMonth + 1) - today.getTime();
    const newEmptyShifts = map(shifts, (s) => {
      s['SHIFT_ID'] += 1000; // Avoid duplicates
      s['SHIFT_DATE'] += nextMonthDiff; // = 2 month;
      s['SHIFT_ASSO_ID'] = null;
      return s;
    });
    await db('SHIFTS').insert(newEmptyShifts);
  });

  // Create Association for current and next year for Reserved and Cancelled fake users
  await db('USERS').select('*')
  .where('USER_FIRST_NAME', '=', 'Reserved')
  .orWhere('USER_FIRST_NAME', '=', 'Cancelled')
  .then(async (resSpecialUsers) => {
    const specialUserIds = map(resSpecialUsers, 'USER_ID');
    const ntofInstID = await db('INSTITUTES').select('*')
    .where('INST_ALIAS', '=', '__nTOF')
    .then((inst) => map(inst, 'INST_ID'));
    if (specialUserIds.length !== 2 || ntofInstID.length !== 1) {
      debug("ERROR creating special associations for reserved and cancelled");
      return;
    }
    /** @type any[] */
    const specialAssociations = transform(specialUserIds,
      function(/** @type any[] */ res, userId) {
        res.push({
          'ASSO_OPER_YEAR': thisYear,
          'ASSO_USER_ID': userId,
          'ASSO_INST_ID': first(ntofInstID)
        });
        res.push({
          'ASSO_OPER_YEAR': thisYear + 1,
          'ASSO_USER_ID': userId,
          'ASSO_INST_ID': first(ntofInstID)
        });
      }, []);

    await db.raw(db('ASSOCIATIONS').insert(specialAssociations)
    .toString().replace(/^insert/, 'insert or replace'));
  });

  debug('stub data udpated');
}

/**
 * @param {knex.Knex} db
 */
async function stubSetup(db) {
  await createSchema(db);
  await insertData(db, path.join(__dirname, 'sample.sql.dump.gz'));
  await updateData(db);
}

// @ts-ignore
if (!module.parent) {
  const config = require('../../config-stub'); // eslint-disable-line global-require
  // @ts-ignore
  const db = knex(omit(config.db, 'definition'));

  stubSetup(db)
  .catch((err) => console.log('Error:', err))
  .finally(() => db.destroy());
}
else {
  /* export our server */
  module.exports = stubSetup;
}
