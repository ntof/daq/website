// @ts-check
const
  _ = require('lodash'),
  // @ts-ignore
  config = require('../../../config'),
  knex = require('knex');

/* do not use node-oracledb LOB objects, dump content as a Buffer */
const oracledb = require('oracledb');
oracledb.fetchAsBuffer = [ oracledb.BLOB ];

/**
 * @typedef {{
 *  db: knex.Knex
 *  out: knex.Knex,
 *  history: { [tableName: string]: Set<string> }
 * }} Env
 */

/**
 *
 * @param {knex.Knex} k
 * @param {string} tableName
 * @param {any} row
 */
function sqlInsert(k, tableName, row) {
  row = _.omitBy(row, (value, key) => (key[0] === '_'));
  row = _.mapValues(row,
    (value) => (_.isString(value) ? value.replace(/;\r?\n/g, '; \n') : value));
  return k(tableName).insert(row)
  .toString().replace(/^insert/, 'insert or replace');
}

/**
 *
 * @param {Env} env
 * @param {string} tableName
 * @param {any[]} rows
 * @param {string} rowIdName
 */
function dumpRow(env, tableName, rows, rowIdName) {
  if (!_.get(env, [ 'history', tableName ])) {
    _.set(env, [ 'history', tableName ], new Set());
  }
  const hist = env.history[tableName];

  if (_.isArray(rows)) {
    rows = rows.filter((row) => !hist.has(row[rowIdName]));
    _.forEach(rows, (row) => {
      hist.add(row[rowIdName]);
      console.log(sqlInsert(env.out, tableName, row) + ';');
    });
  }
  else if (_.isObject(rows) && !hist.has(rows[rowIdName])) {
    hist.add(rows[rowIdName]);
    console.log(sqlInsert(env.out, tableName, rows) + ';');
  }
}

/**
 * @param {Env} env
 * @param {number} id
 */
async function dumpMaterialsRel(env, id) {
  await env.db('REL_DOCS_MATERIALS').select('*')
  .where('MAT_ID', id)
  .then(async (rows) => {
    for (const row of rows) {
      row._UUID = row.MAT_ID + ':' + row.DOC_ID;

      const docs = await env.db('DOCUMENTS').select('*')
      .where('DOC_ID', row.DOC_ID);
      dumpRow(env, 'DOCUMENTS', docs, 'DOC_ID');
    }
    dumpRow(env, 'REL_DOCS_MATERIALS', rows, '_UUID');
  });
}

/**
 * @param {Env} env
 * @param {number[]} ids
 */
async function dumpMaterialsSetups(env, ids) {
  await env.db('REL_MATERIALS_SETUPS').select('*')
  .whereIn('MAT_SETUP_ID', ids)
  .then(async (rows) => {
    for (const row of rows) {
      row._UUID = row.MAT_SETUP_ID + ':' + row.MAT_ID;

      const materials = await env.db('MATERIALS').select('*')
      .where('MAT_ID', row.MAT_ID);
      dumpRow(env, 'MATERIALS', materials, 'MAT_ID');
      await dumpMaterialsRel(env, row.MAT_ID);
    }
    dumpRow(env, 'REL_MATERIALS_SETUPS', rows, '_UUID');
  });
}

/**
 * @param {Env} env
 * @param {number[]} ids
 */
async function dumpDetectorsSetups(env, ids) {
  await env.db('REL_DETECTORS_SETUPS').select('*')
  .whereIn('DET_SETUP_ID', ids)
  .then(async (rows) => {
    for (const row of rows) {
      row._UUID = row.DET_SETUP_ID + ':' + row.DET_ID;

      await env.db('DETECTORS').select('*').where('DET_ID', row.DET_ID)
      .then((rows) => dumpRow(env, 'DETECTORS', rows, 'DET_ID'));

      await env.db('CONTAINERS').select('*').where('DET_CONT_ID', row.DET_ID)
      .then((rows) => dumpRow(env, 'CONTAINERS', rows, 'CONT_ID'));
    }
    dumpRow(env, 'REL_DETECTORS_SETUPS', rows, '_UUID');
  });
}

/**
 * @param {Env} env
 * @param {number[]} ids
 */
async function dumpRuns(env, ids) {
  await env.db('CONFIGURATIONS').select('*')
  .whereIn('CONF_RUN_NUMBER', ids)
  .then(async (rows) => {
    for (const chunk of _.chunk(rows, 250)) {
      const rel = await env.db('REL_CONF_PARAMS').select('*')
      .whereIn('CONF_ID', _.map(chunk, 'CONF_ID'));
      rel.forEach((r) => (r._UUID = `${r.PARAM_ID}:${r.CONF_ID}`));

      await env.db('DAQ_PARAMETERS').select('*')
      .whereIn('PARAM_ID', _.uniq(_.map(rel, 'PARAM_ID')))
      .then((rows) => dumpRow(env, 'DAQ_PARAMETERS', rows, 'PARAM_ID'));

      dumpRow(env, 'CONFIGURATIONS', rows, 'CONF_ID');
      dumpRow(env, 'REL_CONF_PARAMS', rel, '_UUID');
    }
  });
  await env.db('HV_PARAMETERS').select('*')
  .whereIn('HV_RUN_NUMBER', ids)
  .then(async (rows) => {
    dumpRow(env, 'HV_PARAMETERS', rows, 'HV_ID');
  });
  // Triggers only for the first run! (most recent)
  await env.db('TRIGGERS').select('*')
  .where('TRIG_RUN_NUMBER', _.first(ids))
  .then(async (rows) => {
    dumpRow(env, 'TRIGGERS', rows, 'TRIG_ID');
  });
}

/**
 * @param {Env} env
 * @param {number[]} ids
 */
async function dumpUsers(env, ids) {
  await env.db('USERS').select('*').whereIn('USER_ID', ids)
  .then((rows) => dumpRow(env, 'USERS', rows, 'USER_ID'));
}

/** @type {Env} */
const env = {
  // @ts-ignore
  db: knex(config.db),
  // @ts-ignore
  out: knex({ client: 'sqlite3', useNullAsDefault: true }),
  history: {}
};

(async () => {
  await env.db('MATERIALS_SETUPS').select('*')
  .where('MAT_SETUP_OPER_YEAR', 2017).limit(50)
  .then(async (rows) => {
    dumpRow(env, 'MATERIALS_SETUPS', rows, 'MAT_SETUP_ID');
    await dumpMaterialsSetups(env, _.map(rows, 'MAT_SETUP_ID'));
  });

  await env.db('MATERIALS_SETUPS').select('*')
  .where('MAT_SETUP_OPER_YEAR', 2018).limit(50)
  .then(async (rows) => {
    dumpRow(env, 'MATERIALS_SETUPS', rows, 'MAT_SETUP_ID');
    await dumpMaterialsSetups(env, _.map(rows, 'MAT_SETUP_ID'));
  });

  await env.db('DETECTORS_SETUPS').select('*')
  .where('DET_SETUP_OPER_YEAR', 2017).limit(50)
  .then(async (rows) => {
    dumpRow(env, 'DETECTORS_SETUPS', rows, 'DET_SETUP_ID');
    await dumpDetectorsSetups(env, _.map(rows, 'DET_SETUP_ID'));
  });

  await env.db('DETECTORS_SETUPS').select('*')
  .where('DET_SETUP_OPER_YEAR', 2018).limit(50)
  .then(async (rows) => {
    dumpRow(env, 'DETECTORS_SETUPS', rows, 'DET_SETUP_ID');
    await dumpDetectorsSetups(env, _.map(rows, 'DET_SETUP_ID'));
  });

  await env.db('COMMENTS').select('*')
  .where('COM_DATE', '>=', Date.parse('01 Jan 2018'))
  .where('COM_DATE', '<=', Date.parse('01 Jan 2019'))
  .then(async (comments) => {
    await dumpUsers(env, _.uniq(_.map(comments, 'COM_USER_ID')));
    dumpRow(env, 'COMMENTS', comments, 'COM_ID');
  });

  await env.db('DAQ_INSTRUMENTS').select('*')
  .then((rows) => dumpRow(env, 'DAQ_INSTRUMENTS', rows, 'DAQI_ID'));

  await env.db('RUNS').select('*')
  .where('RUN_START', '>=', Date.parse('01 Jan 2017'))
  .where('RUN_START', '<=', Date.parse('01 Jan 2018'))
  .limit(250).orderBy('RUN_NUMBER', 'desc')
  .then(async (runs) => {
    dumpRow(env, 'RUNS', runs, 'RUN_NUMBER');
  });

  await env.db('RUNS').select('*')
  .where('RUN_START', '>=', Date.parse('01 Jan 2018'))
  .where('RUN_START', '<=', Date.parse('01 Jan 2019'))
  .limit(500).orderBy('RUN_NUMBER', 'desc')
  .then(async (runs) => {
    _.forEach(runs, (r) => {
      if (!env.history['MATERIALS_SETUPS'].has(r.RUN_MAT_SETUP_ID)) {
        r.RUN_MAT_SETUP_ID = null;
      }
      if (!env.history['DETECTORS_SETUPS'].has(r.RUN_DET_SETUP_ID)) {
        r.RUN_DET_SETUP_ID = null;
      }
    });
    dumpRow(env, 'RUNS', runs, 'RUN_NUMBER');
    await dumpRuns(env, _.map(runs, 'RUN_NUMBER').slice(0, 5));
  });

  await env.db('OPERATION_PERIODS').select('*')
  .where('OPER_YEAR', 2018)
  .then(async (op) => {
    dumpRow(env, 'OPERATION_PERIODS', op, 'OPER_YEAR');
  });

  await env.db('POINTS_SHIFTS').select('*')
  .where('POINT_OPER_YEAR', 2018)
  .then(async (ps) => {
    dumpRow(env, 'POINTS_SHIFTS', ps, 'POINT_ID');
  });

  await env.db('DUE_POINTS').select('*')
  .where('DUE_OPER_YEAR', 2018)
  .then(async (ps) => {
    dumpRow(env, 'DUE_POINTS', ps, 'DUE_ID');
  });

  await env.db('INSTITUTES').select('*')
  .then((inst) => {
    dumpRow(env, 'INSTITUTES', inst, 'INST_ID');
  });

  await env.db('SHIFTS').select('*')
  .where('SHIFT_DATE', '>=', Date.parse('01 Jan 2018'))
  .where('SHIFT_DATE', '<=', Date.parse('01 Jan 2019'))
  .limit(500).orderBy('SHIFT_DATE', 'desc')
  .then(async (shifts) => {
    // Dump Relative Associations
    const assIds = _.uniq(_.map(shifts, 'SHIFT_ASSO_ID'));
    await env.db('ASSOCIATIONS').select('*').whereIn('ASSO_ID', assIds)
    .then(async (ass) => {
      // Get special users (reserved and canceled)
      const specialUserIds = await env.db('USERS').select('*')
      .where('USER_FIRST_NAME', '=', 'Reserved')
      .orWhere('USER_FIRST_NAME', '=', 'Cancelled')
      .then((resSpecialUsers) => _.map(resSpecialUsers, 'USER_ID'));
      // Dump Relative Users
      const userIds = _.map(ass, 'ASSO_USER_ID').concat(specialUserIds);
      await dumpUsers(env, _.uniq(userIds));

      dumpRow(env, 'ASSOCIATIONS', ass, 'ASSO_ID');
    });
    dumpRow(env, 'SHIFTS', shifts, 'SHIFT_ID');
  });

})()
.finally(() => {
  env.db.destroy();
});
