// @ts-check
const
  { assign, get, noop, cloneDeep,
    now, toNumber, isNaN, isNil } = require('lodash'),
  stubSetup = require('./scripts/stub-setup'),
  debug = require('debug')('app:db'),
  express = require('express'),
  bodyParser = require('body-parser'),
  KnexServer = require('@cern/mrest');

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 */

const doc = require('./doc');

class Database extends KnexServer {
  /**
   *
   * @param {string} basePath
   * @param {mrest.DatabaseDescription} schema
   * @param {mrest.KnexServer.Options} config
   */
  constructor(basePath, schema, config) {
    super(config, schema, assign(cloneDeep(doc), { basePath }));
  }

  /**
   * @param {boolean} userMode
   */
  router(userMode = true) {
    const router = express.Router();
    router.post('/comments', bodyParser.json(),
      KnexServer.ExpressHelpers.wrap(this.insertComment.bind(this)));
    router.put('/runs/item/:id/updateTriggers', bodyParser.json(),
      KnexServer.ExpressHelpers.wrap(this.updateRunTriggers.bind(this)));
    if (userMode) {
      router.put('/shifts/item/:id', bodyParser.json(),
        KnexServer.ExpressHelpers.wrap(this.updateShift.bind(this)));
      router.put('/users/item/:id', bodyParser.json(),
        KnexServer.ExpressHelpers.wrap(this.updateUser.bind(this)));
      router.put('/associations/item/:id', bodyParser.json(),
        KnexServer.ExpressHelpers.wrap(
          this.updateAssociation.bind(this, false)));
      router.delete('/associations/item/:id',
        KnexServer.ExpressHelpers.wrap(
          this.updateAssociation.bind(this, true)));
      router.post('/associations', bodyParser.json(),
        KnexServer.ExpressHelpers.wrap(this.insertAssociation.bind(this)));
    }
    super.register(router);
    return router;
  }

  /**
   * @param {Request} req
   * @param {string | null} def default username to use
   */
  async getUserId(req, def = 'ntofdev') {
    const login = get(req, 'user.sub', def);
    if (!login) return null;
    return this.tables['users'].search( // @ts-ignore fake Request
      { get: noop, query: { max: 1, filter: 'login:' + login } }, { set: noop })
    .then((ret) => get(ret, [ 0, 'id' ]));
  }

  /**
   * @param {number} assoId
   */
  async getAssociation(assoId) {
    return this.tables['associations'].search( // @ts-ignore fake Request
      { get: noop, query: { max: 1, filter: `id:${assoId}` } }, { set: noop })
    .then((ret) => get(ret, [ 0 ]));
  }

  /**
   * @param {number} shiftId
   */
  async getShift(shiftId) {
    return this.tables['shifts'].search( // @ts-ignore fake Request
      { get: noop, query: { max: 1, filter: 'id:' + shiftId } }, { set: noop })
    .then((ret) => get(ret, [ 0 ]));
  }

  /**
   * @param {Request} req
   * @param {Response} res
   */
  async insertComment(req, res) {
    const userId = await this.getUserId(req);
    assign(req.body, { userId, date: now() });

    return await this.tables['comments'].insert(req, res);
  }

  /**
   * @param {Request} req
   * @param {Response} res
   */
  /* eslint-disable-next-line complexity */
  async updateShift(req, res) {
    // Check the user exist from the req
    const userId = await this.getUserId(req, null);
    if (!userId) { throw { status: 500, message: "user not found" }; }

    // Check the shift exist
    const shiftId = toNumber(get(req.params, 'id'));
    if (!shiftId || isNaN(shiftId)) {
      throw { status: 400, message: "shiftId not correctly provided" };
    }
    const shift = await this.getShift(shiftId);
    if (!shift) {
      throw { status: 400, message: `shift not found [${shiftId}]` };
    }

    // Get assoId from the request
    const assoId = get(req.body, 'assoId');

    if (isNil(assoId)) {
      // Free the slot
      const currentAssoId = get(shift, 'assoId');
      // Get current association and check the assoId is related to the user
      const association = await this.getAssociation(currentAssoId);
      if (association.userId !== await this.getUserId(req, null)) {
        throw { status: 403, message: "You do not own this shift slot" };
      }
    }
    else {
      // Assign the slot
      if (shift.assoId) {
        throw { status: 403, message: "shift not available" };
      }
      // Get the association and check the assoId is related to the user
      const association = await this.getAssociation(assoId);
      if (association.userId !== await this.getUserId(req, null)) {
        throw { status: 403, message: "AssoId is not related to the user" };
      }
    }
    // Finally update the shift table
    return await this.tables['shifts'].update(req, res);
  }

  /**
   * @param {Request} req
   * @param {Response} res
   */
  async updateUser(req, res) {
    const authUserId = await this.getUserId(req, null);
    const userId = toNumber(get(req.params, 'id'));

    if (authUserId !== userId) {
      throw { status: 403, message: 'You do not own this account' };
    }
    return await this.tables['users'].update(req, res);
  }

  /**
   * @param {boolean} isDelete
   * @param {Request} req
   * @param {Response} res
   */
  async updateAssociation(isDelete, req, res) {
    const authUserId = await this.getUserId(req, null);
    const assoId = toNumber(get(req.params, 'id'));
    const association = await this.getAssociation(assoId);

    if (isNil(association) || association.userId !== authUserId) {
      throw { status: 403, message: 'You do not own this association' };
    }
    if (isDelete) {
      return await this.tables['associations'].delete(req, res);
    }
    else {
      return await this.tables['associations'].update(req, res);
    }
  }

  /**
   * @param {Request} req
   * @param {Response} res
   */
  async insertAssociation(req, res) {
    const authUserId = await this.getUserId(req, null);
    const userId = get(req.body, 'userId');
    if (authUserId !== userId) {
      throw { status: 403, message: 'You do not own this account' };
    }
    return await this.tables['associations'].insert(req, res);
  }

  /**
   * @param {Request} req
   * @param {Response} res
   */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async updateRunTriggers(req, res) {
    const runNumber = toNumber(get(req.params, 'id'));
    // Prevent sql injection
    // it includes EAR1 and EAR2 + LAB (unnecessary but ok)
    if (isNaN(runNumber) || runNumber < 0 || runNumber > 999999) {
      throw { status: 500, message: 'run number provided is not correct' };
    }
    debug('Executing updateRunTriggers on', runNumber);

    const query =
    `
     UPDATE RUNS
      SET (RUN_TRIGGERS_CALIBRATION,RUN_TRIGGERS_PRIMARY,RUN_TRIGGERS_PARASITIC)
       = (SELECT
         COUNT(CASE WHEN TRIG_SOURCE = 'CALIBRATION' THEN 1 END) AS CALIB,
         COUNT(CASE WHEN TRIG_SOURCE = 'PRIMARY' THEN 1 END) AS PRIM,
         COUNT(CASE WHEN TRIG_SOURCE = 'PARASITIC' THEN 1 END) AS PARAS
        FROM TRIGGERS WHERE TRIG_RUN_NUMBER = RUNS.RUN_NUMBER 
        GROUP BY TRIG_RUN_NUMBER) 
     WHERE RUNS.RUN_NUMBER = ?
      AND EXISTS(select 1 FROM TRIGGERS WHERE TRIG_RUN_NUMBER = RUNS.RUN_NUMBER)
    `;

    return await this.knex.raw(query, [ runNumber ]);
  }

  async createStub() {
    return stubSetup(this.knex);
  }
}

module.exports = Database;
