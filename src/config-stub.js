// @ts-check
const path = require('path');

console.warn('Loading stub configuration');

module.exports = {
  db: {
    client: 'sqlite3',
    useNullAsDefault: true,
    connection: { filename: path.join(__dirname, '..', 'db.sqlite') },
    definition: {
      info: {
        title: 'Sample stub DB', version: '1.0.0'
      }
    }
  }
};
