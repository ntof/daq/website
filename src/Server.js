// @ts-check
const
  express = require('express'),
  helmet = require('helmet'),
  path = require('path'),
  { assign, attempt, noop, cloneDeep, forEach, isNil, get } = require('lodash'),
  cors = require('cors'),
  { makeDeferred } = require('@cern/prom'),
  serveStatic = require('express-static-gzip'),

  debug = require('debug')('app:server'),
  logger = require('./httpLogger'),
  auth = require('./auth'),
  Database = require('./db'),
  schema = require('./db/schema');

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').NextFunction} NextFunction
 */

class Server {
  /**
   * @param {AppServer.Config} config
   */
  constructor(config) {
    this.config = config;
    this.app = express();
    this._prom = this.prepare(config);
  }

  /**
   * @param {AppServer.Config} config
   */
  async prepare(config) {
    if (process.env.NODE_ENV === 'production') {
      // @ts-ignore
      this.app.use(helmet());
    }
    logger.register(this.app);

    this.router = express.Router();
    const dist = path.join(__dirname, '..', 'www', 'dist');
    this.router.use('/dist', serveStatic(dist, { enableBrotli: true }));

    /* everything after this point is authenticated */
    if (config.auth) {
      await auth.register(this.router, config, this.checkUser.bind(this));
      this.router.use(this.runAuth.bind(this, 'user'));
    }

    this.app.set('view engine', 'pug');
    this.app.set('views', path.join(__dirname, 'views'));

    this.router.get('/', (req, res) => res.render('index', config));

    this.db = new Database(config.basePath + '/db', schema, config.db);
    this.router.use('/db', cors(), this.db.router());

    const editSchema = cloneDeep(schema);
    forEach(editSchema, (v) => (v.rw = true));
    this.edit = new Database(config.basePath + '/admin/db', editSchema,
      // @ts-ignore private param, use same db connection
      assign({ knex: this.db.knex }, config.db));
    if (config.auth) {
      this.router.use('/admin', this.runAuth.bind(this, [ 'editor', 'admin' ]));
    }
    this.router.use('/admin/db', cors(),
      this.edit.router(false /* not user mode */));

    if (config.basePath) {
      this.router.get('/beta',
        (req, res) => res.redirect(path.join('/beta', config.basePath)));
    }

    this.app.use(config.basePath, this.router);

    // default route
    this.app.use(function(req, res, next) {
      next({ status: 404, message: 'Not Found' });
    });

    // error handler
    this.app.use(function(
      /** @type {any} */ err,
      /** @type {Request} */ req,
      /** @type {Response} */res,
      /** @type {NextFunction} */ next) { /* eslint-disable-line */ /* jshint ignore:line */
      res.locals.message = err.message;
      res.locals.error = req.app.get('env') === 'development' ? err : {};
      res.locals.status = err.status || 500;
      res.status(err.status || 500);
      res.render('error', { baseUrl: config.basePath });
    });
  }

  close() {
    if (this.server) {
      this.server.close();
      this.server = null;
    }
  }

  /**
   * @param {string|string[]} role
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  runAuth(role, req, res, next) {
    if (req.path.startsWith('/dist')) {
      return next();
    }
    return auth.roleCheck(role, req, res, next);
  }

  /**
   * @param  {any} user user information
   */
  async checkUser(user) {
    try {
      const login = get(user, 'sub');
      const db = /** @type {Database} */ (this.db);
      // @ts-ignore
      const userId = await db.getUserId({ user });
      debug('checking for user', login, userId);
      if (isNil(userId)) {
        // @ts-ignore
        await db.tables['users'].insert({
          body: {
            login,
            firstName: get(user, 'given_name'),
            lastName: get(user, 'family_name'),
            email: get(user, 'email'),
            status: 1, privileges: 0
          }
        }, { set: noop });
        debug('user created: %s', login);
      }
    }
    catch (e) {
      debug('checkUser error:', e);
    }
  }

  /**
   * @param {() => any} cb
   */
  async listen(cb) {
    await this._prom;
    const def = makeDeferred();
    /* we're called as a main, let's listen */
    var server = this.app.listen(this.config.port, () => {
      this.server = server;
      def.resolve(undefined);
      return attempt(cb || noop);
    });
    return def.promise;
  }

  address() {
    if (this.server) {
      return this.server.address();
    }
    return null;
  }
}

module.exports = Server;
