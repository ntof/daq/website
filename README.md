# n_ToF WebSite

WebSite for the n_ToF experiment

This application is deployed there: <https://ntof-data.web.cern.c/website>

A *beta* version is also available there: <https://ntof-data.web.cern.ch/website/beta>

This application is developped according to [EN-SMM-APC guidelines](https://mro-dev.web.cern.ch/docs/std/en-smm-apc-web-guidelines.html).

# Documentation

The complete n_ToF architecture is described in [n_ToF Software Architecture](https://mro-dev.web.cern.ch/docs/info/ntof-software-architecture.html).

# Build

To build this application application (assuming that Node.js is installed on your machine):
```bash
# Run webpack and bundle things in /dist
npm run build

# Serve pages on port 8080
npm run serve
```

# Deploy

To deploy the example application, assuming that oc is configured and connected
on the proper project:
```bash
cd deploy

# Add correct passwords in ConfigMap, replacing xxx
# Do never commit passwords !
vim website-beta.yaml

oc apply -f website-beta.yaml
```
